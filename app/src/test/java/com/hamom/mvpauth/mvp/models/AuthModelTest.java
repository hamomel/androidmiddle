package com.hamom.mvpauth.mvp.models;

import com.birbit.android.jobqueue.JobManager;
import com.hamom.mvpauth.data.managers.DataManager;
import com.hamom.mvpauth.data.network.req.AuthReq;
import com.hamom.mvpauth.data.network.res.AuthRes;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.matchers.Any;
import org.mockito.stubbing.Answer;
import retrofit2.Response;
import retrofit2.Retrofit;
import rx.Observable;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by hamom on 13.03.17.
 */
public class AuthModelTest {
  private AuthModel model;
  @Mock
  DataManager mockDataManager;
  @Mock
  JobManager mockJobManager;

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    model = new AuthModel(mockDataManager, mockJobManager);
  }
  @Test
  public void getLoginSuccessObs() throws Exception {

  }

  @Test
  public void isAuthUser_TRUE() throws Exception {
    when(mockDataManager.isUserAuth()).thenReturn(true);

    assertEquals(true, model.isAuthUser());
    verify(mockDataManager, times(1)).isUserAuth();

  }

  @Test
  public void isAuthUser_FALSE() throws Exception {
    when(mockDataManager.isUserAuth()).thenReturn(false);

    assertEquals(false, model.isAuthUser());
    verify(mockDataManager, times(1)).isUserAuth();

  }

  @Test
  public void loginUser() throws Exception {
    model.loginUser("anymail@mail.com", "password");
    verify(mockDataManager, only()).loginUser(any(AuthReq.class));
  }

  @Test
  public void saveAuthToken() throws Exception {
    String string = "anyString";
    model.saveAuthToken(string);
    verify(mockDataManager, times(1)).saveAuthToken(string);
  }

  @Test
  public void saveUserId() throws Exception {
    String string = "anyString";
    model.saveUserId(string);
    verify(mockDataManager, times(1)).saveUserId(string);
  }
}