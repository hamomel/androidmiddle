package com.hamom.mvpauth.ui.screens.auth;

import android.content.Context;
import android.util.Log;
import com.hamom.mvpauth.data.network.error.ApiError;
import com.hamom.mvpauth.data.network.res.AuthRes;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.mvp.models.AuthModel;
import com.hamom.mvpauth.mvp.presenters.RootPresenter;
import com.hamom.mvpauth.ui.activities.RootActivity;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import rx.Observable;
import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by hamom on 14.03.17.
 */
public class AuthPresenterTest {
  @Mock
  Context mockContext;
  @Mock
  AuthModel mockModel;
  @Mock
  AuthView mockView;
  @Mock
  RootActivity mockRootView;
  @Mock
  RootPresenter mockRootPresenter;
  @Mock
  RootPresenter.ActionBarBuilder mockActionBarBuilder;
  @Mock
  RootPresenter.FabBuilder mockFabBuilder;

  private AuthScreen.AuthPresenter presenter;

  @Before
  @SuppressWarnings("all")
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    BundleServiceRunner mockBundleServiceRunner = new BundleServiceRunner();
    MortarScope mockScope = MortarScope.buildRootScope()
        .withService(BundleServiceRunner.SERVICE_NAME, mockBundleServiceRunner)
        .withService(DaggerService.SERVICE_NAME, mock(AuthScreen.Component.class))
        .build("MockScope");

    given(mockContext.getSystemService(BundleServiceRunner.SERVICE_NAME)).willReturn(mockBundleServiceRunner);
    given(mockContext.getSystemService(MortarScope.class.getName())).willReturn(mockScope);
    given(mockView.getContext()).willReturn(mockContext);
    given(mockModel.getErrorObs()).willReturn(PublishSubject.create());
    given(mockRootPresenter.getRootView()).willReturn(mockRootView);
    given(mockRootPresenter.newActionBarBuilder()).willReturn(mockActionBarBuilder);
    given(mockRootPresenter.newFabBuilder()).willReturn(mockFabBuilder);
    given(mockActionBarBuilder.setVisible(false)).willReturn(mockActionBarBuilder);
    given(mockFabBuilder.setVisible(false)).willReturn(mockFabBuilder);

    RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook() {
      @Override
      public Scheduler getMainThreadScheduler() {
        return Schedulers.immediate();
      }
    });

    presenter = new AuthScreen.AuthPresenter(mockModel, mockRootPresenter);
  }

  @After
  public void tearDown() throws Exception {
    RxAndroidPlugins.getInstance().reset();
  }

  @Test
  public void onLoad_NEVER_SHOW_ERROR() throws Exception{
    given(mockModel.isAuthUser()).willReturn(true);

    presenter.takeView(mockView);
    verify(mockRootView, never()).showError(any(Throwable.class));
    presenter.dropView(mockView);
  }


  @Test
  public void onLoad_isAuthUser_HIDE_LOGIN_BTN() throws Exception{
    given(mockModel.isAuthUser()).willReturn(true);
    presenter.takeView(mockView);
    verify(mockView, times(1)).hideLoginBtn();
    presenter.dropView(mockView);

  }

  @Test
  public void onLoad_isAuthUser_SHOW_LOGIN_BTN() throws Exception{
    given(mockModel.isAuthUser()).willReturn(false);
    presenter.takeView(mockView);
    verify(mockView, times(1)).showLoginBtn();
    presenter.dropView(mockView);

  }

  @Test
  public void clickOnLogin_VIEW_IDLE() throws Exception {
    given(mockView.isIdle()).willReturn(true);
    presenter.takeView(mockView);
    presenter.clickOnLogin();
    verify(mockView, times(1)).showLoginWithAnim();
    presenter.dropView(mockView);

  }

  @Test
  public void clickOnLogin_VIEW_LOGIN_SUCCESS() throws Exception {
    System.out.println("success test ");
    given(mockView.isIdle()).willReturn(false);
    given(mockView.getUserEmail()).willReturn("anymail@mail.com");
    given(mockView.getUserPassword()).willReturn("anypassword");
    given(mockModel.loginUser(anyString(), anyString())).willReturn(Observable.just(new AuthRes()));

    presenter.takeView(mockView);
    presenter.clickOnLogin();
    verify(mockView, times(1)).hideEmailError();
    verify(mockView, times(1)).hidePasswordError();
    verify(mockView, times(1)).showProgressDialog(anyString());
    verify(mockView, times(1)).hidePasswordError();
    verify(mockModel, times(1)).saveAuthToken(null);
    verify(mockModel, times(1)).saveUserId(null);
    presenter.dropView(mockView);


  }

  @Test
  public void clickOnLogin_VIEW_LOGIN_FAIL() throws Exception {
    System.out.println("fail test ");
    given(mockView.isIdle()).willReturn(false);
    given(mockView.getUserEmail()).willReturn("anymail@mail.com");
    given(mockView.getUserPassword()).willReturn("anypassword");
    given(mockRootPresenter.getRootView()).willReturn(mockRootView);
    given(mockModel.loginUser(anyString(), anyString())).willReturn(Observable.error(new ApiError(501)));

    presenter.takeView(mockView);
    presenter.clickOnLogin();
    verify(mockRootView, times(1)).showMessage("Ошибка сервера 501");
    presenter.dropView(mockView);

  }

  @Test
  public void clickOnLogin_VIEW_LOGIN_INVALID() throws Exception {
    given(mockView.isIdle()).willReturn(false);
    given(mockView.getUserEmail()).willReturn("anymail@mai");
    given(mockView.getUserPassword()).willReturn("any");

    presenter.takeView(mockView);
    presenter.clickOnLogin();
    verify(mockView, times(1)).showEmailError();
    verify(mockView, times(1)).showPasswordError();
    presenter.dropView(mockView);


  }

  @Test
  @Ignore
  public void clickOnShowCatalog() throws Exception {

  }

  @Ignore
  @Test
  public void clickOnFb() throws Exception {

  }

  @Test
  @Ignore
  public void clickOnVk() throws Exception {

  }

  @Test
  @Ignore
  public void clickOnTwitter() throws Exception {

  }

  @Test
  @Ignore
  public void checkUserAuth() throws Exception {

  }
}