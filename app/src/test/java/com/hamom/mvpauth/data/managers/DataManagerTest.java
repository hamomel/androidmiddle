package com.hamom.mvpauth.data.managers;

import android.accounts.NetworkErrorException;
import com.hamom.mvpauth.data.network.RestService;
import com.hamom.mvpauth.data.network.error.AccessError;
import com.hamom.mvpauth.data.network.error.ApiError;
import com.hamom.mvpauth.data.network.req.AuthReq;
import com.hamom.mvpauth.data.network.res.AuthRes;
import com.hamom.mvpauth.resourses.MockAuthResponce;
import com.hamom.mvpauth.utils.ConstantManager;
import com.squareup.moshi.Moshi;
import java.util.concurrent.TimeUnit;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.model.TestTimedOutException;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;
import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by hamom on 13.03.17.
 */
public class DataManagerTest {
  private Retrofit testRetrofit;
  private RestService testRestService;
  private MockWebServer testMockWebServer;
  private DataManager testDataManager;
  private TestSubscriber<AuthRes> testSubscriber = new TestSubscriber<>();

  @Before
  public void setUp() throws Exception {
    testMockWebServer = new MockWebServer();
    testRetrofit = new Retrofit.Builder()
        .baseUrl(testMockWebServer.url("").toString())
        .addConverterFactory(MoshiConverterFactory.create(new Moshi.Builder().build()))
        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
        .build();

    testRestService = testRetrofit.create(RestService.class);
    testDataManager = new DataManager(testRestService);
  }

  @After
  public void tearDown() throws Exception {
    testMockWebServer.shutdown();
    testSubscriber.unsubscribe();
  }

  @Test
  public void getUserProfileInfo() throws Exception {

  }

  @Test
  public void saveProfileInfo() throws Exception {

  }

  @Test
  public void getUserSettings() throws Exception {

  }

  @Test
  public void saveSettings() throws Exception {

  }

  @Test
  public void isUserAuth() throws Exception {

  }

  @Test
  public void saveAuthToken() throws Exception {

  }

  @Test
  public void saveUserId() throws Exception {

  }

  @Test
  public void getFavoriteProductsFromRealm() throws Exception {

  }

  @Test
  public void getProductFromRealm() throws Exception {

  }

  @Test
  public void getUserAddressesFromRealm() throws Exception {

  }

  @Test
  public void removeAddressFromRealm() throws Exception {

  }

  @Test
  public void getUserAddressById() throws Exception {

  }

  @Test
  public void saveUserAddressToRealm() throws Exception {

  }

  @Test
  public void getCartProductObs() throws Exception {

  }

  @Test
  public void getProductObsFromNetwork() throws Exception {

  }

  @Test
  public void sendComment() throws Exception {

  }

  @Test
  public void uploadAvatar() throws Exception {

  }

  @Test
  public void updateLocalDataWithTimer() throws Exception {

  }

  @Test(expected = NullPointerException.class)
  public void exceptionExpected(){
    testDataManager.getRetrofit().baseUrl();
  }

  @Test(timeout = 1000)
  public void loginUser_200_OK() throws Exception {
    MockResponse mockResponse = new MockResponse()
        .setHeader(ConstantManager.LAST_MODIFIED_HEADER, "Wed, 15 Nov 1995 04:58:08 GMT")
        .setBody(MockAuthResponce.USER_RES_200)
        .throttleBody(56, 100, TimeUnit.MILLISECONDS);

    testMockWebServer.enqueue(mockResponse);
    testDataManager.loginUser(new AuthReq("anymail@mail.com", "password"))
        .subscribe(response -> {
          assertNotNull(response);
          assertEquals("Вася", response.getFullName());

        }, throwable -> Assert.fail());
  }

  @Test
  public void loginUser_200_RX_OK() throws Exception {
    MockResponse mockResponse = new MockResponse()
        .setHeader(ConstantManager.LAST_MODIFIED_HEADER, "Wed, 15 Nov 1995 04:58:08 GMT")
        .setBody(MockAuthResponce.USER_RES_200);

    testMockWebServer.enqueue(mockResponse);
    testDataManager.loginUser(new AuthReq("anymail@mail.com", "password"))
        .subscribe(testSubscriber);

    testSubscriber.assertCompleted();
  }

  @Test
  public void loginUser_403_FORBIDDEN() throws Exception {
    MockResponse mockResponse = new MockResponse()
        .setResponseCode(403);

    testMockWebServer.enqueue(mockResponse);
    testDataManager.loginUser(new AuthReq("anymail@mail.com", "password"))
        .subscribe(response -> Assert.fail(), throwable -> {
          assertNotNull(throwable);
          assertEquals("Не верный логин или пароль", throwable.getMessage());
        });
  }

  @Test
  public void loginUser_403_RX_FORBIDDEN() throws Exception {
    MockResponse mockResponse = new MockResponse()
        .setResponseCode(403);

    testMockWebServer.enqueue(mockResponse);
    testDataManager.loginUser(new AuthReq("anymail@mail.com", "password"))
        .subscribe(testSubscriber);

    testSubscriber.assertError(AccessError.class);
  }


  @Test
  public void sendFavorite() throws Exception {

  }

  @Test
  public void getFavoriteProducts() throws Exception {

  }

  @Test
  public void deleteFavorite() throws Exception {

  }
}