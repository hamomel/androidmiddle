package com.hamom.mvpauth.utils;

public interface AppConfig {
  boolean DEBUG = true;

  //network
  String BASE_URL = "https://skba1.mgbeta.ru/api/v1/";
  //String BASE_URL = "https://private-anon-54d196cff8-middleappskillbranch.apiary-mock.com";
  long MAX_CONNECT_TIMEOUT = 5000L;
  long MAX_READ_TIMEOUT = 5000L;
  long MAX_WRITE_TIMEOUT = 5000L;

  //job queue
  int MAX_CONSUMER_COUNT = 3;
  int MIN_CONSUMER_COUNT = 1;
  int KEEP_ALIVE_CONST = 120;
  int LOAD_FACTOR_CONST = 3;
  int INITIAL_BACK_OFF_IN_MS = 1000;
  int UPDATE_DATA_INTERVAL = 30;
  int RETRY_REQUEST_COUNT = 5;
  int RETRY_REQUEST_BASE_DELAY = 500;
}
