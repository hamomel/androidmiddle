package com.hamom.mvpauth.utils;

import com.hamom.mvpauth.BuildConfig;

/**
 * Created by hamom on 22.10.16.
 */
public interface ConstantManager {
  String TAG_PREFIX = "Mvp: ";
  String FILE_PROVIDER_AUTHORITY = BuildConfig.APPLICATION_ID + ".fileprovider";

  int REQUEST_PHOTO_FROM_GALLERY = 1001;
  int REQUEST_CAMERA_PICTURE = 1002;
  int REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 3001;
  int REQUEST_PERMISSION_CAMERA = 3002;

  String LAST_MODIFIED_HEADER = "Last-Modified";
  String IF_MODIFIED_SINCE_HEADER = "If-Modified-Since";
  String USER_ID_HEADER = "Authorization";
}
