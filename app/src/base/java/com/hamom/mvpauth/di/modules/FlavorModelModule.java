package com.hamom.mvpauth.di.modules;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;
import com.hamom.mvpauth.utils.App;
import com.hamom.mvpauth.utils.AppConfig;
import dagger.Module;
import dagger.Provides;

/**
 * Created by hamomel on 26.01.17.
 */
@Module
public class FlavorModelModule {

  @Provides
  JobManager provideJobManager() {

    Configuration configuration = new Configuration.Builder(App.getAppContext())
        .minConsumerCount(AppConfig.MIN_CONSUMER_COUNT)
        .maxConsumerCount(AppConfig.MAX_CONSUMER_COUNT)
        .consumerKeepAlive(AppConfig.KEEP_ALIVE_CONST)
        .loadFactor(AppConfig.LOAD_FACTOR_CONST)
        .build();
    return new JobManager(configuration);
  }
}
