package com.hamom.mvpauth.di.modules;

import android.content.Context;

import com.facebook.stetho.Stetho;
import com.hamom.mvpauth.data.managers.AppPreferencesManager;
import com.hamom.mvpauth.data.managers.RealmManager;
import com.hamom.mvpauth.di.scopes.RootScope;

import com.hamom.mvpauth.utils.ConstantManager;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hamom on 04.11.16.
 */
@Module
public class FlavorLocalModule {
  private static String TAG = ConstantManager.TAG_PREFIX + "BASELocalModule: ";

  @Provides
  @Singleton
  RealmManager provideRealmManager(Context context){
    Stetho.initialize(Stetho.newInitializerBuilder(context)
        .enableDumpapp(Stetho.defaultDumperPluginsProvider(context))
        .enableWebKitInspector(RealmInspectorModulesProvider.builder(context).build())
        .build());
    RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
    Realm.setDefaultConfiguration(realmConfiguration);
    return new RealmManager();
  }
}
