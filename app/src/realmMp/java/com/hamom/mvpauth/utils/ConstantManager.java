package com.hamom.mvpauth.utils;

import com.hamom.mvpauth.BuildConfig;

/**
 * Created by hamom on 22.10.16.
 */
public interface ConstantManager {
  String TAG_PREFIX = "Mvp: ";
  String FILE_PROVIDER_AUTHORITY = BuildConfig.APPLICATION_ID + ".fileprovider";

  int REQUEST_PHOTO_FROM_GALLERY = 1001;
  int REQUEST_CAMERA_PICTURE = 1002;
  int REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 3001;
  int REQUEST_PERMISSION_CAMERA = 3002;

  String LAST_MODIFIED_HEADER = "Last-Modified";
  String IF_MODIFIED_SINCE_HEADER = "If-Modified-Since";

  String REALM_USER = "mymail@mymail.com";
  String REALM_PASSWORD = "2222";
  String REALM_AUTH_URL = "http://192.168.0.22:9080/auth";
  String REALM_DB_URL = "realm://192.168.0.22:9080/~/default";
}
