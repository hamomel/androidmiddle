package com.hamom.mvpauth.utils;

/**
 * Created by hamom on 21.10.16.
 */
public interface AppConfig {
    boolean DEBUG = true;
    String BASE_URL = "https://skba1.mgbeta.ru/api/v1/";
    long MAX_CONNECT_TIMEOUT = 5000L;
    long MAX_READ_TIMEOUT = 5000L;
    long MAX_WRITE_TIMEOUT = 5000L;
}
