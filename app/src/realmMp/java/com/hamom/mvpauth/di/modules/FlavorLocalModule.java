package com.hamom.mvpauth.di.modules;

import android.content.Context;
import android.util.Log;
import com.facebook.stetho.Stetho;
import com.hamom.mvpauth.data.managers.RealmManager;
import com.hamom.mvpauth.utils.ConstantManager;
import dagger.Module;
import dagger.Provides;
import io.realm.ObjectServerError;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.SyncConfiguration;
import io.realm.SyncCredentials;
import io.realm.SyncUser;
import javax.inject.Singleton;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by hamom on 04.11.16.
 */
@Module
public class FlavorLocalModule {
  private static String TAG = ConstantManager.TAG_PREFIX + "MPLocalModule: ";


  @Provides
  @Singleton
  RealmManager provideRealmManager(Context context){
    Stetho.initializeWithDefaults(context);

    Observable.create(new Observable.OnSubscribe<SyncUser>() {
      @Override
      public void call(Subscriber<? super SyncUser> subscriber) {
        SyncCredentials myCredentials = SyncCredentials.usernamePassword(ConstantManager.REALM_USER,
            ConstantManager.REALM_PASSWORD, false);

        //sync. create observable from sync result
        if (!subscriber.isUnsubscribed()){
          try {
            subscriber.onNext(SyncUser.login(myCredentials, ConstantManager.REALM_AUTH_URL));
            subscriber.onCompleted();
          } catch (Exception e) {
            subscriber.onError(e);
          }
        }

        //async. create observable from callback result
        //if (!subscriber.isUnsubscribed()){
        //  SyncUser.loginAsync(myCredentials, ConstantManager.REALM_AUTH_URL, new SyncUser.Callback() {
        //    @Override
        //    public void onSuccess(SyncUser user) {
        //      subscriber.onNext(user);
        //      subscriber.onCompleted();
        //    }
        //
        //    @Override
        //    public void onError(ObjectServerError error) {
        //      subscriber.onError(error);
        //    }
        //  });
        //}
      }
    })
    .subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())
    .subscribe(syncUser -> {
      SyncConfiguration syncConfiguration = new SyncConfiguration.Builder(syncUser,
          ConstantManager.REALM_DB_URL).build();
      Realm.setDefaultConfiguration(syncConfiguration); //устанавливаем sync конфигурацию для realm mobile platform
    });


    return new RealmManager();
  }
}
