package com.hamom.mvpauth.flow;

import android.util.Log;

import com.hamom.mvpauth.mortar.ScreenScoper;
import com.hamom.mvpauth.utils.ConstantManager;

import flow.ClassKey;

/**
 * Created by hamom on 26.11.16.
 */

public abstract class AbstractScreen<T> extends ClassKey {
  private static String TAG = ConstantManager.TAG_PREFIX + "AbstractScreen: ";

  public String getScopeName() {
    return getClass().getName();
  }

  public abstract Object createScreenComponent(T parentComponent);

  public void unregisterScope() {
    Log.d(TAG, "unregisterScope: " + this.getScopeName());
    ScreenScoper.destroyScreenScope(getScopeName());
  }

  public int getLayoutResId() {
    int layout = 0;
    Screen screen;
    screen = this.getClass().getAnnotation(Screen.class);
    if (screen != null) {
      throw new IllegalStateException("@Screen annotation is missing on screen " + getScopeName());
    } else {
      layout = screen.value();
    }
    return layout;
  }
}
