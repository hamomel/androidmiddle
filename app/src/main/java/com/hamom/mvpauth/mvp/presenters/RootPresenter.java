package com.hamom.mvpauth.mvp.presenters;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;

import android.util.Log;
import com.fernandocejas.frodo.annotation.RxLogSubscriber;
import com.hamom.mvpauth.data.storage.dto.ActivityResultDto;
import com.hamom.mvpauth.data.storage.dto.UserInfoDto;
import com.hamom.mvpauth.mvp.models.AccountModel;
import com.hamom.mvpauth.mvp.views.IFabView;
import com.hamom.mvpauth.mvp.views.IRootView;
import com.hamom.mvpauth.ui.activities.RootActivity;

import com.hamom.mvpauth.utils.App;

import com.hamom.mvpauth.utils.ConstantManager;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import mortar.Presenter;
import mortar.bundler.BundleService;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by hamom on 06.11.16.
 */

public class RootPresenter extends Presenter<IRootView> {
  private static String TAG = ConstantManager.TAG_PREFIX + "RootPresenter: ";
  private final static int DEFAULT_MODE = 0;
  private final static int TAB_MODE = 1;

  @Inject
  AccountModel mAccountModel;

  private BehaviorSubject<ActivityResultDto> mActivityResultDtoObs = BehaviorSubject.create();
  private CompositeSubscription mSubscriptions = new CompositeSubscription();


  public RootPresenter() {
    App.getRootActivityComponent().inject(this);
  }

  @Override
  protected BundleService extractBundleService(IRootView view) {
    return BundleService.getBundleService((RootActivity) view);
  }

  public BehaviorSubject<ActivityResultDto> getActivityResultDtoObs() {
    return mActivityResultDtoObs;
  }


  @Override
  protected void onLoad(Bundle savedInstanceState) {
    super.onLoad(savedInstanceState);
    mSubscriptions.add(subscribeOnUserInfoObs());
    mSubscriptions.add(subscribeOnCartObs());
  }


  @Override
  public void dropView(IRootView view) {
    if (mSubscriptions != null) {
      mSubscriptions.unsubscribe();
    }
    super.dropView(view);
  }

  private Subscription subscribeOnCartObs(){
    return mAccountModel.getProductCartObs().subscribe(productRealms -> {
      if (hasView()) {
        getView().initCartIcon(productRealms);
      }
    });
  }

  private Subscription subscribeOnUserInfoObs(){
    return  mAccountModel.getUserInfoObs()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new UserInfoSubscriber());
  }

  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    mActivityResultDtoObs.onNext(new ActivityResultDto(requestCode, resultCode, data));
  }

  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResult) {
    // TODO: 09.12.16 implement method
  }

  public boolean checkPermissionsAndRequestIfNotGranted(@NonNull String[] permissions,
      int requestCode) {
    boolean allGranted = true;
    for (String permission : permissions) {

      int selfPermission =
          ContextCompat.checkSelfPermission(((RootActivity) getView()), permission);
      if (selfPermission != PackageManager.PERMISSION_GRANTED) {
        allGranted = false;
        break;
      }
    }
    if (!allGranted) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        ((RootActivity) getView()).requestPermissions(permissions, requestCode);
      }
      return false;
    }

    return allGranted;
  }

  @Nullable
  public IRootView getRootView() {
    return getView();
  }

  @RxLogSubscriber
  private class UserInfoSubscriber extends Subscriber<UserInfoDto>{
    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
      if (getView() != null) {
        getView().showError(e);
      }
    }

    @Override
    public void onNext(UserInfoDto userInfoDto) {
      Log.d(TAG, "userInfo");
      if (getView() != null) {
        getView().initDrawerInfo(userInfoDto);
      }
    }
  }

  //region===================== Builders ==========================
  public ActionBarBuilder newActionBarBuilder() {
    return new ActionBarBuilder();
  }

  public FabBuilder newFabBuilder(){
    return new FabBuilder();
  }

  public class FabBuilder{
    private boolean isVisible = false;
    private int image = 0;
    private FloatingActionButton.OnClickListener listener = null;

    public FabBuilder setVisible(boolean isVisible){
      this.isVisible = isVisible;
      return this;
    }

    public FabBuilder setIcon(int resId){
      this.image = resId;
      return this;
    }

    public FabBuilder setAction(FloatingActionButton.OnClickListener action){
      this.listener = action;
      return this;
    }

    public void build(){
      IFabView activity = (IFabView) getView();
      activity.setFabVisible(isVisible);
      activity.setFabIcon(image);
      activity.setAction(listener);
    }

  }

  public class ActionBarBuilder{
    private boolean isGoBack = false;
    private boolean isVisible = true;
    private boolean showCart = true;
    private CharSequence title;
    private List<MenuItemHolder> items = new ArrayList<>();
    private ViewPager pager;
    private int toolbarMode = DEFAULT_MODE;

    public ActionBarBuilder setBackArrow(boolean isGoBack){
      this.isGoBack = isGoBack;
      return this;
    }
    public ActionBarBuilder setVisible(boolean isVisible){
      this.isVisible = isVisible;
      return this;
    }

    public ActionBarBuilder setTitle(CharSequence title){
      this.title = title;
      return this;
    }

    public ActionBarBuilder addAction(MenuItemHolder action){
      items.add(action);
      return this;
    }

    public ActionBarBuilder showCart(boolean showCart){
      this.showCart = showCart;
      return this;
    }
    public ActionBarBuilder setTab(ViewPager pager){
      toolbarMode = TAB_MODE;
      this.pager = pager;
      return this;
    }

    public void build(){
      Log.d(TAG, "build: " + items.size());
      if (getView() != null) {
        RootActivity activity = (RootActivity) getView();
        activity.setToolbarVisible(isVisible);
        activity.showCart(showCart);
        activity.setMenuItems(items);
        activity.setToolbarTitle(title);
        activity.setBackArrow(isGoBack);
        if (toolbarMode == TAB_MODE){
          activity.setTabLayout(pager);
        } else {
          activity.removeTabLayout();
        }
      }
    }
  }
  //endregion

}
