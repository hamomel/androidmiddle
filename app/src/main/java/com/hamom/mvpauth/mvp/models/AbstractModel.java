package com.hamom.mvpauth.mvp.models;

import android.util.Log;
import com.birbit.android.jobqueue.JobManager;
import com.hamom.mvpauth.data.managers.DataManager;

import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.di.components.AppComponent;
import com.hamom.mvpauth.di.modules.ModelModule;
import com.hamom.mvpauth.di.scopes.RootScope;
import com.hamom.mvpauth.utils.ConstantManager;
import com.hamom.mvpauth.utils.App;
import javax.inject.Inject;

import dagger.Provides;
import javax.inject.Singleton;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

/**
 * Created by hamom on 04.11.16.
 */

public class AbstractModel {
  private static String TAG = ConstantManager.TAG_PREFIX + "AbstractModel: ";

  @Inject
  DataManager mDataManager;

  @Inject
  JobManager mJobManager;

  public AbstractModel() {
    DaggerService.createComponent(App.getAppComponent(), AbstractModel.Component.class).inject(this);
  }

  //for tests only
  public AbstractModel(DataManager dataManager, JobManager jobManager) {
    mDataManager = dataManager;
    mJobManager = jobManager;
  }

  private PublishSubject<Throwable> mErrorObs = PublishSubject.create();

  public PublishSubject<Throwable> getErrorObs() {
    return mErrorObs;
  }

  protected <T> Subscription subscribe(Observable<T> observable, ModelSubscriber<T> subscriber) {
    return observable.subscribeOn(Schedulers.io()).subscribe(subscriber);
  }

  protected abstract class ModelSubscriber<T> extends Subscriber<T> {
    @Override
    public void onCompleted() {
      Log.d(TAG, "onComplete model observable");
    }

    @Override
    public void onError(Throwable e) {
      mErrorObs.onNext(e);
    }

    @Override
    public abstract void onNext(T t);
  }

  //region===================== DI ==========================

  @RootScope
  @dagger.Component(dependencies = AppComponent.class, modules = ModelModule.class)
  public interface Component{
    void inject(AbstractModel abstractModel);

  }
  //endregion
}
