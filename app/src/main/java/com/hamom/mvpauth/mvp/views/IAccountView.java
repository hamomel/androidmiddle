package com.hamom.mvpauth.mvp.views;

/**
 * Created by hamom on 28.11.16.
 */

public interface IAccountView extends IView {
    void changeState();

    void showEditState();

    void showPreviewState();

    void showPhotoSourceDialog();
}
