package com.hamom.mvpauth.mvp.views;

import android.support.v4.view.ViewPager;

import com.hamom.mvpauth.mvp.presenters.MenuItemHolder;

import java.util.List;

/**
 * Created by hamom on 06.01.2017.
 */

public interface IActionBarView {

    void setToolbarTitle(CharSequence title);
    void setToolbarVisible(boolean visible);
    void setBackArrow(boolean enabled);
    void setMenuItems(List<MenuItemHolder> items);
    void showCart(boolean showCart);
    void setTabLayout(ViewPager pager);
    void removeTabLayout();
}
