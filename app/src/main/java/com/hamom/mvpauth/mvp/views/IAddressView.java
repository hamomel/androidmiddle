package com.hamom.mvpauth.mvp.views;

import com.hamom.mvpauth.data.storage.dto.UserAddressDto;

/**
 * Created by hamom on 30.11.16.
 */

public interface IAddressView extends IView {
    void showInputError();
    UserAddressDto getUserAddress();
}
