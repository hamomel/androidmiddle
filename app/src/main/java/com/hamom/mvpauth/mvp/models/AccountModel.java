package com.hamom.mvpauth.mvp.models;

import android.text.TextUtils;
import android.util.Log;
import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.hamom.mvpauth.data.managers.AppPreferencesManager;
import com.hamom.mvpauth.data.managers.DataManager;
import com.hamom.mvpauth.data.storage.dto.UserAddressDto;
import com.hamom.mvpauth.data.storage.dto.UserInfoDto;
import com.hamom.mvpauth.data.storage.dto.UserSettingsDto;

import com.hamom.mvpauth.data.storage.realm.AddressRealm;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import com.hamom.mvpauth.jobs.UploadAvatarJob;
import com.hamom.mvpauth.utils.ConstantManager;
import io.realm.RealmResults;
import java.util.Map;

import javax.inject.Inject;
import rx.Observable;
import rx.subjects.BehaviorSubject;

/**
 * Created by hamom on 28.11.16.
 */

public class AccountModel extends AbstractModel {
  private static String TAG = ConstantManager.TAG_PREFIX + "AccountModel: ";
  private BehaviorSubject<UserInfoDto> mUserInfoObs = BehaviorSubject.create();


  public AccountModel() {
    super();
    mUserInfoObs.onNext(getUserProfileInfo());
  }

  @RxLogObservable
  public BehaviorSubject<UserInfoDto> getUserInfoObs() {
    Log.d(TAG, "getUserInfoObs: ");
    return mUserInfoObs;
  }

  //region===================== Address ==========================

  public Observable<UserAddressDto> getAddressObs() {
    return mDataManager.getUserAddressesFromRealm().map(UserAddressDto::new);
  }

  public void removeAddress(int id) {
    mDataManager.removeAddressFromRealm(id);
  }

  public void saveUserAddress(UserAddressDto userAddress) {
    mDataManager.saveUserAddressToRealm(userAddress);
  }

  public AddressRealm getAddressFromRealm(int id) {
    return mDataManager.getUserAddressById(id);
  }
  //endregion

  //region===================== Settings ==========================
  public Observable<UserSettingsDto> getUserSettingsObs() {
    return Observable.just(getUserSettings());
  }

  private UserSettingsDto getUserSettings() {
    Map<String, Boolean> map = mDataManager.getUserSettings();
    return new UserSettingsDto(map.get(AppPreferencesManager.NOTIFICATION_ORDER_KEY),
        map.get(AppPreferencesManager.NOTIFICATION_PROMO_KEY));
  }

  public void saveSettings(UserSettingsDto settings) {
    mDataManager.saveSettings(AppPreferencesManager.NOTIFICATION_ORDER_KEY,
        settings.isOrderNotification());
    mDataManager.saveSettings(AppPreferencesManager.NOTIFICATION_PROMO_KEY,
        settings.isPromoNotification());
  }

  //endregion

  //region===================== User ==========================

  public UserInfoDto getUserProfileInfo() {
    Map<String, String> map = mDataManager.getUserProfileInfo();
    return new UserInfoDto(map.get(AppPreferencesManager.PROFILE_FULL_NAME_KEY),
        map.get(AppPreferencesManager.PROFILE_PHONE_KEY),
            map.get(AppPreferencesManager.PROFILE_AVATAR_KEY));
  }

  public void saveProfileInfo(UserInfoDto userProfileInfo) {
    mDataManager.saveProfileInfo(userProfileInfo.getName(), userProfileInfo.getPhone(),
        userProfileInfo.getAvatar());
    mUserInfoObs.onNext(userProfileInfo);

    String avatarUri = userProfileInfo.getAvatar();
    if (!avatarUri.equals("null") && !avatarUri.contains("http")){
      uploadAvatarOnServer(avatarUri);
    }
  }

  private void uploadAvatarOnServer(String avatarUri){

    if (avatarUri != null){
      Log.d(TAG, "uploadAvatarOnServer: " + avatarUri);
      mJobManager.addJobInBackground(new UploadAvatarJob(avatarUri));
    }
  }

  public Observable<RealmResults<ProductRealm>> getProductCartObs() {
    return mDataManager.getCartProductObs();
  }
  //endregion
}
