package com.hamom.mvpauth.mvp.models;

import android.util.Log;
import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.hamom.mvpauth.data.managers.AppPreferencesManager;
import com.hamom.mvpauth.data.managers.DataManager;
import com.hamom.mvpauth.data.network.error.ApiError;
import com.hamom.mvpauth.data.network.error.NetworkAvailableError;
import com.hamom.mvpauth.data.network.res.CommentRes;
import com.hamom.mvpauth.data.network.res.ProductRes;
import com.hamom.mvpauth.data.storage.dto.ProductDto;

import com.hamom.mvpauth.data.storage.dto.ProductLocalInfo;
import com.hamom.mvpauth.data.storage.realm.CommentRealm;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import com.hamom.mvpauth.jobs.AddFavoriteJob;
import com.hamom.mvpauth.jobs.DeleteFavoriteJob;
import com.hamom.mvpauth.jobs.SendMessageJob;
import com.hamom.mvpauth.utils.ConstantManager;
import java.util.List;

import io.realm.RealmObject;
import javax.inject.Inject;
import retrofit2.Response;
import rx.Observable;
import rx.Subscription;

/**
 * Created by hamom on 30.10.16.
 */

public class CatalogModel extends AbstractModel {
  private static String TAG = ConstantManager.TAG_PREFIX + "CatalogModel: ";


  public CatalogModel() {
    super();
  }

  public boolean isUserAuth() {
    return mDataManager.isUserAuth();
  }

  @RxLogObservable
  public Observable<ProductRealm> getProductObs() {
    Observable<ProductRealm> network = fromNetwork();
    Observable<ProductRealm> disk = fromDisc();

    return Observable.mergeDelayError(network, disk).distinct(ProductRealm::getId);
  }

  public Observable<ProductRealm> fromNetwork() {
    return mDataManager.getProductObsFromNetwork();
  }

  public Observable<ProductRealm> fromDisc() {
    return mDataManager.getProductFromRealm();
  }

  public String getUserName() {
    return mDataManager.getUserProfileInfo().get(AppPreferencesManager.PROFILE_FULL_NAME_KEY);
  }

  public void sendComment(String id, CommentRealm comment) {
    SendMessageJob job = new SendMessageJob(id, comment);
    mJobManager.addJobInBackground(job);
  }

  public void addFavoriteJob(String productId){
    mJobManager.addJobInBackground(new AddFavoriteJob(productId));
  }

  public void deleteFavoriteJob(String productId) {
    mJobManager.addJobInBackground(new DeleteFavoriteJob(productId));
  }
}
