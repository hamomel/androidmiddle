package com.hamom.mvpauth.mvp.views;

import com.hamom.mvpauth.mvp.presenters.IAuthPresenter;

/**
 * Created by hamom on 21.10.16.
 */

public interface IAuthView extends  IView{

    IAuthPresenter getPresenter();

    void showLoginBtn();
    void hideLoginBtn();

    void showProgressDialog(String title);
    void hideProgressDialog();

    String getUserEmail();
    String getUserPassword();

    void showEmailError();
    void showPasswordError();
    void hideEmailError();
    void hidePasswordError();
    boolean isIdle();

    void showLoginWithAnim();
}
