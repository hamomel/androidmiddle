package com.hamom.mvpauth.mvp.models;

import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import com.hamom.mvpauth.jobs.DeleteFavoriteJob;
import io.realm.RealmResults;
import java.util.List;
import retrofit2.Call;
import retrofit2.Response;
import rx.Observable;

/**
 * Created by hamom on 27.02.17.
 */
public class FavoriteModel extends AbstractModel{

  public Observable<RealmResults<ProductRealm>> getFavoriteProducts(){
    return mDataManager.getFavoriteProductsFromRealm();
  }

  public void deleteFavoriteJob(String productId) {
    mJobManager.addJobInBackground(new DeleteFavoriteJob(productId));
  }

}
