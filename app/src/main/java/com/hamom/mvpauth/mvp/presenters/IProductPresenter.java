package com.hamom.mvpauth.mvp.presenters;

/**
 * Created by hamom on 28.10.16.
 */

public interface IProductPresenter {
    void clickOnPlus();
    void clickOnMinus();
}
