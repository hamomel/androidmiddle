package com.hamom.mvpauth.mvp.presenters;

import android.view.View;

/**
 * Created by hamom on 30.10.16.
 */

public interface ICatalogPresenter {
    void clickOnBuyButton(int position);
    boolean checkUserAuth();
    void setMenuItemChecked();

}
