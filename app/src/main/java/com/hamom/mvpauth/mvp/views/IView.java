package com.hamom.mvpauth.mvp.views;

/**
 * Created by hamom on 28.10.16.
 */

public interface IView {
    boolean viewOnBackPressed();
}
