package com.hamom.mvpauth.mvp.models;

import android.os.Handler;

import com.birbit.android.jobqueue.JobManager;
import com.hamom.mvpauth.data.managers.DataManager;
import com.hamom.mvpauth.data.network.req.AuthReq;
import com.hamom.mvpauth.data.network.res.AuthRes;
import java.util.List;
import javax.inject.Inject;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

/**
 * Created by hamom on 21.10.16.
 */

public class AuthModel extends AbstractModel {
private PublishSubject<Boolean> mLoginSuccessObs = PublishSubject.create();

  public AuthModel() {
    super();
  }

  //for tests only
  public AuthModel(DataManager dataManager, JobManager jobManager){
    super(dataManager, jobManager);
  }

  public PublishSubject<Boolean> getLoginSuccessObs() {
    return mLoginSuccessObs;
  }

  public boolean isAuthUser() {

    return mDataManager.isUserAuth();
  }

  public Observable<AuthRes> loginUser(String login, String password) {

    return mDataManager.loginUser(new AuthReq(login, password));

  }

  public void saveAuthToken(String token) {
    mDataManager.saveAuthToken(token);
  }

  public void saveUserId(String id){
    mDataManager.saveUserId(id);
  }


}
