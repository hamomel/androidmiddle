package com.hamom.mvpauth.mvp.views;

import android.view.View;
import com.hamom.mvpauth.data.storage.dto.UserInfoDto;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import io.realm.RealmResults;

/**
 * Created by hamom on 06.11.16.
 */
public interface IRootView extends IView {
    void showMessage(String message);
    void showError(Throwable e);

    void showLoad();
    void hideLoad();
    void initDrawerInfo(UserInfoDto userInfoDto);

    void hideToolbar();
    void showToolbar();

    void setMenuItemChecked(View view);

    IView getCurrentScreen();

    void initCartIcon(RealmResults<ProductRealm> productRealms);
}
