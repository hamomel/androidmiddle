package com.hamom.mvpauth.mvp.presenters;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import com.hamom.mvpauth.mvp.models.AbstractModel;
import com.hamom.mvpauth.mvp.views.AbstractView;
import com.hamom.mvpauth.mvp.views.IRootView;
import com.hamom.mvpauth.mvp.views.IView;

import javax.inject.Inject;

import mortar.MortarScope;
import mortar.ViewPresenter;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

/**
 * Created by hamom on 28.10.16.
 */

public abstract class AbstractPresenter<V extends AbstractView, M extends AbstractModel>
    extends ViewPresenter<V> {
  private String TAG = this.getClass().getSimpleName();

  @Inject
  protected M mModel;

  @Inject
  protected RootPresenter mRootPresenter;

  protected CompositeSubscription mSubscriptions;

  public AbstractPresenter() {
  }

  //for tests only
  public AbstractPresenter(M model, RootPresenter rootPresenter) {
    mModel = model;
    mRootPresenter = rootPresenter;
  }

  @Override
  protected void onEnterScope(MortarScope scope) {
    super.onEnterScope(scope);
    initDagger(scope);
  }

  @Override
  protected void onLoad(Bundle savedInstanceState) {
    super.onLoad(savedInstanceState);
    mSubscriptions = new CompositeSubscription();
    subscribeOnModelError();
    initActionBar();
    initFab();

  }


  @Override
  public void dropView(V view) {
    super.dropView(view);
    if (mSubscriptions.hasSubscriptions()) mSubscriptions.unsubscribe();
  }

  public void setMenuItemChecked() {
    if (getRootView() != null && getView() != null) {
      getRootView().setMenuItemChecked(getView());
    }
  }

  protected abstract void initActionBar();

  protected abstract void initFab();

  protected abstract void initDagger(MortarScope scope);

  protected IRootView getRootView() {
    return mRootPresenter.getRootView();
  }

  //provide errors from model to MainActivity
  protected void subscribeOnModelError() {
    subscribe(mModel.getErrorObs(), new ViewSubscriber<Throwable>() {
      @Override
      public void onNext(Throwable throwable) {
        if (getRootView() != null) {
          getRootView().showError(throwable);
        }
      }
    });
  }

  protected <T> void subscribe(Observable<T> observable, Subscriber<T> subscriber) {
    Subscription subscription = observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(subscriber);
    mSubscriptions.add(subscription);
  }

  protected abstract class ViewSubscriber<T> extends Subscriber<T> {
    @Override
    public void onCompleted() {
      Log.d(TAG, "onComplete presenter observable");
    }

    @Override
    public void onError(Throwable e) {
      if (getRootView() != null) {
        getRootView().showError(e);
      }
    }

    @Override
    public abstract void onNext(T t);
  }
}
