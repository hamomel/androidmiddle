package com.hamom.mvpauth.mvp.presenters;

import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by hamom on 06.01.2017.
 */

public class MenuItemHolder {
  private CharSequence itemTitle;
  private int itemResId;
  private View actionView;
  private MenuItem.OnMenuItemClickListener listener;

  public MenuItemHolder(CharSequence itemTitle, int itemResId,
      MenuItem.OnMenuItemClickListener listener) {
    this.itemResId = itemResId;
    this.itemTitle = itemTitle;
    this.listener = listener;
  }

  public MenuItemHolder(CharSequence itemTitle, View actionView) {
    this.itemTitle = itemTitle;
    this.actionView = actionView;

  }

  public int getItemResId() {
    return itemResId;
  }

  public CharSequence getItemTitle() {
    return itemTitle;
  }

  public MenuItem.OnMenuItemClickListener getListener() {
    return listener;
  }

  public View getActionView() {
    return actionView;
  }
}
