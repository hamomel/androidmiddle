package com.hamom.mvpauth.mvp.presenters;

import android.view.View;

/**
 * Created by hamom on 28.11.16.
 */

public interface IAccountPresenter {
    void clickOnAddAddress();
    boolean switchViewState();
    void takePhoto();
    void chooseCamera();
    void chooseGallery();
    void setMenuItemChecked();
    void removeAddress(int position);

}
