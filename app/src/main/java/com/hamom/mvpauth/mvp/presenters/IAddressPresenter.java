package com.hamom.mvpauth.mvp.presenters;

/**
 * Created by hamom on 30.11.16.
 */

public interface IAddressPresenter {
    void clickOnAddAddress();
}
