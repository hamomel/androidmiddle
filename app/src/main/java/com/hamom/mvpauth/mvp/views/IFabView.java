package com.hamom.mvpauth.mvp.views;

import android.support.design.widget.FloatingActionButton;
import android.view.View;

/**
 * Created by hamom on 13.01.17.
 */

public interface IFabView {
  void setFabVisible(boolean visible);
  void setFabIcon(int resId);
  void setAction(FloatingActionButton.OnClickListener action);
}
