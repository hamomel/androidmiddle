package com.hamom.mvpauth.mvp.presenters;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.ViewGroup;
import com.hamom.mvpauth.mvp.views.IRootView;
import com.hamom.mvpauth.utils.ConstantManager;
import java.util.ArrayList;
import java.util.List;

import mortar.MortarScope;
import mortar.ViewPresenter;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by hamom on 08.12.16.
 */

public abstract class SubscribePresenter<V extends ViewGroup> extends ViewPresenter<V> {
  private String TAG = ConstantManager.TAG_PREFIX + this.getClass().getSimpleName();

  @Nullable
  protected abstract IRootView getRootView();

  protected CompositeSubscription mSubscriptions;

  @Override
  protected void onEnterScope(MortarScope scope) {
    super.onEnterScope(scope);
    mSubscriptions = new CompositeSubscription();
  }

  @Override
  protected void onLoad(Bundle savedInstanceState) {
    super.onLoad(savedInstanceState);
  }

  @Override
  public void dropView(V view) {
    super.dropView(view);
    if (mSubscriptions.hasSubscriptions()) {
      Log.d(TAG, "dropView: ");

      mSubscriptions.unsubscribe();
    }
  }

  //provide errors from model to MainActivity
  protected abstract void subscribeOnModelError();

  protected <T> void subscribe(Observable<T> observable, Subscriber<T> subscriber) {
    Subscription subscription = observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(subscriber);
    mSubscriptions.add(subscription);
  }

  protected abstract class ViewSubscriber<T> extends Subscriber<T> {
    @Override
    public void onCompleted() {
      Log.d(TAG, "onComplete presenter observable");
    }

    @Override
    public void onError(Throwable e) {
      if (getRootView() != null) {
        getRootView().showError(e);
      }
    }

    @Override
    public abstract void onNext(T t);
  }
}
