package com.hamom.mvpauth.mvp.presenters;

import android.support.annotation.Nullable;

import com.hamom.mvpauth.mvp.views.IAuthView;

/**
 * Created by hamom on 21.10.16.
 */

public interface IAuthPresenter {

    void clickOnLogin();
    void clickOnShowCatalog();
    void clickOnFb();
    void clickOnVk();
    void clickOnTwitter();

    boolean checkUserAuth();
}
