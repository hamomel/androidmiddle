package com.hamom.mvpauth.mvp.models;

import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import io.realm.RealmResults;
import rx.Observable;

/**
 * Created by hamom on 03.03.17.
 */

public class CartModel extends AbstractModel {
  public Observable<RealmResults<ProductRealm>> getCartProductsObs() {
    return mDataManager.getCartProductObs();
  }
}
