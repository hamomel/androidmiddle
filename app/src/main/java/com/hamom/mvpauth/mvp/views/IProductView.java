package com.hamom.mvpauth.mvp.views;

import com.hamom.mvpauth.data.storage.dto.ProductDto;

/**
 * Created by hamom on 28.10.16.
 */

public interface IProductView extends IView{
    void showProductView(ProductDto product);
    void updateProductCountView(ProductDto product);
}
