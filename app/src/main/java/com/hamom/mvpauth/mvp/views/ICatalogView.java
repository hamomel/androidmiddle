package com.hamom.mvpauth.mvp.views;

import com.hamom.mvpauth.data.storage.dto.ProductDto;

import java.util.List;

/**
 * Created by hamom on 30.10.16.
 */

public interface ICatalogView extends IView {
    void showCatalogView();
    void updateProductCounter();
}
