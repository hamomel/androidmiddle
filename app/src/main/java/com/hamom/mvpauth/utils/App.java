package com.hamom.mvpauth.utils;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;
import android.util.Log;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.di.components.AppComponent;
import com.hamom.mvpauth.di.components.DaggerAppComponent;
import com.hamom.mvpauth.di.modules.AppModule;
import com.hamom.mvpauth.di.modules.LocalModule;
import com.hamom.mvpauth.di.modules.NetworkModule;
import com.hamom.mvpauth.di.modules.PicassoCacheModule;
import com.hamom.mvpauth.di.modules.RootModule;
import com.hamom.mvpauth.mortar.ScreenScoper;
import com.hamom.mvpauth.ui.activities.DaggerRootActivity_RootActivityComponent;
import com.hamom.mvpauth.ui.activities.RootActivity;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import io.realm.Realm;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

/**
 * Created by hamom on 22.10.16.
 */

public class App extends Application {
  private static String TAG = ConstantManager.TAG_PREFIX + "App: ";
  private static AppComponent sAppComponent;
  private static RootActivity.RootActivityComponent mRootActivityComponent;
  private MortarScope mRootScope;
  private MortarScope mRootActivityScope;
  private RefWatcher mRefWatcher;
  private static Context mContext;

  public static AppComponent getAppComponent() {
    return sAppComponent;
  }

  @Override
  public Object getSystemService(String name) {
    if (mRootScope != null) {
      return mRootScope.hasService(name) ? mRootScope.getService(name)
          : super.getSystemService(name);
    } else {
      return super.getSystemService(name);
    }
  }

  @Override
  public void onCreate() {
    if (AppConfig.DEBUG) {
      StrictMode.setThreadPolicy(
          new StrictMode.ThreadPolicy.Builder()
              .detectAll()
              .penaltyLog()
              .build());
      StrictMode.setVmPolicy(
          new StrictMode.VmPolicy.Builder()
              .detectAll()
              .penaltyLog()
              .build());
    }
    super.onCreate();

    if (LeakCanary.isInAnalyzerProcess(this)) {
      // This process is dedicated to LeakCanary for heap analysis.
      // You should not init your app in this process.
      return;
    }
    mRefWatcher = LeakCanary.install(this);
    mContext = getApplicationContext();

    Log.d(TAG, "onCreate: Realm.init");
    Realm.init(this);

    createComponent();

    mRootScope = MortarScope.buildRootScope()
        .withService(DaggerService.SERVICE_NAME, sAppComponent)
        .build("Root");

    createRootActivityComponent();

    mRootActivityScope = mRootScope.buildChild()
        .withService(DaggerService.SERVICE_NAME, mRootActivityComponent)
        .withService(BundleServiceRunner.SERVICE_NAME, new BundleServiceRunner())
        .build(RootActivity.class.getName());

    ScreenScoper.registerScope(mRootScope);
    ScreenScoper.registerScope(mRootActivityScope);
  }

  private void createComponent() {
    sAppComponent = DaggerAppComponent.builder()
        .appModule(new AppModule(getApplicationContext(), mRefWatcher))
        .localModule(new LocalModule())
        .networkModule(new NetworkModule())
        .build();
  }

  private void createRootActivityComponent() {
    mRootActivityComponent = DaggerRootActivity_RootActivityComponent.builder()
        .appComponent(sAppComponent)
        .rootModule(new RootModule())
        .picassoCacheModule(new PicassoCacheModule())
        .build();
  }

  public static RootActivity.RootActivityComponent getRootActivityComponent() {
    return mRootActivityComponent;
  }

  public static Context getAppContext() {
    return mContext;
  }
}
