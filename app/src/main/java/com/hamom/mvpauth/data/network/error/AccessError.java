package com.hamom.mvpauth.data.network.error;

/**
 * Created by hamom on 13.03.17.
 */

public class AccessError extends Exception{

  public AccessError() {
    super("Не верный логин или пароль");
  }
}
