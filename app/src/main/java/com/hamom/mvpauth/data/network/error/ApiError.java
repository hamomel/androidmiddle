package com.hamom.mvpauth.data.network.error;

/**
 * Created by hamom on 17.12.16.
 */

public class ApiError extends Throwable {

  public ApiError(int statusCode) {
    super("Ошибка сервера " + statusCode);
  }
}
