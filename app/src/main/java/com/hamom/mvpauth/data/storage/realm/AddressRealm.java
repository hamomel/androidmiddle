package com.hamom.mvpauth.data.storage.realm;

import com.hamom.mvpauth.data.storage.dto.UserAddressDto;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by hamom on 11.01.17.
 */

public class AddressRealm extends RealmObject {
  @PrimaryKey
  private int id;
  private String mPlaceName;
  private String mStreet;
  private String mHome;
  private String mApartment;
  private int mStage;
  private String mComment;
  private boolean favorite;

  public AddressRealm() {
  }

  public AddressRealm(UserAddressDto dto) {
    this.id = dto.getId();
    this.mPlaceName = dto.getPlaceName();
    this.mStreet = dto.getStreet();
    this.mHome = dto.getHome();
    this.mApartment = dto.getApartment();
    this.mStage = dto.getStage();
    this.mComment = dto.getComment();
    this.favorite = dto.isFavorite();
  }

  public int getId() {
    return id;
  }

  public String getPlaceName() {
    return mPlaceName;
  }

  public String getStreet() {
    return mStreet;
  }

  public String getHome() {
    return mHome;
  }

  public String getApartment() {
    return mApartment;
  }

  public int getStage() {
    return mStage;
  }

  public String getComment() {
    return mComment;
  }

  public boolean isFavorite() {
    return favorite;
  }

  public void setId(int id){
    this.id = id;
  }

  public void fromDto(UserAddressDto dto) {
    this.mPlaceName = dto.getPlaceName();
    this.mStreet = dto.getStreet();
    this.mHome = dto.getHome();
    this.mApartment = dto.getApartment();
    this.mStage = dto.getStage();
    this.mComment = dto.getComment();
    this.favorite = dto.isFavorite();
  }
}
