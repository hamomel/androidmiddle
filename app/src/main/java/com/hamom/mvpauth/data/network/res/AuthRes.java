package com.hamom.mvpauth.data.network.res;

import io.realm.RealmObject;
import java.util.List;

/**
 * Created by hamom on 07.03.17.
 */


public class AuthRes {

  public String id;
  public String fullName;
  public String avatarUrl;
  public String token;
  public String phone;
  public List<Address> addresses = null;

  public String getId() {
    return id;
  }

  public String getFullName() {
    return fullName;
  }

  public String getAvatarUrl() {
    return avatarUrl;
  }

  public String getToken() {
    return token;
  }

  public String getPhone() {
    return phone;
  }

  public List<Address> getAddresses() {
    return addresses;
  }
}