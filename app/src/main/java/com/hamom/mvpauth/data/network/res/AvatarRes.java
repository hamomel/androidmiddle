package com.hamom.mvpauth.data.network.res;

/**
 * Created by hamomel on 27.01.17.
 */

public class AvatarRes {
  private String avatarUrl;

  public AvatarRes(String avatarUrl) {
    this.avatarUrl = avatarUrl;
  }

  public String getAvatarUrl() {
    return avatarUrl;
  }
}
