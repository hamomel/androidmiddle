package com.hamom.mvpauth.data.storage.dto;

import com.hamom.mvpauth.data.network.res.CommentRes;
import com.hamom.mvpauth.data.storage.realm.CommentRealm;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by hamom on 24.12.16.
 */

public class ProductCommentDto {
  public String id;
  public int remoteId;
  public String avatar;
  public String name;
  public float raiting;
  public String commentDate;
  public String comment;
  public boolean active;

  public ProductCommentDto(String id, int remoteId, String avatar, String name, int raiting, String commentDate,
      String comment, boolean active) {
    this.id = id;
    this.remoteId = remoteId;
    this.avatar = avatar;
    this.name = name;
    this.raiting = raiting;
    this.commentDate = commentDate;
    this.comment = comment;
    this.active = active;
  }

  public ProductCommentDto(CommentRealm commentRealm) {
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);
    this.active = commentRealm.isActive();
    this.avatar = commentRealm.getAvatar();
    this.comment = commentRealm.getComment();
    if (commentRealm.getCommentDate() != null) {
      this.commentDate = format.format(commentRealm.getCommentDate());
    }
    this.id = commentRealm.getId();
    this.name = commentRealm.getUserName();
    this.raiting = commentRealm.getRating();
  }

  public ProductCommentDto(CommentRes commentRes) {
    this.id = commentRes.getId();
    this.remoteId = commentRes.getRemoteId();
    this.avatar = commentRes.getAvatar();
    this.raiting = commentRes.getRaiting();
    this.commentDate = commentRes.getCommentDate();
    this.comment = commentRes.getComment();
    this.active = commentRes.isActive();
  }

  public String getId() {
    return id;
  }

  public int getRemoteId() {
    return remoteId;
  }

  public String getAvatar() {
    return avatar;
  }

  public String getName() {
    return name;
  }

  public float getRaiting() {
    return raiting;
  }

  public String getCommentDate() {
    return commentDate;
  }

  public String getComment() {
    return comment;
  }

  public boolean isActive() {
    return active;
  }
}
