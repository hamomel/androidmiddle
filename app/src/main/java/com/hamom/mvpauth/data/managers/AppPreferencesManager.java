package com.hamom.mvpauth.data.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import com.hamom.mvpauth.data.network.res.ProductRes;
import com.hamom.mvpauth.data.storage.dto.ProductDto;
import com.hamom.mvpauth.data.storage.dto.ProductLocalInfo;
import com.hamom.mvpauth.utils.ConstantManager;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by hamom on 22.10.16.
 */

public class AppPreferencesManager {
  private static String TAG = ConstantManager.TAG_PREFIX + "PrefManager: ";
  public static final String PROFILE_FULL_NAME_KEY = "PROFILE_FULL_NAME_KEY";
  public static final String PROFILE_AVATAR_KEY = "PROFILE_AVATAR_KEY";
  public static final String PROFILE_PHONE_KEY = "PROFILE_PHONE_KEY";
  public static final String NOTIFICATION_ORDER_KEY = "NOTIFICATION_ORDER_KEY";
  public static final String NOTIFICATION_PROMO_KEY = "NOTIFICATION_PROMO_KEY";
  public static final String AUTH_TOKEN_KEY = "AUTH_TOKEN_KEY";
  public static final String USER_ID_KEY = "USER_ID_KEY";
  private static final String PRODUCT_LAST_UPDATE_KEY = "PRODUCT_LAST_UPDATE_KEY";

  private SharedPreferences mSharedPreferences;

  SharedPreferences getSharedPreferences() {
    return mSharedPreferences;
  }

  public AppPreferencesManager(Context context) {
    mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
  }

  void saveAuthToken(String authToken) {
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.putString(AUTH_TOKEN_KEY, authToken);
    editor.apply();
  }

  String getAuthToken() {
    return mSharedPreferences.getString(AUTH_TOKEN_KEY, null);
  }

  public void saveUserId(String id) {
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.putString(USER_ID_KEY, id);
    editor.apply();
  }

  public String getUserId(){
    return mSharedPreferences.getString(USER_ID_KEY, "");
  }

  public String getLastProductUpdate() {
    //return "Fri, 22 Nov 1963 00:00:00 GMT";
    return mSharedPreferences.getString(PRODUCT_LAST_UPDATE_KEY, "Fri, 22 Nov 1963 00:00:00 GMT");
  }

  public void saveLastProductUpdate(String lastModified) {
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.putString(PRODUCT_LAST_UPDATE_KEY, lastModified);
    editor.apply();
  }

  public void saveProfileInfo(String name, String phone, String avatar) {
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.putString(PROFILE_FULL_NAME_KEY, name);
    editor.putString(PROFILE_PHONE_KEY, phone);
    editor.putString(PROFILE_AVATAR_KEY, avatar);
    editor.apply();
  }

  public Map<String, String> getUserProfileInfo() {
    Map<String, String> map = new HashMap<>();
    map.put(PROFILE_FULL_NAME_KEY, mSharedPreferences.getString(PROFILE_FULL_NAME_KEY, ""));
    map.put(PROFILE_PHONE_KEY, mSharedPreferences.getString(PROFILE_PHONE_KEY, ""));
    map.put(PROFILE_AVATAR_KEY, mSharedPreferences.getString(PROFILE_AVATAR_KEY, null));
    return map;
  }

  public void saveUserAvatar(String avatarUrl) {
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.putString(PROFILE_AVATAR_KEY, avatarUrl);
    editor.apply();
  }


}
