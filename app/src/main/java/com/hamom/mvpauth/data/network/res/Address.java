package com.hamom.mvpauth.data.network.res;

import io.realm.RealmObject;

/**
 * Created by hamom on 13.03.17.
 */

class Address {
  public String id;
  public String name;
  public String street;
  public String house;
  public String apartment;
  public int floor;
  public String comment;

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getStreet() {
    return street;
  }

  public String getHouse() {
    return house;
  }

  public String getApartment() {
    return apartment;
  }

  public int getFloor() {
    return floor;
  }

  public String getComment() {
    return comment;
  }
}
