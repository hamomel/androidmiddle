package com.hamom.mvpauth.data.managers;

import com.hamom.mvpauth.data.network.res.CommentRes;
import com.hamom.mvpauth.data.network.res.ProductRes;
import com.hamom.mvpauth.data.storage.dto.UserAddressDto;
import com.hamom.mvpauth.data.storage.realm.AddressRealm;
import com.hamom.mvpauth.data.storage.realm.CommentRealm;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;

import com.hamom.mvpauth.utils.ConstantManager;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import rx.Observable;

/**
 * Created by hamom on 09.01.2017.
 */

public class RealmManager {
  private static String TAG = ConstantManager.TAG_PREFIX + "RealmManager: ";
  private Realm mRealmInstance;

  public void saveProductResponseToRealm(ProductRes productRes) {
    Realm realm = Realm.getDefaultInstance();

    ProductRealm productRealm = new ProductRealm(productRes);

    if (!productRes.getComments().isEmpty()) {
      Observable.from(productRes.getComments())
          .doOnNext(commentRes -> {
            if (!commentRes.isActive()) {
              deleteFromRealm(CommentRealm.class, commentRes.getId());
            }
          })
          .filter(CommentRes::isActive)
          .map(CommentRealm::new)
          .subscribe(commentRealm -> productRealm.getComments().add(commentRealm));

      realm.executeTransaction(realm1 -> realm.insertOrUpdate(productRealm));
      realm.close();
    }
  }

  public void deleteFromRealm(Class<? extends RealmObject> entryRealmClass, String id) {
    Realm realm = Realm.getDefaultInstance();

    RealmObject entry = realm.where(entryRealmClass).equalTo("id", id).findFirst();

    if (entry != null) {
      realm.executeTransaction(realm1 -> entry.deleteFromRealm());
    }
    realm.close();
  }

  public void deleteFromRealm(Class<? extends RealmObject> entryRealmClass, int id) {
    Realm realm = Realm.getDefaultInstance();

    RealmObject entry = realm.where(entryRealmClass).equalTo("id", id).findFirst();

    if (entry != null) {
      realm.executeTransaction(realm1 -> entry.deleteFromRealm());
    }
    realm.close();
  }

  public Observable<ProductRealm> getAllProductsFromRealm() {
    RealmResults<ProductRealm> managedProduct =
        getQueryRealmInstance().where(ProductRealm.class).findAllAsync();

    return managedProduct.asObservable()
        .filter(RealmResults::isLoaded)
        .flatMap(Observable::from);
  }

  public Observable<RealmResults<ProductRealm>> getFavoriteProducts(){
    RealmResults<ProductRealm> managedProduct =
        getQueryRealmInstance().where(ProductRealm.class).equalTo("favorite", true).findAllAsync();

    return managedProduct.asObservable()
        .filter(RealmResults::isLoaded);

  }

  public Observable<AddressRealm> getUserAddressesFromRealm() {
    RealmResults<AddressRealm> managedAddresses =
        getQueryRealmInstance().where(AddressRealm.class).findAllAsync();

    return managedAddresses.asObservable()
        .filter(RealmResults::isLoaded)
        .first()
        .flatMap(Observable::from);
  }

  public void saveUserAddressToRealm(UserAddressDto dto) {
    Realm realm = Realm.getDefaultInstance();
    AddressRealm addressRealm = new AddressRealm(dto);

    realm.executeTransactionAsync(realm1 -> {
      int id = 0;
      for (int i = 0; i < Integer.MAX_VALUE; i++) {
        AddressRealm address = realm1.where(AddressRealm.class).equalTo("id", i).findFirst();
        if (address == null) {
          id = i;
          break;
        }
      }

      addressRealm.setId(id);
      realm1.copyToRealm(addressRealm);
    });
    realm.close();
  }

  public AddressRealm getUserAddressById(int id) {
    return getQueryRealmInstance().where(AddressRealm.class).equalTo("id", id).findFirst();
  }

  public Observable<RealmResults<ProductRealm>> getCartProductObs() {
    RealmResults<ProductRealm> results = getQueryRealmInstance()
        .where(ProductRealm.class)
        .equalTo("isInCart", true)
        .findAllAsync();

    return results.asObservable().filter(RealmResults::isLoaded);
  }

  private Realm getQueryRealmInstance() {
    if (mRealmInstance == null) {
      mRealmInstance = Realm.getDefaultInstance();
    }
    return mRealmInstance;
  }
}
