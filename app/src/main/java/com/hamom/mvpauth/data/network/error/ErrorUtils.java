package com.hamom.mvpauth.data.network.error;

import com.hamom.mvpauth.data.managers.DataManager;
import java.io.IOException;
import javax.inject.Inject;
import retrofit2.Response;

/**
 * Created by hamom on 17.12.16.
 */

public class ErrorUtils {
  private static DataManager sDataManager;

  @Inject
  public ErrorUtils(DataManager dataManager) {
    sDataManager = dataManager;
  }

  public static ApiError parseError(Response<?> response) {

    ApiError error;
    try {
      error = (ApiError) sDataManager
          .getRetrofit()
          .responseBodyConverter(ApiError.class, ApiError.class.getAnnotations())
          .convert(response.errorBody());
    } catch (IOException e){
      e.printStackTrace();
      return new ApiError(response.code());
    }
    return error;
  }
}
