package com.hamom.mvpauth.data.storage.realm;

import com.hamom.mvpauth.data.network.res.CommentRes;

import com.hamom.mvpauth.data.storage.dto.UserInfoDto;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import java.util.Locale;

/**
 * Created by hamom on 09.01.2017.
 */

public class CommentRealm extends RealmObject implements Serializable{
  @PrimaryKey
  private String id;
  private String userName;
  private String avatar;
  private String comment;
  private boolean isActive;
  private float rating;
  private Date commentDate;

  public CommentRealm() {
  }

  public CommentRealm(String userName, float rating, String comment){
    this.id = String.valueOf(this.hashCode());
    this.userName = userName;
    this.rating = rating;
    this.comment = comment;
    this.commentDate = new Date();
    this.isActive = true;
  }

  public CommentRealm(CommentRes commentRes){
    this.avatar = commentRes.getAvatar();
    this.comment = commentRes.getComment();
    this.id = commentRes.getId();
    this.isActive = commentRes.isActive();
    this.rating = commentRes.getRaiting();
    this.userName = commentRes.getUserName();

    DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);
    try {
      this.commentDate = format.parse(commentRes.getCommentDate());
    } catch (ParseException e){
      e.printStackTrace();
    }
  }

  public String getAvatar() {
    return avatar;
  }

  public String getComment() {
    return comment;
  }

  public Date getCommentDate() {
    return commentDate;
  }

  public String getId() {
    return id;
  }

  public float getRating() {
    return rating;
  }

  public String getUserName() {
    return userName;
  }

  public boolean isActive() {
    return isActive;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public void setActive(boolean active) {
    isActive = active;
  }

  public void setRating(float rating) {
    this.rating = rating;
  }

  public void setCommentDate(Date commentDate) {
    this.commentDate = commentDate;
  }
}
