package com.hamom.mvpauth.data.managers;

import android.util.Log;

import com.hamom.mvpauth.data.network.RestCallTransformer;
import com.hamom.mvpauth.data.network.RestService;
import com.hamom.mvpauth.data.network.error.AccessError;
import com.hamom.mvpauth.data.network.error.ApiError;
import com.hamom.mvpauth.data.network.req.AuthReq;
import com.hamom.mvpauth.data.network.res.AuthRes;
import com.hamom.mvpauth.data.network.res.AvatarRes;
import com.hamom.mvpauth.data.network.res.CommentRes;
import com.hamom.mvpauth.data.network.res.ProductRes;
import com.hamom.mvpauth.data.storage.dto.UserAddressDto;
import com.hamom.mvpauth.data.storage.realm.AddressRealm;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import com.hamom.mvpauth.utils.AppConfig;
import com.hamom.mvpauth.utils.ConstantManager;

import com.hamom.mvpauth.utils.NetworkStatusChecker;
import io.realm.RealmResults;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

import javax.inject.Singleton;
import okhttp3.MultipartBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by hamom on 22.10.16.
 */
@Singleton
public class DataManager {
  private static String TAG = ConstantManager.TAG_PREFIX + "DataManager: ";

  AppPreferencesManager mAppPreferencesManager;
  RestService mRestService;
  Retrofit mRetrofit;
  RealmManager mRealmManager;

  @Inject
  public DataManager(AppPreferencesManager appPreferencesManager, RestService restService,
      Retrofit retrofit, RealmManager realmManager) {
    mAppPreferencesManager = appPreferencesManager;
    mRestService = restService;
    mRetrofit = retrofit;
    mRealmManager = realmManager;
    Log.d(TAG, "DataManager: ");
    updateLocalDataWithTimer();
  }

  //only for tests
  public DataManager(RestService restService){
    mRestService = restService;
  }

  public AppPreferencesManager getAppPreferencesManager() {
    return mAppPreferencesManager;
  }

  public Retrofit getRetrofit() {
    return mRetrofit;
  }

  //region===================== User ==========================

  public Map<String, String> getUserProfileInfo() {
    return mAppPreferencesManager.getUserProfileInfo();
  }

  public void saveProfileInfo(String name, String phone, String avatar) {
    mAppPreferencesManager.saveProfileInfo(name, phone, avatar);
  }
  //endregion

  //region===================== Settings, Token ==========================
  public Map<String, Boolean> getUserSettings() {
    Map<String, Boolean> map = new HashMap<>();
    map.put(AppPreferencesManager.NOTIFICATION_ORDER_KEY,
        mAppPreferencesManager.getSharedPreferences()
            .getBoolean(AppPreferencesManager.NOTIFICATION_ORDER_KEY, true));

    map.put(AppPreferencesManager.NOTIFICATION_PROMO_KEY,
        mAppPreferencesManager.getSharedPreferences()
            .getBoolean(AppPreferencesManager.NOTIFICATION_PROMO_KEY, false));
    return map;
  }

  public void saveSettings(String notificationKey, boolean isChecked) {
    mAppPreferencesManager.getSharedPreferences()
        .edit()
        .putBoolean(notificationKey, isChecked)
        .apply();
  }

  public boolean isUserAuth() {
    return mAppPreferencesManager.getAuthToken() != null;
  }

  public void saveAuthToken(String token) {
    mAppPreferencesManager.saveAuthToken(token);
  }

  public void saveUserId(String id) {
    mAppPreferencesManager.saveUserId(id);
  }
  //endregion

  //region===================== Realm ==========================

  public Observable<RealmResults<ProductRealm>> getFavoriteProductsFromRealm(){
    return mRealmManager.getFavoriteProducts();
  }

  public Observable<ProductRealm> getProductFromRealm() {
    return mRealmManager.getAllProductsFromRealm();
  }

  public Observable<AddressRealm> getUserAddressesFromRealm() {
    return mRealmManager.getUserAddressesFromRealm();
  }

  public void removeAddressFromRealm(int id) {
    mRealmManager.deleteFromRealm(AddressRealm.class, id);
  }

  public AddressRealm getUserAddressById(int id) {
    return mRealmManager.getUserAddressById(id);
  }

  public void saveUserAddressToRealm(UserAddressDto userAddress) {
    mRealmManager.saveUserAddressToRealm(userAddress);
  }

  public Observable<RealmResults<ProductRealm>> getCartProductObs() {
    return mRealmManager.getCartProductObs();
  }

  //endregion

  //region===================== Network ==========================
  public Observable<ProductRealm> getProductObsFromNetwork() {
    return mRestService.getProductResObs(mAppPreferencesManager.getLastProductUpdate())
        .compose(new RestCallTransformer<>(this))//трансформируем response выбрасываем ApiError
        .flatMap(Observable::from) //преобразуем список в последовательность
        .subscribeOn(Schedulers.newThread())
        .observeOn(Schedulers.io())
        .doOnNext(productRes -> {
          if (!productRes.isActive()) {
            mRealmManager.deleteFromRealm(ProductRealm.class, productRes.getId());
          }
        })
        .filter(ProductRes::isActive) //пропускаем только активные товары
        .doOnNext(productRes -> mRealmManager.saveProductResponseToRealm(
            productRes))//сохраняем только активные товары
        .retryWhen(errorObservable -> errorObservable
            .zipWith(Observable.range(1, AppConfig.RETRY_REQUEST_COUNT),
                (throwable, retryCount) -> retryCount)
            .doOnNext(retryCount -> Log.d(TAG,
                "LOCAL UPDATE request retry count: " + retryCount + " " + new Date()))
            .map(retryCount -> ((long) (AppConfig.RETRY_REQUEST_BASE_DELAY * Math.pow(Math.E, retryCount))))
            .doOnNext(delay -> Log.d(TAG, "LOCAL UPDATE delay: "))
            .flatMap(delay -> Observable.timer(delay, TimeUnit.MILLISECONDS)))
        .flatMap(productRes -> Observable.empty());
  }

  public Observable<CommentRes> sendComment(String productId, CommentRes commentRes) {
    return mRestService.sendComment(productId, commentRes);
  }

  public Observable<AvatarRes> uploadAvatar(MultipartBody.Part body) {
    return mRestService.uploadAvatar(body);
  }

  public void updateLocalDataWithTimer() {
    Observable.interval(AppConfig.UPDATE_DATA_INTERVAL, TimeUnit.SECONDS)
        .flatMap(aLong -> NetworkStatusChecker.isInternetAvailable())
        .filter(aBoolean -> true)
        .flatMap(aBoolean -> getProductObsFromNetwork())
        .subscribe(productRealm -> Log.d(TAG, "LOCAL UPDATE COMPLETE: "),
            throwable -> {
              throwable.printStackTrace();
              Log.d(TAG, "LOCAL UPDATE ERROR: " + throwable.getMessage());
        });
  }

  public Observable<AuthRes> loginUser(AuthReq req){
    return mRestService.loginUser(req)
        .flatMap(response -> {
          if (response.code() == 200){
            return Observable.just(response.body());
          } else {
            if (response.code() == 403){
              return Observable.error(new AccessError());
            } else {
              return Observable.error(new ApiError(response.code()));
            }
          }
        });
  }

  public Observable<Response<Void>> sendFavorite(Set<String> favoriteProducts){
    return mRestService.sendFavorite(mAppPreferencesManager.getUserId(),
        mAppPreferencesManager.getAuthToken(),favoriteProducts);
  }

  public Observable<Response<List<String>>> getFavoriteProducts(){
    return mRestService.getFavoriteProducts(mAppPreferencesManager.getUserId());
  }

  public Observable<Response<Void>> deleteFavorite(Set<String> favoriteProducts){
    return mRestService.deleteFavoriteProducts(mAppPreferencesManager.getUserId(),
        mAppPreferencesManager.getAuthToken(), favoriteProducts);
  }
  //endregion
}
