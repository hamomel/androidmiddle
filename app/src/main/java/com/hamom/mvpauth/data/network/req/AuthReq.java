package com.hamom.mvpauth.data.network.req;

/**
 * Created by hamom on 13.03.17.
 */

public class AuthReq {
  private String email;
  private String password;

  public AuthReq(String email, String password) {
    this.email = email;
    this.password = password;
  }
}
