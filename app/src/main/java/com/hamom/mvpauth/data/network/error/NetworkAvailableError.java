package com.hamom.mvpauth.data.network.error;

/**
 * Created by hamom on 17.12.16.
 */

public class NetworkAvailableError extends Throwable {
  public NetworkAvailableError() {
    super("Сеть не доступна, попробуйте позже");
  }
}
