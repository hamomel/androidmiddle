package com.hamom.mvpauth.data.network;

import com.hamom.mvpauth.data.network.req.AuthReq;
import com.hamom.mvpauth.data.network.res.AuthRes;
import com.hamom.mvpauth.data.network.res.AvatarRes;
import com.hamom.mvpauth.data.network.res.CommentRes;
import com.hamom.mvpauth.data.network.res.ProductRes;
import com.hamom.mvpauth.utils.ConstantManager;
import java.util.List;
import java.util.Set;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by hamom on 04.11.16.
 */
public interface RestService {
  @GET("products")
  Observable<Response<List<ProductRes>>> getProductResObs(
      @Header(ConstantManager.IF_MODIFIED_SINCE_HEADER) String lastEntityUpdate);

  @POST("products/{productId}/comments")
  Observable<CommentRes> sendComment(@Path("productId") String productId, @Body CommentRes commentRes);

  @Multipart
  @POST("avatar")
  Observable<AvatarRes> uploadAvatar(@Part MultipartBody.Part file);

  @POST("login")
  Observable<Response<AuthRes>> loginUser(@Body AuthReq req);

  @POST("user/{userId}/favorite")
  Observable<Response<Void>> sendFavorite(@Path("userId") String userId,
      @Header(ConstantManager.USER_ID_HEADER) String userToken,
      @Body Set<String> favoriteProducts);

  @GET("user/{userId}/favorite")
  Observable<Response<List<String>>> getFavoriteProducts(@Path("userId") String userId);


  @HTTP(method = "DELETE", path = "user/{userId}/favorite", hasBody = true)
  Observable<Response<Void>> deleteFavoriteProducts(@Path("userId") String userId,
      @Header(ConstantManager.USER_ID_HEADER) String userToken,
      @Body Set<String> favoriteProducts);
}
