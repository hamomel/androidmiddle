package com.hamom.mvpauth.data.storage.realm;

import com.hamom.mvpauth.data.network.res.ProductRes;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by hamom on 09.01.2017.
 */

public class ProductRealm extends RealmObject {
  @PrimaryKey
  private String id;
  private String productName;
  private String productDescription;
  private String imageUrl;
  private int price;
  private float rating;
  private int count = 1;
  private boolean favorite;



  private boolean isInCart;
  private RealmList<CommentRealm> comments = new RealmList<>();

  public ProductRealm() {
  }

  public ProductRealm(ProductRes productRes) {
    this.id = productRes.getId();
    this.imageUrl = productRes.getImageUrl();
    this.price = productRes.getPrice();
    this.productDescription = productRes.getDescription();
    this.productName = productRes.getProductName();
    this.rating = productRes.getRaiting();
  }

  public RealmList<CommentRealm> getComments() {
    return comments;
  }

  public int getCount() {
    return count;
  }

  public boolean isFavorite() {
    return favorite;
  }

  public String getId() {
    return id;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public int getPrice() {
    return price;
  }

  public String getProductDescription() {
    return productDescription;
  }

  public String getProductName() {
    return productName;
  }

  public float getRating() {
    return rating;
  }

  public void add() {
    count++;
  }

  public void setCount(int count){
    this.count = count;
  }

  public void remove() {
    if (count > 0) count--;
  }

  public void changeFavorite() {
    if (favorite) {
      favorite = false;
    } else {
      favorite = true;
    }
  }

  public boolean isInCart() {
    return isInCart;
  }

  public void addToCart(){
    isInCart = true;
  }

  public void deleteFromCart(){
    isInCart = false;
  }

  public void addComment(CommentRealm comment) {
    comments.add(comment);
  }
}
