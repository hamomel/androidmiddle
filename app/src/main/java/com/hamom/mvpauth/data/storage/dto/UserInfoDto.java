package com.hamom.mvpauth.data.storage.dto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by hamom on 10.12.16.
 */

public class UserInfoDto implements Parcelable {
  String name;
  String phone;
  String avatar;

  public UserInfoDto(String name, String phone, String avatar) {
    this.name = name;
    this.phone = phone;
    this.avatar = avatar;
  }

  protected UserInfoDto(Parcel in) {
    name = in.readString();
    phone = in.readString();
    avatar = in.readString();
  }

  public static final Creator<UserInfoDto> CREATOR = new Creator<UserInfoDto>() {
    @Override
    public UserInfoDto createFromParcel(Parcel in) {
      return new UserInfoDto(in);
    }

    @Override
    public UserInfoDto[] newArray(int size) {
      return new UserInfoDto[size];
    }
  };

  public String getName() {
    return name;
  }

  public String getPhone() {
    return phone;
  }

  public String getAvatar() {
    return avatar;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(name);
    dest.writeString(phone);
    dest.writeString(avatar);
  }
}
