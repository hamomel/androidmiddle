package com.hamom.mvpauth.data.storage.dto;

import android.os.Parcel;
import android.os.Parcelable;
import com.hamom.mvpauth.data.network.res.CommentRes;
import com.hamom.mvpauth.data.network.res.ProductRes;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hamom on 28.10.16.
 */
public class ProductDto implements Parcelable {
  private int id;
  private String remoteId;
  private String productName;
  private String imageUrl;
  private String description;
  private int price;
  private int count;
  private float raiting;
  private boolean favorite;
  private List<ProductCommentDto> comments;



  public ProductDto() {
    this.id = 0;
    this.remoteId = "";
    this.productName = "";
    this.imageUrl = "";
    this.description = "";
    this.price = 0;
    this.count = 0;
    this.raiting = 0f;
    this.favorite = false;
    this.comments = new ArrayList<>();
  }

  public ProductDto(int id, String productName, String imageUrl, String description, int price,
      int count, float raiting, boolean favorite, List<ProductCommentDto> comments) {
    this.id = id;
    this.productName = productName;
    this.imageUrl = imageUrl;
    this.description = description;
    this.price = price;
    this.count = count;
    this.raiting = raiting;
    this.favorite = favorite;
    this.comments = comments;
  }

  public ProductDto(ProductRes productRes, ProductLocalInfo productLocalInfo) {
    this.id = productRes.getRemoteId();
    this.remoteId = productRes.getId();
    this.productName = productRes.getProductName();
    this.imageUrl = productRes.getImageUrl();
    this.description = productRes.getDescription();
    this.price = productRes.getPrice();
    this.raiting = productRes.getRaiting();
    if (productLocalInfo != null) {
      this.count = productLocalInfo.getCount();
      this.favorite = productLocalInfo.isFavorite();
    }
    this.comments = fromCommentRes(productRes.getComments());
  }

  public ProductDto(ProductRealm productRealm) {
    this.comments = new ArrayList<>();
    this.count = productRealm.getCount();
    this.description = productRealm.getProductDescription();
    this.favorite = productRealm.isFavorite();
    this.imageUrl = productRealm.getImageUrl();
    this.price = productRealm.getPrice();
    this.productName = productRealm.getProductName();
    this.raiting = productRealm.getRating();
    this.remoteId = productRealm.getId();
  }

  protected ProductDto(Parcel in) {
    id = in.readInt();
    remoteId = in.readString();
    productName = in.readString();
    imageUrl = in.readString();
    description = in.readString();
    price = in.readInt();
    count = in.readInt();
    favorite = in.readByte() != 0;
  }



  private List<ProductCommentDto> fromCommentRes(List<CommentRes> comments) {
    List<ProductCommentDto> list = new ArrayList<>();
    for (CommentRes comment : comments) {
      list.add(new ProductCommentDto(comment));
    }
    return list;
  }

  public void setLocalInfo(ProductLocalInfo productLocalInfo) {
    this.favorite = productLocalInfo.isFavorite();
    this.count = productLocalInfo.getCount();
  }

  //region===============Parcelable==========================
  public static final Creator<ProductDto> CREATOR = new Creator<ProductDto>() {
    @Override
    public ProductDto createFromParcel(Parcel in) {
      return new ProductDto(in);
    }

    @Override
    public ProductDto[] newArray(int size) {
      return new ProductDto[size];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(id);
    dest.writeString(remoteId);
    dest.writeString(productName);
    dest.writeString(imageUrl);
    dest.writeString(description);
    dest.writeInt(price);
    dest.writeInt(count);
    dest.writeFloat(raiting);
    dest.writeByte((byte) (favorite ? 1 : 0));
  }
  //endregion

  //region=======Getters=============================
  public int getCount() {
    return count;
  }

  public int getPrice() {
    return price;
  }

  public String getDescription() {
    return description;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public String getProductName() {
    return productName;
  }

  public int getId() {
    return id;
  }

  public String getRemoteId() {
    return remoteId;
  }

  public boolean isFavorite() {
    return favorite;
  }

  public float getRaiting() {
    return raiting;
  }

  public List<ProductCommentDto> getComments() {
    return comments;
  }
  //endregion

  public void deleteProduct() {
    if (count > 0) count--;
  }

  public void addProduct() {
    count++;
  }


  public void addComment(ProductCommentDto dto) {
    comments.add(dto);
  }
}
