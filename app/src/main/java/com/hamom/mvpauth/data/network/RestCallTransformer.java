package com.hamom.mvpauth.data.network;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.hamom.mvpauth.data.managers.DataManager;
import com.hamom.mvpauth.data.network.error.ErrorUtils;
import com.hamom.mvpauth.data.network.error.NetworkAvailableError;
import com.hamom.mvpauth.utils.ConstantManager;
import com.hamom.mvpauth.utils.NetworkStatusChecker;
import javax.inject.Inject;
import retrofit2.Response;
import rx.Observable;

/**
 * Created by hamom on 17.12.16.
 */

public class RestCallTransformer<R> implements Observable.Transformer<Response<R>, R> {
  private DataManager mDataManager;

  public RestCallTransformer(DataManager dataManager) {
    mDataManager = dataManager;
  }

  @Override
  @RxLogObservable
  public Observable<R> call(Observable<Response<R>> responseObservable) {
    return NetworkStatusChecker.isInternetAvailable()
        .flatMap(aBoolean -> aBoolean ? responseObservable : Observable.error(new NetworkAvailableError()))
        .flatMap(rResponse -> {
          switch (rResponse.code()){
            case 200:
              String lastModified = rResponse.headers().get(ConstantManager.LAST_MODIFIED_HEADER);
              if (lastModified != null) {
                mDataManager.getAppPreferencesManager().saveLastProductUpdate(lastModified);
              }
              return Observable.just(rResponse.body());
            case 304:
              return Observable.empty();
            default:
              return Observable.error(ErrorUtils.parseError(rResponse));
          }
        });
  }
}
