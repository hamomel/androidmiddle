package com.hamom.mvpauth.data.storage.dto;

/**
 * Created by hamom on 17.12.16.
 */

public class ProductLocalInfo {
  private String remoteId;
  private boolean favorite;
  private int count;

  public ProductLocalInfo() {
  }

  public ProductLocalInfo(String remoteId, boolean favorite, int count) {
    this.remoteId = remoteId;
    this.favorite = favorite;
    this.count = count;
  }

  public String getRemoteId() {
    return remoteId;
  }

  public boolean isFavorite() {
    return favorite;
  }

  public int getCount() {
    return count;
  }

  public void addCount(){
    count++;
  }

  public void deleteCount(){
    count--;
  }

  public void setRemoteId(String remoteId) {
    this.remoteId = remoteId;
  }
}
