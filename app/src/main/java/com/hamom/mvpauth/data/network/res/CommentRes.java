package com.hamom.mvpauth.data.network.res;

import com.hamom.mvpauth.data.managers.AppPreferencesManager;
import com.hamom.mvpauth.data.storage.dto.UserInfoDto;
import com.hamom.mvpauth.data.storage.realm.CommentRealm;
import com.hamom.mvpauth.utils.App;
import com.squareup.moshi.Json;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Map;

/**
 * Created by hamom on 17.12.16.
 */
public class CommentRes {
  @Json(name = "_id")
  private String id;
  private int remoteId;
  private String avatar;
  private float raiting;
  private String commentDate;
  private String comment;
  private boolean active;
  private String userName;

  public CommentRes(String id, int remoteId, String avatar, int raiting, String commentDate,
      String comment, boolean active, String userName) {
    this.id = id;
    this.remoteId = remoteId;
    this.avatar = avatar;
    this.raiting = raiting;
    this.commentDate = commentDate;
    this.comment = comment;
    this.active = active;
    this.userName = userName;
  }

  public CommentRes(CommentRealm commentRealm) {
    this.userName = commentRealm.getUserName();
    this.comment = commentRealm.getComment();
    this.raiting = commentRealm.getRating();
    this.active = true;

    Map<String, String> userInfoDto = App.getAppComponent().getDataManger().getUserProfileInfo();
    this.avatar = userInfoDto.get(AppPreferencesManager.PROFILE_AVATAR_KEY);
    this.userName = userInfoDto.get(AppPreferencesManager.PROFILE_FULL_NAME_KEY);

    DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);
    this.commentDate = format.format(commentRealm.getCommentDate());

  }

  public String getId() {
    return id;
  }

  public int getRemoteId() {
    return remoteId;
  }

  public String getAvatar() {
    return avatar;
  }

  public float getRaiting() {
    return raiting;
  }

  public String getCommentDate() {
    return commentDate;
  }

  public String getComment() {
    return comment;
  }

  public boolean isActive() {
    return active;
  }

  public String getUserName() {
    return userName;
  }
}
