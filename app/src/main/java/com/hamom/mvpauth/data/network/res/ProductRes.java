package com.hamom.mvpauth.data.network.res;

import com.squareup.moshi.Json;

import java.util.List;

/**
 * Created by hamom on 17.12.16.
 */

public class ProductRes {
  @Json(name = "_id")
  private String id;
  private int remoteId;
  private String productName;
  private String imageUrl;
  private String description;
  private int price;
  private float raiting;
  private boolean active;
  private List<CommentRes> comments = null;

  public ProductRes(String id, int remoteId, String productName, String imageUrl,
      String description, int price, float raiting, boolean active, List<CommentRes> comments) {
    this.id = id;
    this.remoteId = remoteId;
    this.productName = productName;
    this.imageUrl = imageUrl;
    this.description = description;
    this.price = price;
    this.raiting = raiting;
    this.active = active;
    this.comments = comments;
  }

  public String getId() {
    return id;
  }

  public int getRemoteId() {
    return remoteId;
  }

  public String getProductName() {
    return productName;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public String getDescription() {
    return description;
  }

  public int getPrice() {
    return price;
  }

  public float getRaiting() {
    return raiting;
  }

  public boolean isActive() {
    return active;
  }

  public List<CommentRes> getComments() {
    return comments;
  }
}
