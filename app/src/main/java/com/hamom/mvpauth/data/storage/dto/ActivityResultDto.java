package com.hamom.mvpauth.data.storage.dto;

import android.content.Intent;
import android.support.annotation.Nullable;

/**
 * Created by hamom on 09.12.16.
 */

public class ActivityResultDto {
    int resultCode;
    int requestCode;
    @Nullable
    Intent data;

    public ActivityResultDto(int requestCode, int resultCode, Intent data) {
        this.resultCode = resultCode;
        this.requestCode = requestCode;
        this.data = data;
    }

    public int getResultCode() {
        return resultCode;
    }

    public int getRequestCode() {
        return requestCode;
    }

    @Nullable
    public Intent getIntent() {
        return data;
    }
}
