package com.hamom.mvpauth.data.storage.dto;

import android.os.Parcel;
import android.os.Parcelable;
import com.hamom.mvpauth.data.storage.realm.AddressRealm;

/**
 * Created by hamom on 10.11.16.
 */
public class UserAddressDto implements Parcelable {

  private int id;
  private String mPlaceName;
  private String mStreet;
  private String mHome;
  private String mApartment;
  private int mStage;
  private String mComment;
  private boolean favorite;

  public UserAddressDto(int addressId, String comment, int stage, String apartment, String home,
      String street, String placeName) {
    id = addressId;
    mComment = comment;
    mStage = stage;
    mApartment = apartment;
    mHome = home;
    mStreet = street;
    mPlaceName = placeName;
  }

  public UserAddressDto(AddressRealm addressRealm) {
    this.id = addressRealm.getId();
    mPlaceName = addressRealm.getPlaceName();
    mStreet = addressRealm.getStreet();
    mHome = addressRealm.getHome();
    mApartment = addressRealm.getApartment();
    mStage = addressRealm.getStage();
    mComment = addressRealm.getComment();
    this.favorite = addressRealm.isFavorite();
  }

  protected UserAddressDto(Parcel in) {
    id = in.readInt();
    mPlaceName = in.readString();
    mStreet = in.readString();
    mHome = in.readString();
    mApartment = in.readString();
    mStage = in.readInt();
    mComment = in.readString();
    favorite = in.readByte() != 0;
  }

  //region===================== Parcelable ==========================
  public static final Creator<UserAddressDto> CREATOR = new Creator<UserAddressDto>() {
    @Override
    public UserAddressDto createFromParcel(Parcel in) {
      return new UserAddressDto(in);
    }

    @Override
    public UserAddressDto[] newArray(int size) {
      return new UserAddressDto[size];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(id);
    dest.writeString(mPlaceName);
    dest.writeString(mStreet);
    dest.writeString(mHome);
    dest.writeString(mApartment);
    dest.writeInt(mStage);
    dest.writeString(mComment);
    dest.writeByte((byte) (favorite ? 1 : 0));
  }
  //endregion

  public String getPlaceName() {
    return mPlaceName;
  }

  public String getStreet() {
    return mStreet;
  }

  public int getId() {
    return id;
  }

  public String getHome() {
    return mHome;
  }

  public String getApartment() {
    return mApartment;
  }

  public int getStage() {
    return mStage;
  }

  public String getComment() {
    return mComment;
  }

  public void setId(int id) {
    this.id = id;
  }

  public boolean isFavorite() {
    return favorite;
  }
}
