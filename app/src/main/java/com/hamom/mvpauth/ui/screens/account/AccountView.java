package com.hamom.mvpauth.ui.screens.account;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.dto.UserDto;
import com.hamom.mvpauth.data.storage.dto.UserInfoDto;
import com.hamom.mvpauth.data.storage.dto.UserSettingsDto;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.mvp.views.AbstractView;
import com.hamom.mvpauth.mvp.views.IAccountView;
import com.hamom.mvpauth.utils.ConstantManager;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;
import flow.Flow;
import javax.inject.Inject;

/**
 * Created by hamom on 28.11.16.
 */

public class AccountView extends AbstractView<AccountScreen.AccountPresenter> implements IAccountView {
  private static String TAG = ConstantManager.TAG_PREFIX + "AccountView: ";
  public static final int PREVIEW_STATE = 1;
  public static final int EDIT_STATE = 0;
  @Inject
  AccountScreen.AccountPresenter mPresenter;
  @Inject
  Picasso mPicasso;

  @BindView(R.id.avatar_iw)
  CircleImageView mAvatarIv;
  @BindView(R.id.name_et)
  EditText mNameEt;
  @BindView(R.id.phone_et)
  EditText mPhoneEt;
  @BindView(R.id.name_header_tv)
  TextView mNameHeaderTxt;
  @BindView(R.id.name_container)
  LinearLayout mNameContainer;
  @BindView(R.id.main_fields_container)
  LinearLayout mMainFieldsContainer;
  @BindView(R.id.address_list)
  RecyclerView mAddressList;
  @BindView(R.id.order_sw)
  Switch mOrderSw;
  @BindView(R.id.promo_sw)
  Switch mPromoSw;

  private AccountScreen mScreen;
  private UserDto mUserDto;
  private TextWatcher mWatcher;
  private AddressAdapter mAddressAdapter;
  private Uri mAvatarUri;

  public AccountView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  protected void initDagger(Context context) {
    if (!isInEditMode()) {
      mScreen = Flow.getKey(this);
      DaggerService.<AccountScreen.Component>getDaggerComponent(context).inject(this);
    }
  }

  public AddressAdapter getAddressAdapter() {
    return mAddressAdapter;
  }

  private void showViewFromState() {
    if (mScreen.getCustomState() == PREVIEW_STATE) {
      showPreviewState();
    } else {
      showEditState();
    }
  }

  @Override
  public boolean viewOnBackPressed() {
    if (mScreen.getCustomState() == EDIT_STATE) {
      mPresenter.switchViewState();
      return true;
    } else {
      return false;
    }
  }

  //region===================== InitView ==========================
  public void initView() {
    initAddressList();
    showViewFromState();
  }

  public void initSettings(UserSettingsDto settings) {
    CompoundButton.OnCheckedChangeListener listener =
        (buttonView, isChecked) -> mPresenter.switchSettings();
    mOrderSw.setChecked(settings.isOrderNotification());
    mOrderSw.setOnCheckedChangeListener(listener);
    mPromoSw.setChecked(settings.isPromoNotification());
    mPromoSw.setOnCheckedChangeListener(listener);
  }

  private void initAddressList() {
    LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
    mAddressAdapter = new AddressAdapter();
    mAddressList.setLayoutManager(layoutManager);
    mAddressList.setAdapter(mAddressAdapter);
    initItemTouchHelper();
  }

  private void initItemTouchHelper() {
    MyItemTouchHelperCallback callback = new MyItemTouchHelperCallback(getContext(), 0,
        ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {
      @Override
      public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
          RecyclerView.ViewHolder target) {
        return false;
      }

      @Override
      public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        int position = viewHolder.getAdapterPosition();
        int id = mAddressAdapter.getItemIntId(position);
        if (direction == ItemTouchHelper.RIGHT) {
          showEditAddressDialog(id);
        } else {
          showRemoveAddressDialog(id);
        }
      }
    };

    ItemTouchHelper helper = new ItemTouchHelper(callback);
    helper.attachToRecyclerView(mAddressList);
  }

  //endregion

  private void showEditAddressDialog(int id) {
    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
    dialogBuilder.setTitle(R.string.go_edit_address)
        .setPositiveButton(R.string.yes,
            (dialog, which) -> mPresenter.onRightAddressSwipe(id))
        .setNegativeButton(R.string.cancel, (dialog, which) -> dialog.cancel())
        .setOnCancelListener(dialog -> mAddressAdapter.notifyDataSetChanged())
        .show();
  }

  private void showRemoveAddressDialog(int id) {
    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
    dialogBuilder.setTitle(R.string.remove_address)
        .setPositiveButton(R.string.yes, (dialog, which) -> mPresenter.removeAddress(id))
        .setNegativeButton(R.string.cancel, (dialog, which) -> dialog.cancel())
        .setOnCancelListener(dialog -> mAddressAdapter.notifyDataSetChanged())
        .show();
  }

  //region===================== IAccountView ==========================

  @Override
  public void showPhotoSourceDialog() {
    String source[] = {
        getContext().getString(R.string.load_from_gallery),
        getContext().getString(R.string.take_photo), getContext().getString(R.string.cancel)
    };
    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
    alertDialog.setTitle(R.string.set_photo);
    alertDialog.setItems(source, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        switch (which) {
          case 0:
            mPresenter.chooseGallery();
            break;
          case 1:
            mPresenter.chooseCamera();
            break;
          case 2:
            dialog.cancel();
            break;
        }
      }
    });
    alertDialog.show();
  }

  @Override
  public void changeState() {
    if (mScreen.getCustomState() == PREVIEW_STATE) {
      mScreen.setCustomState(EDIT_STATE);
    } else {
      mScreen.setCustomState(PREVIEW_STATE);
    }
    showViewFromState();
  }

  @Override
  public void showEditState() {
    mWatcher = new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        mNameHeaderTxt.setText(s);
      }

      @Override
      public void afterTextChanged(Editable s) {

      }
    };
    mMainFieldsContainer.setDescendantFocusability(FOCUS_BEFORE_DESCENDANTS);
    mNameContainer.setVisibility(VISIBLE);
    mNameEt.setEnabled(true);
    mNameEt.addTextChangedListener(mWatcher);
    mPhoneEt.setEnabled(true);
    mPicasso.load(R.drawable.ic_add_white_24dp).error(R.drawable.ic_add_white_24dp).into(mAvatarIv);
    mAvatarIv.setOnClickListener(v -> mPresenter.takePhoto());
  }

  @Override
  public void showPreviewState() {
    mMainFieldsContainer.setDescendantFocusability(FOCUS_BLOCK_DESCENDANTS);
    mNameContainer.setVisibility(GONE);
    mNameEt.removeTextChangedListener(mWatcher);
    mNameEt.setEnabled(false);
    mPhoneEt.setEnabled(false);
    updateAvatarImage(mAvatarUri);
    mAvatarIv.setOnClickListener(null);
  }

   //endregion
  
  public UserSettingsDto getSettings() {
    return new UserSettingsDto(mOrderSw.isChecked(), mPromoSw.isChecked());
  }

  public UserInfoDto getUserProfileInfo() {
    return new UserInfoDto(mNameEt.getText().toString(), mPhoneEt.getText().toString(),
        String.valueOf(mAvatarUri));
  }

  public void updateAvatarImage(Uri avatarUri) {
    Log.d(TAG, "updateAvatarImage: " + avatarUri);
    mAvatarUri = avatarUri;
    mPicasso.load(mAvatarUri).resize(136, 136).centerCrop().into(mAvatarIv);
  }

  public void updateProfileInfo(UserInfoDto userInfoDto) {
    Log.d(TAG, "updateProfileInfo: ");
    mNameHeaderTxt.setText(userInfoDto.getName());
    mNameEt.setText(userInfoDto.getName());
    mPhoneEt.setText(userInfoDto.getPhone());
    if (mScreen.getCustomState() == PREVIEW_STATE){
      updateAvatarImage(Uri.parse(userInfoDto.getAvatar()));

    }
  }

  //region===================== Events ==========================
  @OnClick(R.id.add_address_btn)
  void clickAddAddressBtn() {
    mPresenter.clickOnAddAddress();
  }

  //endregion
}
