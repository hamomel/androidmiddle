package com.hamom.mvpauth.ui.custom_views.behaviors;

import android.content.Context;
import android.os.Parcelable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.AbsSavedState;
import android.view.View;
import com.hamom.mvpauth.utils.ConstantManager;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by hamom on 19.12.16.
 */

@SuppressWarnings("unused")
public class AccountAvatarBehavior extends CoordinatorLayout.Behavior<CircleImageView> {
  private static String TAG = ConstantManager.TAG_PREFIX + "AvatarBehavior: ";

  private int mMaxScrollDistance;
  private int mMaxHeight;

  public AccountAvatarBehavior(Context context, AttributeSet attrs) {

  }

  @Override
  public boolean layoutDependsOn(CoordinatorLayout parent, CircleImageView child, View dependency) {
    return dependency instanceof AppBarLayout;
  }

  @Override
  public boolean onDependentViewChanged(CoordinatorLayout parent, CircleImageView child,
      View dependency) {
    if (mMaxScrollDistance == 0) mMaxScrollDistance = dependency.getBottom();
    if (mMaxHeight == 0) mMaxHeight = child.getHeight();

    int newHeight = (int) ((float) dependency.getBottom() / mMaxScrollDistance * mMaxHeight);

    child.setY(dependency.getBottom() - (float) child.getHeight() / 2);

    CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) child.getLayoutParams();
    lp.width = newHeight;
    lp.height = newHeight;
    child.setLayoutParams(lp);

    return true;
  }

  @Override
  public Parcelable onSaveInstanceState(CoordinatorLayout parent, CircleImageView child) {
    final Parcelable superState = super.onSaveInstanceState(parent, child);
    SavedState ss = new SavedState(superState);
    ss.height = child.getHeight();
    ss.width = child.getWidth();
    ss.maxHeight = mMaxHeight;
    ss.maxScrollDistance = mMaxScrollDistance;
    return ss;
  }

  @Override
  public void onRestoreInstanceState(CoordinatorLayout parent, CircleImageView child,
      Parcelable state) {
    if (state instanceof SavedState) {
      mMaxHeight = ((SavedState) state).maxHeight;
      mMaxScrollDistance = ((SavedState) state).maxScrollDistance;
      CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) child.getLayoutParams();
      lp.width = ((SavedState) state).width;
      lp.height = ((SavedState) state).height;
      child.setLayoutParams(lp);
    } else {
      super.onRestoreInstanceState(parent, child, state);
    }
  }

  protected static class SavedState extends AbsSavedState {
    int height;
    int width;
    int maxScrollDistance;
    int maxHeight;

    protected SavedState(Parcelable superState) {
      super(superState);
    }
  }
}