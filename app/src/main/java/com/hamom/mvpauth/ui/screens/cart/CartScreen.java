package com.hamom.mvpauth.ui.screens.cart;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.di.scopes.CartScope;
import com.hamom.mvpauth.flow.AbstractScreen;
import com.hamom.mvpauth.flow.Screen;
import com.hamom.mvpauth.mvp.models.CartModel;
import com.hamom.mvpauth.mvp.presenters.AbstractPresenter;
import com.hamom.mvpauth.ui.activities.RootActivity;
import com.hamom.mvpauth.ui.screens.detail.DetailScreen;
import com.hamom.mvpauth.utils.App;
import com.hamom.mvpauth.utils.ConstantManager;
import dagger.Component;
import dagger.Provides;
import flow.Flow;
import io.realm.Realm;
import mortar.MortarScope;

/**
 * Created by hamom on 03.03.17.
 */
@Screen(R.layout.screen_cart)
public class CartScreen extends AbstractScreen<RootActivity.RootActivityComponent> {
  @Override
  public Object createScreenComponent(RootActivity.RootActivityComponent parentComponent) {
    return DaggerService.createComponent(parentComponent, CartScreen.Component.class);
  }

  //region===================== DI ==========================
  @dagger.Module
  public static class Module{
    @Provides
    @CartScope
    CartPresenter provideCartPresenter() {
      return new CartPresenter();
    }

    @Provides
    @CartScope
    CartModel provideCartModel() {
      return new CartModel();
    }
  }

  @CartScope
  @dagger.Component(dependencies = RootActivity.RootActivityComponent.class, modules = Module.class)
  public interface Component {
    void inject(CartView cartView);
    void inject(CartPresenter cartPresenter);
  }
  //endregion

  //region===================== Presenter ==========================

  public static class CartPresenter extends AbstractPresenter<CartView, CartModel>{
    private static String TAG = ConstantManager.TAG_PREFIX + "CartPresenter: ";

    @Override
    protected void onLoad(Bundle savedInstanceState) {
      super.onLoad(savedInstanceState);
      subscribeOnCartProductsObs();
    }

    @Override
    protected void initActionBar() {
      mRootPresenter.newActionBarBuilder()
          .setTitle(App.getAppContext().getString(R.string.cart))
          .showCart(false)
          .build();
    }

    @Override
    protected void initFab() {
      mRootPresenter.newFabBuilder().setVisible(false).build();
    }

    @Override
    protected void initDagger(MortarScope scope) {
      ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    //region===================== Events ==========================
    public void onDeleteClick(ProductRealm productRealm) {
      Log.d(TAG, "onDeleteClick: ");
      Realm realm = Realm.getDefaultInstance();
      realm.executeTransaction(realm1 -> productRealm.deleteFromCart());
      realm.close();
    }

    public void onImageClick(ProductRealm productRealm) {
      if (hasView()){
        Flow.get(getView().getContext()).set(new DetailScreen(productRealm));
      }
    }

    public void onPlaceOrderClick() {
      getRootView().showMessage("Оформить заказ");
    }
    //endregion

    private void subscribeOnCartProductsObs() {
      mSubscriptions.add(mModel.getCartProductsObs().subscribe(productRealms -> {
        if (hasView()) getView().initView(productRealms);
      }));
    }

  }
  //endregion
}
