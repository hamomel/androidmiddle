package com.hamom.mvpauth.ui.screens.cart;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.mvp.views.AbstractView;
import com.hamom.mvpauth.utils.ConstantManager;
import com.squareup.picasso.Picasso;
import io.realm.RealmResults;
import javax.inject.Inject;

/**
 * Created by hamom on 03.03.17.
 */

public class CartView extends AbstractView<CartScreen.CartPresenter> {
  private static String TAG = ConstantManager.TAG_PREFIX + "CartView: ";

  @Inject
  Picasso mPicasso;

  @BindView(R.id.cart_recycler)
  RecyclerView cartRecycler;
  @BindView(R.id.product_count_cart_tv)
  TextView productCountCartTv;
  @BindView(R.id.discount_cart_tv)
  TextView discountCartTv;
  @BindView(R.id.total_price_cart_tv)
  TextView totalPriceCartTv;

  private CartAdapter mAdapter;

  //region===================== Events ==========================
  @OnClick(R.id.place_order_cart_btn)
  void onPlaceOrderClick(){
    mPresenter.onPlaceOrderClick();
  }
  //endregion

  public CartView(Context context, AttributeSet attrs) {
    super(context, attrs);
    Log.d(TAG, "CartView: ");
  }

  @Override
  protected void initDagger(Context context) {
    if (!isInEditMode()) {
      DaggerService.<CartScreen.Component>getDaggerComponent(context).inject(this);
    }

  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    setAdapter();
    ButterKnife.bind(this);
  }

  @Override
  public boolean viewOnBackPressed() {
    return false;
  }

  private void setAdapter(){
    mAdapter = new CartAdapter(mPicasso, getOnClickListener());
    LinearLayoutManager manager = new LinearLayoutManager(getContext());
    cartRecycler.setLayoutManager(manager);
    cartRecycler.setAdapter(mAdapter);

  }

  public void initView(RealmResults<ProductRealm> products){
    int sum = getSum(products);
    float discount = sum > 3000 ? sum * 0.15f : 0;
    discount = Math.round(discount * 100)/100;
    float totalPrice = sum > 3000 ? sum * 0.85f : sum;
    totalPrice = Math.round(totalPrice * 100)/100;

    productCountCartTv.setText(String.valueOf(products.size()));
    discountCartTv.setText(String.valueOf(discount));
    totalPriceCartTv.setText(String.valueOf(totalPrice));

    mAdapter.updateAdapter(products);
  }

  private int getSum(RealmResults<ProductRealm> products) {
    int sum = 0;
    for (ProductRealm product : products) {
      sum += product.getPrice() * product.getCount();
    }
    return sum;
  }

  private OnclickListener getOnClickListener() {
    return (View view, ProductRealm productRealm) -> {
      switch (view.getId()){
        case R.id.delete_cart_btn:
          mPresenter.onDeleteClick(productRealm);
          break;
        case R.id.product_image_cart:
          mPresenter.onImageClick(productRealm);
          break;
      }
    };
  }

  public interface OnclickListener {
    void onClick(View view, ProductRealm productRealm);
  }
}
