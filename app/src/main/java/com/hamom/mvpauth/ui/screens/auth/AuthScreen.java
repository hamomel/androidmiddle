package com.hamom.mvpauth.ui.screens.auth;

import android.os.Bundle;

import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.network.res.AuthRes;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.di.scopes.AuthScope;
import com.hamom.mvpauth.flow.AbstractScreen;
import com.hamom.mvpauth.flow.Screen;
import com.hamom.mvpauth.mvp.models.AuthModel;
import com.hamom.mvpauth.mvp.presenters.AbstractPresenter;
import com.hamom.mvpauth.mvp.presenters.IAuthPresenter;
import com.hamom.mvpauth.mvp.presenters.RootPresenter;
import com.hamom.mvpauth.mvp.views.IAuthView;
import com.hamom.mvpauth.mvp.views.IRootView;
import com.hamom.mvpauth.ui.activities.RootActivity;
import com.hamom.mvpauth.ui.screens.catalog.CatalogScreen;

import com.hamom.mvpauth.utils.App;
import com.hamom.mvpauth.utils.ConstantManager;
import com.squareup.leakcanary.RefWatcher;
import flow.Direction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;

/**
 * Created by hamom on 27.11.16.
 */
@Screen(R.layout.screen_auth)
public class AuthScreen extends AbstractScreen<RootActivity.RootActivityComponent> {
  private static String TAG = ConstantManager.TAG_PREFIX + "AuthScreen: ";
  private int mCustomState = 1;

   int getCustomState() {
    return mCustomState;
  }

   void setCustomState(int customState) {
    mCustomState = customState;
  }

  @Override
  public Object createScreenComponent(RootActivity.RootActivityComponent parentComponent) {
    return DaggerService.createComponent(parentComponent, AuthScreen.Component.class);
  }

  //region===================== DI ==========================
  @dagger.Module
   static class AuthPresenterModule {

    @Provides
    @AuthScope
    AuthModel provideAuthModel() {
      return new AuthModel();
    }

    @Provides
    @AuthScope
    AuthPresenter provideAuthPresenter() {
      return new AuthPresenter();
    }
  }

  @AuthScope
  @dagger.Component(dependencies = RootActivity.RootActivityComponent.class, modules = AuthPresenterModule.class)
  interface Component {
    void inject(AuthPresenter authPresenter);

    void inject(AuthView authView);

    //AuthModel getAuthModel();
  }
  //endregion

  //region===================== Presenter ==========================

  public static class AuthPresenter extends AbstractPresenter<AuthView, AuthModel> implements IAuthPresenter {
    @Inject
    RefWatcher mRefWatcher;
    //
    //@Inject
    //AuthModel mModel;
    //
    //@Inject
    //RootPresenter mRootPresenter;

    public AuthPresenter() {
    }

    //for tests only
    public AuthPresenter(AuthModel model, RootPresenter rootPresenter) {
      super(model, rootPresenter);
      //mModel = mModel;
      //mRootPresenter = rootPresenter;
    }

    protected IRootView getRootView() {
      return mRootPresenter.getRootView();
    }



    @Override
    protected void onLoad(Bundle savedInstanceState) {
      super.onLoad(savedInstanceState);

      if (getView() != null) {
        if (checkUserAuth()) {
          getView().hideLoginBtn();
        } else {
          getView().showLoginBtn();
        }
      } else {
        getRootView().showError(new NullPointerException("Что-то не работает"));
      }
    }

    @Override
    protected void initActionBar() {
      mRootPresenter.newActionBarBuilder().setVisible(false).build();
    }

    @Override
    protected void initFab() {
      mRootPresenter.newFabBuilder().setVisible(false).build();
    }

    @Override
    protected void initDagger(MortarScope scope) {
      ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    @Override
    protected void onExitScope() {
      super.onExitScope();
      mRefWatcher.watch(this);
    }

    //region===================== Subscription ==========================

    private void subscribeOnLoginObs(String email, String password) {

      subscribe(mModel.loginUser(email, password), new ViewSubscriber<AuthRes>() {
            @Override
            public void onNext(AuthRes response) {

              getView().hideProgressDialog();

              System.out.println("onLoginSuccess " + Thread.currentThread().getName());
              mModel.saveUserId(response.getId());
              mModel.saveAuthToken(response.getToken());
              System.out.println("onLoginSuccess after model invocation " + mModel + response.getId());

              getRootView().showMessage(App.getAppContext().getString(R.string.auth_complete));
              Flow.get(getView()).goBack();
            }

        @Override
        public void onError(Throwable e) {
          System.out.println("onLoginFail " + Thread.currentThread().getName());
            getRootView().showMessage(e.getMessage());
        }
      });
    }

    //endregion

    //region===================== IAuthPresenter ==========================
    @Override
    public void clickOnLogin() {
      if (getView() != null) {
        if (getView().isIdle()) {
          getView().showLoginWithAnim();
        } else {
          getView().hideEmailError();
          getView().hidePasswordError();

          if (isValidEmail(getView().getUserEmail()) && isValidPassword(
              getView().getUserPassword())) {

            getView().showProgressDialog("request for user auth");

            subscribeOnLoginObs(getView().getUserEmail(), getView().getUserPassword());
          } else {
            if (!isValidEmail(getView().getUserEmail())) {
              getView().showEmailError();
            }

            if (!isValidPassword(getView().getUserPassword())) {
              getView().showPasswordError();
            }
          }
        }
      }
    }

    @Override
    public void clickOnShowCatalog() {
      if (getView() != null) {
        Flow.get(getView()).replaceTop(new CatalogScreen(), Direction.FORWARD);
      }
    }

    @Override
    public void clickOnFb() {
      if (getRootView() != null) {
        getRootView().showMessage("FB");
      }
    }

    @Override
    public void clickOnVk() {
      if (getRootView() != null) {
        getRootView().showMessage("VK");
      }
    }

    @Override
    public void clickOnTwitter() {
      if (getRootView() != null) {
        getRootView().showMessage("Twitter");
      }
    }

    @Override
    public boolean checkUserAuth() {
      return mModel.isAuthUser();
    }
    //endregion

    //region===================== Validator ==========================
    private boolean isValidEmail(String email) {
      String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
          + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

      Pattern pattern = Pattern.compile(EMAIL_PATTERN);
      Matcher matcher = pattern.matcher(email);
      return matcher.matches();
    }

    private boolean isValidPassword(String pass) {
      if (pass != null && pass.length() > 7) {
        return true;
      }
      return false;
    }
    //endregion
  }

  //endregion
}
