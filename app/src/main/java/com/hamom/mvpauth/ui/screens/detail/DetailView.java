package com.hamom.mvpauth.ui.screens.detail;

import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRatingBar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import butterknife.BindView;

import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.dto.ProductDto;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.mvp.views.AbstractView;
import com.hamom.mvpauth.mvp.views.IView;
import com.hamom.mvpauth.utils.ConstantManager;
import flow.Flow;
import rx.Subscription;
import rx.subjects.PublishSubject;

/**
 * Created by hamom on 22.12.16.
 */
public class DetailView extends AbstractView<DetailScreen.DetailPresenter> implements IView{
  private static String TAG = ConstantManager.TAG_PREFIX + "DetailView: ";
  @BindView(R.id.detail_view_pager)
  ViewPager detailViewPager;

  DetailAdapter mAdapter;

  Subscription mSubscription;

  private PublishSubject<View> mOnclickObs = PublishSubject.create();

  public DetailView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    subscribeOnClick();
  }

  @Override
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    mSubscription.unsubscribe();
  }

  @Override
  protected void initDagger(Context context) {
    if (!isInEditMode()) {
      DaggerService.<DetailScreen.Component>getDaggerComponent(context).inject(this);
    }
  }

  public void initView(ProductDto productDto){
    if (mAdapter == null){
      mAdapter = new DetailAdapter(productDto, mOnclickObs);
      detailViewPager.setAdapter(mAdapter);
    } else {
      mAdapter.refreshData(productDto);
    }

  }

  @Override
  public boolean viewOnBackPressed() {
    Flow.get(this).goBack();
    return true;
  }

  public ViewPager getDetailViewPager(){
    return detailViewPager;
  }


  private void subscribeOnClick(){
    mSubscription = mOnclickObs.subscribe(view -> {
      switch (view.getId()){
          case R.id.plus_btn_detail:
            mPresenter.onPlusClick();
            break;
          case R.id.minus_btn_detail:
            mPresenter.onMinusClick();
            break;
          default: break;
        }});
  }

  public void showAddCommentDialog() {
    LayoutInflater inflater = LayoutInflater.from(getContext());
    View view = inflater.inflate(R.layout.layout_dialog_add_comment, null);
    AppCompatRatingBar ratingBar = (AppCompatRatingBar) view.findViewById(R.id.ratingbar_new_comment);
    TextInputEditText text = (TextInputEditText) view.findViewById(R.id.new_comment_ti);

    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
    builder.setView(view)
        .setPositiveButton(getContext().getString(R.string.add_comment), (dialog, which) -> {
          addNewComment(ratingBar.getRating(), text.getText().toString());
          dialog.dismiss();
        })
        .setNegativeButton(getContext().getString(R.string.cancel), (dialog, which) -> dialog.dismiss())
        .show();
  }

  private void addNewComment(float rating, String text){
    mPresenter.addNewComment(rating, text);
  }

}
