package com.hamom.mvpauth.ui.screens.address;

import android.os.Bundle;
import android.support.annotation.Nullable;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.dto.UserAddressDto;
import com.hamom.mvpauth.data.storage.realm.AddressRealm;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.di.scopes.AddressScope;
import com.hamom.mvpauth.flow.AbstractScreen;
import com.hamom.mvpauth.flow.Screen;
import com.hamom.mvpauth.mvp.models.AccountModel;
import com.hamom.mvpauth.mvp.presenters.AbstractPresenter;
import com.hamom.mvpauth.mvp.presenters.IAddressPresenter;
import com.hamom.mvpauth.mvp.presenters.MenuItemHolder;
import com.hamom.mvpauth.mvp.presenters.RootPresenter;
import com.hamom.mvpauth.ui.screens.account.AccountScreen;

import com.squareup.leakcanary.RefWatcher;
import io.realm.Realm;
import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import flow.TreeKey;
import mortar.MortarScope;
import mortar.ViewPresenter;

/**
 * Created by hamom on 30.11.16.
 */
@Screen(R.layout.screen_address)
public class AddressScreen extends AbstractScreen<AccountScreen.Component> implements TreeKey {
  @Nullable
  private static AddressRealm sAddressRealm;

  public AddressScreen(@Nullable AddressRealm addressRealm) {
    sAddressRealm = addressRealm;
  }

  @Override
  public boolean equals(Object o) {
    if (sAddressRealm != null) {
      return o instanceof AddressScreen && sAddressRealm.equals(((AddressScreen) o).sAddressRealm);
    } else {
      return super.equals(o);
    }
  }

  @Override
  public int hashCode() {
    return sAddressRealm != null ? sAddressRealm.hashCode() : super.hashCode();
  }

  @Override
  public Object createScreenComponent(AccountScreen.Component parentComponent) {
    return DaggerService.createComponent(parentComponent, Component.class);
  }

  @Override
  public Object getParentKey() {
    return new AccountScreen();
  }

  //region===================== DI ==========================
  @dagger.Module
  public static class Module {
    @Provides
    @AddressScope
    AddressPresenter provideAddressPresenter() {
      return new AddressPresenter();
    }

  }

  @dagger.Component(dependencies = AccountScreen.Component.class, modules = Module.class)
  @AddressScope
  public interface Component {
    void inject(AddressPresenter addressPresenter);

    void inject(AddressView addressView);

    AccountModel getAccountModel();
  }

  //endregion

  //region===================== Presenter ==========================
  public static class AddressPresenter extends AbstractPresenter<AddressView, AccountModel>
      implements IAddressPresenter {
    private static final String TAG = "AddressPresenter";
    @Inject
    RefWatcher mRefWatcher;

    @Inject
    AccountModel mModel;

    @Override
    protected void onLoad(Bundle savedInstanceState) {
      super.onLoad(savedInstanceState);
      if (sAddressRealm != null && getView() != null) {
        UserAddressDto dto = new UserAddressDto(sAddressRealm);
        getView().initView(dto);
      }
    }

    @Override
    protected void initActionBar() {

      mRootPresenter.newActionBarBuilder()
          .setBackArrow(true)
          .setTitle(sAddressRealm != null ? getView().getContext().getString(R.string.edit_address)
              : getView().getContext().getString(R.string.new_address))
          .build();
    }


    @Override
    protected void initFab() {
      mRootPresenter.newFabBuilder().setVisible(false).build();
    }

    @Override
    protected void initDagger(MortarScope scope) {
      ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    @Override
    protected void onExitScope() {
      super.onExitScope();
      mRefWatcher.watch(this);
    }


    private void onToolbarCartClick() {

    }

    @Override
    public void clickOnAddAddress() {
      Log.d(TAG, "clickOnAddAddress: ");
      if (TextUtils.isEmpty(getView().getUserAddress().getStreet())
          || TextUtils.isEmpty(getView().getUserAddress().getHome())){
        getView().showInputError();
      } else {
        if (getView() != null) {
          if (sAddressRealm != null) {
            Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(realm1 -> sAddressRealm.fromDto(getView().getUserAddress()));
            realm.close();

            Flow.get(getView()).goBack();
          } else {
            Log.d(TAG, "clickOnAddAddress: ");

            mModel.saveUserAddress(getView().getUserAddress());
            Flow.get(getView()).goBack();
          }
        }
      }
    }
  }
  //endregion
}
