package com.hamom.mvpauth.ui.screens.detail;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.dto.ProductDto;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.utils.ConstantManager;
import com.squareup.picasso.Picasso;
import javax.inject.Inject;
import rx.subjects.PublishSubject;

/**
 * Created by hamom on 23.12.16.
 */
public class DetailCommentsView extends CoordinatorLayout {
  private static String TAG = ConstantManager.TAG_PREFIX + "DetailReviewView: ";


  @BindView(R.id.review_recycler)
  RecyclerView reviewRecycler;



  public DetailCommentsView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }


  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    ButterKnife.bind(this);
  }

  public void initView(ProductDto product) {
    LinearLayoutManager manager = new LinearLayoutManager(getContext());
    CommentsAdapter adapter = new CommentsAdapter(product.getComments());
    reviewRecycler.setLayoutManager(manager);
    reviewRecycler.setAdapter(adapter);
  }
}
