package com.hamom.mvpauth.ui.screens.account;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.dto.UserAddressDto;
import com.hamom.mvpauth.data.storage.dto.UserInfoDto;
import com.hamom.mvpauth.data.storage.dto.UserSettingsDto;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.di.scopes.AccountScope;
import com.hamom.mvpauth.flow.AbstractScreen;
import com.hamom.mvpauth.flow.Screen;
import com.hamom.mvpauth.mvp.models.AccountModel;
import com.hamom.mvpauth.mvp.presenters.AbstractPresenter;
import com.hamom.mvpauth.mvp.presenters.IAccountPresenter;
import com.hamom.mvpauth.mvp.presenters.MenuItemHolder;
import com.hamom.mvpauth.mvp.presenters.RootPresenter;
import com.hamom.mvpauth.mvp.presenters.SubscribePresenter;
import com.hamom.mvpauth.mvp.views.IRootView;
import com.hamom.mvpauth.ui.activities.RootActivity;
import com.hamom.mvpauth.ui.screens.address.AddressScreen;
import com.hamom.mvpauth.utils.App;
import com.hamom.mvpauth.utils.ConstantManager;
import com.hamom.mvpauth.utils.UriFromContentGetter;
import com.squareup.leakcanary.RefWatcher;
import com.squareup.picasso.Picasso;
import dagger.Provides;
import flow.Flow;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.inject.Inject;
import mortar.MortarScope;
import rx.Observable;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by hamom on 28.11.16.
 */
@Screen(R.layout.screen_account)
public class AccountScreen extends AbstractScreen<RootActivity.RootActivityComponent> {
  private static int sCustomState = 1;

  public static int getCustomState() {
    return sCustomState;
  }

  public void setCustomState(int customState) {
    sCustomState = customState;
  }

  @Override
  public Object createScreenComponent(RootActivity.RootActivityComponent parentComponent) {
    return DaggerService.createComponent(parentComponent, Component.class);
  }

  //region===================== DI ==========================
  @dagger.Module
  public static class Module {
    @Provides
    @AccountScope
    AccountPresenter provideAccountPresenter() {
      return new AccountPresenter();
    }
  }

  @dagger.Component(dependencies = RootActivity.RootActivityComponent.class, modules = Module.class)
  @AccountScope
  public interface Component {
    void inject(AccountPresenter accountPresenter);

    void inject(AccountView accountView);

    RefWatcher getRefWatcher();

    Picasso getPicacsso();

    RootPresenter getRootPresenter();

    AccountModel getAccountModel();
  }
  //endregion

  //region===================== Presenter ==========================
  public static class AccountPresenter extends AbstractPresenter<AccountView, AccountModel>
      implements IAccountPresenter {
    private static String TAG = ConstantManager.TAG_PREFIX + "AccountPresenter: ";

    // TODO: 13.12.16 solve problem with saving userinfo fields during getting photo

    @Inject
    RootPresenter mRootPresenter;

    @Inject
    RefWatcher mRefWatcher;

    @Inject
    AccountModel mModel;

    private File mPhotoFile;
    @Override
    protected void initActionBar() {

      int resId = getCustomState() == AccountView.PREVIEW_STATE ? R.drawable.ic_edit_black_24dp : R.drawable.ic_check_black_24dp;
      mRootPresenter.newActionBarBuilder()
          .setTitle(getView().getContext().getString(R.string.cabinet))
          .setBackArrow(false)
          .addAction(new MenuItemHolder("", resId, item -> switchViewState()))
          .build();
    }


    @Override
    protected void initFab() {
      mRootPresenter.newFabBuilder().setVisible(false).build();
    }

    @Override
    protected void initDagger(MortarScope scope) {
      ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    //region===================== Lifecycle ==========================

    @Override
    protected void onLoad(Bundle savedInstanceState) {
      Log.d(TAG, "onLoad: " + savedInstanceState);
      super.onLoad(savedInstanceState);
      if (getView() != null) {
        getView().initView();
      }

      // FIXME: 12.01.17 fix saving user data in edit mode
      updateListView();
      //subscribeOnAddressObs();
      subscribeOnSettingsObs();
      subscribeOnUserInfoObs();
      subscribeOnActivityResult();
    }


    @Override
    protected void onExitScope() {
      super.onExitScope();
      mRefWatcher.watch(this);

    }

    @Override
    public void dropView(AccountView view) {
      Bundle outState = new Bundle();
      outState.putParcelable(this.getClass().getName(), view.getUserProfileInfo());
      onSave(outState);
      super.dropView(view);
    }

    //endregion

    //region===================== Subscription ==========================

    @Override
    protected void subscribeOnModelError() {
      subscribe(mModel.getErrorObs(), new ViewSubscriber<Throwable>() {
        @Override
        public void onNext(Throwable throwable) {
          if (getRootView() != null) {
            getRootView().showError(throwable);
          }
        }
      });
    }

    private void subscribeOnAddressObs() {
     mSubscriptions.add(mModel.getAddressObs().subscribe(new ViewSubscriber<UserAddressDto>() {
        @Override
        public void onNext(UserAddressDto addressDto) {
          if (getView() != null) {
            getView().getAddressAdapter().addItem(addressDto);
          }
        }
      }));
    }

    private void updateListView() {
      getView().getAddressAdapter().reloadAdapter();
      subscribeOnAddressObs();
    }

    private void subscribeOnSettingsObs() {
      subscribe(mModel.getUserSettingsObs(), new ViewSubscriber<UserSettingsDto>() {
        @Override
        public void onNext(UserSettingsDto settings) {
          if (getView() != null) {
            getView().initSettings(settings);
          }
        }
      });
    }

    private void subscribeOnUserInfoObs() {
      Log.d(TAG, "subscribeOnUserInfoObs: ");
      subscribe(mModel.getUserInfoObs(), new ViewSubscriber<UserInfoDto>() {
        @Override
        public void onNext(UserInfoDto t) {
          if (getView() != null && t != null) {
            getView().updateProfileInfo(t);
          }
        }
      });
    }

    @SuppressLint("NewApi")
    private void subscribeOnActivityResult() {
      Observable<Uri> activityResultDtoObs = mRootPresenter.getActivityResultDtoObs()
          .filter(activityResultDto -> activityResultDto.getResultCode() == Activity.RESULT_OK)
          .map(activityResultDto -> {
            Uri uri = null;
            switch (activityResultDto.getRequestCode()) {
              case ConstantManager.REQUEST_PHOTO_FROM_GALLERY:
                if (activityResultDto.getIntent() != null) {
                  Uri photoUri = activityResultDto.getIntent().getData();
                  String realUri = "file://" + UriFromContentGetter.getPath(getView().getContext(), photoUri);
                  uri = Uri.parse(realUri);
                }
                break;
              case ConstantManager.REQUEST_CAMERA_PICTURE:
                if (mPhotoFile != null) {
                  uri = Uri.fromFile(mPhotoFile);
                }
                break;
            }
            return uri;
          })
          .filter(uri -> uri != null);

      mSubscriptions.add(activityResultDtoObs.subscribe(new ViewSubscriber<Uri>() {
        @Override
        public void onNext(Uri uri) {
          handleActivityResult(uri);
        }
      }));
    }

    private void handleActivityResult(Uri photoUri) {
      Log.d(TAG, "handleActivityResult: " + photoUri);

        getView().updateAvatarImage(photoUri);
    }

    //endregion

    //region===================== ICatalogPresenter ==========================
    @Override
    public void clickOnAddAddress() {
      if (getView() != null) {
        Flow.get(getView()).set(new AddressScreen(null));
      }
    }

    @Override
    public boolean switchViewState() {

      if (getCustomState() == AccountView.EDIT_STATE) {
        Log.d(TAG, "switchViewState: ");
        mModel.saveProfileInfo(getView().getUserProfileInfo());

      }
      if (getView() != null) {
        getView().changeState();
      }
      initActionBar();
      return true;
    }

    @Override
    public void takePhoto() {
      if (getView() != null) {
        getView().showPhotoSourceDialog();
      }
    }

    //set checked item in navigation drawer
    @Override
    public void setMenuItemChecked() {
      if (getRootView() != null && getView() != null) {
        getRootView().setMenuItemChecked(getView());
      }
    }


    @Override
    public void removeAddress(int id) {
      mModel.removeAddress(id);
      updateListView();
    }

    @Nullable
    protected IRootView getRootView() {
      return mRootPresenter.getRootView();
    }
    //endregion

    public void onRightAddressSwipe(int id) {
      Flow.get(getView()).set(new AddressScreen(mModel.getAddressFromRealm(id)));
    }

    public void switchSettings() {
      if (getView() != null) {
        mModel.saveSettings(getView().getSettings());
      }
    }

    //region===================== TakePhoto ==========================
    @Override
    public void chooseGallery() {
      if (getRootView() != null) {
        String[] permissions = new String[] { READ_EXTERNAL_STORAGE };
        if (mRootPresenter.checkPermissionsAndRequestIfNotGranted(permissions,
            ConstantManager.REQUEST_PERMISSION_READ_EXTERNAL_STORAGE)) {
          loadPhotoFromGallery();
        }
      }
    }

    public void loadPhotoFromGallery() {
      Intent intent = new Intent();
      if (Build.VERSION.SDK_INT < 19) {
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
      } else {
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
      }
      ((RootActivity) getRootView()).startActivityForResult(intent,
          ConstantManager.REQUEST_PHOTO_FROM_GALLERY);
    }

    @Override
    public void chooseCamera() {
      if (getRootView() != null) {
        String[] permissions = new String[] { CAMERA, WRITE_EXTERNAL_STORAGE };
        if (mRootPresenter.checkPermissionsAndRequestIfNotGranted(permissions,
            ConstantManager.REQUEST_PERMISSION_CAMERA)) {
          mPhotoFile = createImageFile();
          if (mPhotoFile == null) {
            getRootView().showMessage(getView().getContext().getString(R.string.cant_take_photo));
          }
          loadPhotoFromCamera();
        }
      }
    }

    public void loadPhotoFromCamera() {
      Uri uriForFile;
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        uriForFile = FileProvider.getUriForFile(((RootActivity) getRootView()), ConstantManager.FILE_PROVIDER_AUTHORITY, mPhotoFile);
      } else {
        uriForFile = Uri.fromFile(mPhotoFile);
      }

      Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
      takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriForFile);
      ((RootActivity) getRootView()).startActivityForResult(takePictureIntent,
          ConstantManager.REQUEST_CAMERA_PICTURE);
    }

    private File createImageFile() {
      String dateStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
      String imageFileName = "JPEG_" + dateStamp;
      File storageDir = getView().getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);

      Log.d(TAG, "createImageFile: " + storageDir.getAbsolutePath());
      File image = null;
      try {
        image = File.createTempFile(imageFileName, ".jpg", storageDir);
      } catch (IOException e) {
        return null;
      }

      return image;
    }

    //endregion
  }
  //endregion
}
