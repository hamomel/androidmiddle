package com.hamom.mvpauth.ui.screens.favorite;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.di.scopes.FavoriteScope;
import com.hamom.mvpauth.flow.AbstractScreen;
import com.hamom.mvpauth.flow.Screen;
import com.hamom.mvpauth.mvp.models.FavoriteModel;
import com.hamom.mvpauth.mvp.presenters.AbstractPresenter;
import com.hamom.mvpauth.mvp.presenters.MenuItemHolder;
import com.hamom.mvpauth.ui.activities.RootActivity;
import com.hamom.mvpauth.ui.screens.detail.DetailScreen;
import com.hamom.mvpauth.utils.App;
import dagger.Provides;
import flow.Flow;
import io.realm.Realm;
import io.realm.RealmResults;
import java.util.ArrayList;
import java.util.List;
import mortar.MortarScope;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by hamom on 27.02.17.
 */
@Screen(R.layout.screen_favorite)
public class FavoriteScreen extends AbstractScreen<RootActivity.RootActivityComponent> {

  @Override
  public Object createScreenComponent(RootActivity.RootActivityComponent parentComponent) {
    return DaggerService.createComponent(parentComponent, Component.class);
  }

  //region===================== Di ==========================
  @dagger.Module
  public static class Module {

    @Provides
    @FavoriteScope
    FavoritePresenter provideFavoritePresenter() {
      return new FavoritePresenter();
    }

    @Provides
    @FavoriteScope
    FavoriteModel provideFavoriteModel() {
      return new FavoriteModel();
    }
  }

  @FavoriteScope
  @dagger.Component(dependencies = RootActivity.RootActivityComponent.class, modules = Module.class)
  public interface Component {
    void inject(FavoriteView favoriteView);
    void inject(FavoritePresenter favoritePresenter);

  }
  //endregion

  //region===================== Presenter ==========================
  public static class FavoritePresenter extends AbstractPresenter<FavoriteView, FavoriteModel>{

    @Override
    protected void onLoad(Bundle savedInstanceState) {
      super.onLoad(savedInstanceState);
      mSubscriptions.add(mModel.getFavoriteProducts().subscribe(productRealms -> {
        if (hasView()) getView().initView(productRealms);
      }));
    }

    @Override
    protected void initActionBar() {
      mRootPresenter.newActionBarBuilder()
          .setVisible(true)
          .setBackArrow(false)
          .setTitle(App.getAppContext().getString(R.string.favorite_products))
          .build();
    }



    @Override
    protected void initFab() {
      mRootPresenter.newFabBuilder()
          .setVisible(false)
          .build();

    }

    @Override
    protected void initDagger(MortarScope scope) {
      ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    public void onImageClick(ProductRealm product) {
      Flow.get(getView().getContext()).set(new DetailScreen(product));
    }

    public void onCartClick(ProductRealm product) {
      if (product.isInCart()){
        getRootView().showMessage(App.getAppContext().getString(R.string.product_already_in_cart));
        return;
      }
      Realm realm = Realm.getDefaultInstance();
      realm.executeTransaction(realm1 -> product.addToCart());
      realm.close();
      getRootView().showMessage(App.getAppContext().getString(R.string.product_added_to_cart));

    }

    public void onFavoriteClick(ProductRealm product) {
      mModel.deleteFavoriteJob(product.getId());
      Realm realm = Realm.getDefaultInstance();
      realm.executeTransaction(realm1 -> product.changeFavorite());
      realm.close();
    }

  }
  //endregion
}
