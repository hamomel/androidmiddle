package com.hamom.mvpauth.ui.screens.cart;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import com.squareup.picasso.Picasso;
import io.realm.RealmResults;

/**
 * Created by hamom on 03.03.17.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
  private RealmResults<ProductRealm> mProducts;
  private Picasso mPicasso;
  private CartView.OnclickListener mOnclickListener;

  public CartAdapter(Picasso picasso, CartView.OnclickListener listener) {
    this.mPicasso = picasso;
    this.mOnclickListener = listener;
  }

  public void updateAdapter(RealmResults<ProductRealm> products){
    mProducts = products;
    notifyDataSetChanged();
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart, parent, false);

    return new ViewHolder(view, mOnclickListener);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    ProductRealm product = mProducts.get(position);

    mPicasso.load(product.getImageUrl())
        .placeholder(R.drawable.no_image)
        .fit()
        .centerInside()
        .into(holder.productImageCart);
    
    holder.titleFavorite.setText(product.getProductName());
    holder.descFavorite.setText(product.getProductDescription());
    holder.priceItemCartTv.setText(String.valueOf(product.getPrice()));
    holder.countItemCartTv.setText(String.valueOf(product.getCount()));

    int sum = product.getCount() > 0 ? product.getCount() * product.getPrice() : 0;
    holder.sumItemCartTv.setText(String.valueOf(sum));

  }

  @Override
  public int getItemCount() {
    if (mProducts != null) {
      return mProducts.size();
    } else {
      return 0;
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder{
    CartView.OnclickListener mOnclickListener;

    @BindView(R.id.product_image_cart)
    ImageView productImageCart;
    @BindView(R.id.title_favorite)
    TextView titleFavorite;
    @BindView(R.id.desc_favorite)
    TextView descFavorite;
    @BindView(R.id.price_item_cart_tv)
    TextView priceItemCartTv;
    @BindView(R.id.count_item_cart_tv)
    TextView countItemCartTv;
    @BindView(R.id.sum_item_cart_tv)
    TextView sumItemCartTv;
    
    @OnClick(R.id.delete_cart_btn)
    void onDeleteClick(View view){
      mOnclickListener.onClick(view, mProducts.get(getAdapterPosition()));
    }

    @OnClick(R.id.product_image_cart)
    void onImageClick(View view){
      mOnclickListener.onClick(view, mProducts.get(getAdapterPosition()));
    }
    
    public ViewHolder(View itemView, CartView.OnclickListener listener) {
      super(itemView);
      mOnclickListener = listener;
      ButterKnife.bind(this, itemView);
    }
  }
}
