package com.hamom.mvpauth.ui.screens.detail;

import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.dto.ProductDto;
import com.hamom.mvpauth.utils.App;
import rx.subjects.PublishSubject;

/**
 * Created by hamom on 22.12.16.
 */

public class DetailAdapter extends PagerAdapter {
  private static final String TAG = "DetailAdapter";
  private ProductDto mProduct;
  private PublishSubject<View> mOnclickObs;
  private DetailCommentsView mDetailCommentsView;
  private DetailDescriptionView mDetailDescriptionView;

  public DetailAdapter(ProductDto product, PublishSubject<View> onclickObs) {
    Log.d(TAG, "DetailAdapter: ");
    mProduct = product;
    mOnclickObs = onclickObs;
  }

  public void refreshData(ProductDto product){
    mProduct = product;
    notifyDataSetChanged();
    if (mDetailCommentsView != null) mDetailCommentsView.initView(product);
    if (mDetailDescriptionView != null) mDetailDescriptionView.refreshData(product);
  }

  @Override
  public int getCount() {
    return 2;
  }

  @Override
  public boolean isViewFromObject(View view, Object object) {
    return view.equals(object);
  }

  @Override
  public Object instantiateItem(ViewGroup container, int position) {
    Log.d(TAG, "instantiateItem: " + position);
    LayoutInflater inflater = LayoutInflater.from(container.getContext());
    View view = null;
    switch (position){
      case 0:
        view = inflater.inflate(R.layout.screen_description_detail, container, false);
        mDetailDescriptionView = (DetailDescriptionView) view;
        mDetailDescriptionView.initView(mProduct, mOnclickObs);
        break;
      case 1:
        view = inflater.inflate(R.layout.screen_reviews_detail, container, false);
        mDetailCommentsView = (DetailCommentsView) view;
        mDetailCommentsView.initView(mProduct);
        break;
    }
    container.addView(view);
    return view;
  }

  @Override
  public void destroyItem(ViewGroup container, int position, Object object) {
    container.removeView((View) object);
  }

  @Override
  public CharSequence getPageTitle(int position) {
    CharSequence title = "";
    switch (position){
      case 0:
        title = App.getAppContext().getString(R.string.tab_description);
      break;
      case 1:
        title = App.getAppContext().getString(R.string.tab_reviews);
      break;
    }
    return title;
  }
}
