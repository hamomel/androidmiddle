package com.hamom.mvpauth.ui.screens.product;

import android.os.Bundle;
import android.util.Log;

import com.birbit.android.jobqueue.JobManager;
import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.dto.ProductDto;
import com.hamom.mvpauth.data.storage.dto.ProductLocalInfo;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.di.scopes.ProductScope;
import com.hamom.mvpauth.flow.AbstractScreen;
import com.hamom.mvpauth.flow.Screen;
import com.hamom.mvpauth.jobs.AddFavoriteJob;
import com.hamom.mvpauth.mvp.models.CatalogModel;
import com.hamom.mvpauth.mvp.presenters.IProductPresenter;
import com.hamom.mvpauth.ui.screens.catalog.CatalogScreen;
import com.hamom.mvpauth.ui.screens.detail.DetailScreen;
import com.hamom.mvpauth.utils.ConstantManager;

import com.squareup.leakcanary.RefWatcher;
import flow.Flow;
import javax.inject.Inject;

import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import mortar.MortarScope;
import mortar.ViewPresenter;
import rx.subjects.PublishSubject;

/**
 * Created by hamom on 27.11.16.
 */
@Screen(R.layout.screen_product)
public class ProductScreen extends AbstractScreen<CatalogScreen.Component> {
  private ProductRealm mProductRealm;

  public ProductScreen(ProductRealm productRealm) {
    mProductRealm = productRealm;
  }

  @Override
  public Object createScreenComponent(CatalogScreen.Component parentComponent) {
    return DaggerProductScreen_Component.builder()
        .module(new Module(mProductRealm))
        .component(parentComponent)
        .build(); //DaggerService.createComponent(parentComponent, ProductScreen.Component.class, mProductRealm);
  }

  @Override
  public boolean equals(Object o) {
    return super.equals(o) && ((ProductScreen) o).mProductRealm.equals(mProductRealm);
  }

  @Override
  public int hashCode() {
    return mProductRealm.hashCode();
  }

  //region===================== DI ==========================
  @dagger.Module
  public static class Module {
    ProductRealm mProductRealm1;

    public Module(ProductRealm productRealm1) {
      mProductRealm1 = productRealm1;
    }

    @Provides
    @ProductScope
    ProductPresenter provideProductPresenter() {
      return new ProductPresenter(mProductRealm1);
    }
  }

  @dagger.Component(dependencies = CatalogScreen.Component.class, modules = Module.class)
  @ProductScope
  public interface Component {
    void inject(ProductPresenter productPresenter);

    void inject(ProductView productView);

    CatalogModel getCatalogModel();

  }

  //endregion

  //region===================== ProductPresenter ==========================

  public static class ProductPresenter extends ViewPresenter<ProductView>
      implements IProductPresenter {
    private static String TAG = ConstantManager.TAG_PREFIX + "ProductPresenter: ";

    private ProductRealm mProduct;

    @Inject
    RefWatcher mRefWatcher;

    @Inject
    CatalogModel mModel;

    private RealmChangeListener mListener;

    public ProductPresenter(ProductRealm product) {
      mProduct = product;
    }

    @Override
    protected void onEnterScope(MortarScope scope) {
      super.onEnterScope(scope);
      ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    @Override
    protected void onExitScope() {
      super.onExitScope();
      mRefWatcher.watch(this);

    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
      super.onLoad(savedInstanceState);
      if (getView() != null && mProduct.isValid()) {
        Log.d(TAG, "onLoad: " + mProduct.getId());

        mListener = element -> {
          if (getView() != null) {
            getView().showProductView(new ProductDto(mProduct));
          }
        };
        mProduct.addChangeListener(mListener);
        getView().showProductView(new ProductDto(mProduct));
      }
    }

    @Override
    public void dropView(ProductView view) {
      mProduct.removeChangeListener(mListener);
      super.dropView(view);
    }

    @Override
    public void clickOnPlus() {
      Realm realm = Realm.getDefaultInstance();
      realm.executeTransaction(realm1 -> mProduct.add());
      realm.close();
    }

    @Override
    public void clickOnMinus() {
      Realm realm = Realm.getDefaultInstance();
      realm.executeTransaction(realm1 -> mProduct.remove());
      realm.close();
    }

    public void clickOnFavorite() {
      if (mProduct.isFavorite()){
        mModel.deleteFavoriteJob(mProduct.getId());
      } else {
        mModel.addFavoriteJob(mProduct.getId());
      }
      Realm realm = Realm.getDefaultInstance();
      Log.d(TAG, "clickOnFavorite: RealmConfig" + realm.getConfiguration());
      realm.executeTransaction(realm1 -> mProduct.changeFavorite());
      realm.close();
    }

    public void clickOnShowMore() {
      Flow.get(getView()).set(new DetailScreen(mProduct));
    }


  }
  //endregion
}
