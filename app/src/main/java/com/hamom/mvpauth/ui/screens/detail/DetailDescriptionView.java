package com.hamom.mvpauth.ui.screens.detail;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.dto.ProductDto;
import com.hamom.mvpauth.utils.ConstantManager;
import rx.subjects.PublishSubject;

/**
 * Created by hamom on 22.12.16.
 */

public class DetailDescriptionView extends CoordinatorLayout implements View.OnClickListener {
  private static String TAG = ConstantManager.TAG_PREFIX + "DetailDesc: ";
  private PublishSubject<View> onClickObs;

  @BindView(R.id.product_detail_description)
  TextView productDetailDescription;

  @BindView(R.id.detail_product_count)
  TextView detailProductCount;

  @BindView(R.id.detail_product_price)
  TextView detailProductPrice;

  @BindView(R.id.detail_ratingBar)
  RatingBar detailRatingBar;

  @BindView(R.id.plus_btn_detail)
  ImageButton plusBtnDetail;

  @BindView(R.id.minus_btn_detail)
  ImageButton minusBtnDetail;

  public DetailDescriptionView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    ButterKnife.bind(this);
  }

  public void initView(ProductDto product, PublishSubject<View> observable){
    initDataFields(product);
    onClickObs = observable;
    plusBtnDetail.setOnClickListener(this);
    minusBtnDetail.setOnClickListener(this);
  }

  @Override
  public void onClick(View v) {
    onClickObs.onNext(v);
  }

  private void initDataFields(ProductDto product){
    productDetailDescription.setText(product.getDescription());
    detailProductCount.setText(String.valueOf(product.getCount()));
    if (product.getCount() > 0){
      detailProductPrice.setText(String.valueOf(product.getPrice() * product.getCount() + ".-"));
    } else {
      detailProductPrice.setText(String.valueOf(product.getPrice() + ".-"));
    }
    detailRatingBar.setRating(product.getRaiting());
  }

  public void refreshData(ProductDto product) {
    initDataFields(product);
  }
}
