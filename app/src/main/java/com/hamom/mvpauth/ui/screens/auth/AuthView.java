package com.hamom.mvpauth.ui.screens.auth;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.hamom.mvpauth.R;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.mvp.presenters.IAuthPresenter;
import com.hamom.mvpauth.mvp.views.AbstractView;
import com.hamom.mvpauth.mvp.views.IAuthView;
import com.hamom.mvpauth.utils.ViewHelper;
import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.Fade;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;
import flow.Flow;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by hamom on 28.11.16.
 */

public class AuthView extends AbstractView<AuthScreen.AuthPresenter> implements IAuthView {
  private final Animator mScaleAnim;
  @Inject
  AuthScreen.AuthPresenter mPresenter;
  public static final int LOGIN_STATE = 0;

  public static final int IDLE_STATE = 1;
  private final Transition mBounds;
  private final Transition mFade;

  private AuthScreen mScreen;

  @BindView(R.id.login_email_et)
  EditText mLoginEt;

  @BindView(R.id.login_email_wrap)
  TextInputLayout mLoginWrap;

  @BindView(R.id.password_wrap)
  TextInputLayout mPasswordWrap;

  @BindView(R.id.password_et)
  EditText mPasswordEt;

  @BindView(R.id.auth_card)
  CardView mAuthCard;

  @BindView(R.id.login_btn)
  Button mLoginBtn;

  @BindView(R.id.show_btn)
  Button mShowBtn;

  @BindView(R.id.panel_wrapper)
  FrameLayout mPanelWrapper;

  @BindView(R.id.logo_img)
  ImageView mLogoImg;

  private float mDen;

  private ProgressDialog mProgressDialog;
  private Subscription mAnimSub;

  public AuthView(Context context, AttributeSet attrs) {
    super(context, attrs);
    mDen = ViewHelper.getDensity(context);
    mBounds = new ChangeBounds();
    mFade = new Fade();
    mScaleAnim = AnimatorInflater.loadAnimator(context, R.animator.logo_scale_animator);
  }

  @Override
  protected void initDagger(Context context) {
    if (!isInEditMode()) {
      DaggerService.<AuthScreen.Component>getDaggerComponent(context).inject(this);
      mScreen = Flow.getKey(this);
    }
  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    ButterKnife.bind(this);
    mProgressDialog = new ProgressDialog(getContext());
    startLogoAnim();
  }

  @Override
  public void onAttachedToWindow() {
    super.onAttachedToWindow();
    if (!isInEditMode()) mPresenter.takeView(this);
    showViewFromState();
  }

  @Override
  public void onDetachedFromWindow() {
    mAnimSub.unsubscribe();
    super.onDetachedFromWindow();
    if (!isInEditMode()) mPresenter.dropView(this);
  }

  //region===================== IAuthView ==========================
  @Override
  public IAuthPresenter getPresenter() {
    return mPresenter;
  }

  @Override
  public void showLoginBtn() {
    mLoginBtn.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideLoginBtn() {
    mLoginBtn.setVisibility(View.GONE);
  }

  @Override
  public void showProgressDialog(String title) {
    mProgressDialog.setTitle(title);
    mProgressDialog.setCancelable(false);
    mProgressDialog.show();
  }

  @Override
  public void hideProgressDialog() {
    if (mProgressDialog.isShowing()) {
      mProgressDialog.hide();
    }
  }

  @Override
  public String getUserEmail() {
    return String.valueOf(mLoginEt.getText());
  }

  @Override
  public String getUserPassword() {
    return String.valueOf(mPasswordEt.getText());
  }

  @Override
  public boolean viewOnBackPressed() {
    if (!isIdle()) {
      showIdleWithAnim();
      return true;
    } else {
      return false;
    }
  }


  //endregion

  //region===================== AuthCard ==========================
  public void showEmailError() {
    mLoginWrap.setErrorEnabled(true);
    mLoginWrap.setError(getContext().getString(R.string.email_error));
    invalidLoginAnimation();
  }

  public void showPasswordError() {
    mPasswordWrap.setErrorEnabled(true);
    mPasswordWrap.setError(getContext().getString(R.string.password_error));
    invalidLoginAnimation();
  }

  public void hideEmailError() {
    mLoginWrap.setErrorEnabled(false);
  }

  public void hidePasswordError() {
    mPasswordWrap.setErrorEnabled(false);
  }

  public boolean isIdle() {
    return mScreen.getCustomState() == IDLE_STATE;
  }


  private void showLoginState() {
    CardView.LayoutParams cardParams = (CardView.LayoutParams) mAuthCard.getLayoutParams();
    cardParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
    mAuthCard.setLayoutParams(cardParams);
    mAuthCard.getChildAt(0).setVisibility(VISIBLE);
    mAuthCard.setCardElevation(4 * mDen);
    mShowBtn.setClickable(false);
    mShowBtn.setVisibility(GONE);
    mScreen.setCustomState(LOGIN_STATE);

    //mAuthCard.setVisibility(VISIBLE);
    //mShowBtn.setVisibility(GONE);
  }

  private void showIdleState() {
    CardView.LayoutParams cardParams = (CardView.LayoutParams) mAuthCard.getLayoutParams();
    cardParams.height = ((int) (44 * mDen));
    mAuthCard.setLayoutParams(cardParams);
    mAuthCard.getChildAt(0).setVisibility(INVISIBLE);
    mAuthCard.setCardElevation(0f);
    mShowBtn.setClickable(true);
    mShowBtn.setVisibility(VISIBLE);
    mScreen.setCustomState(IDLE_STATE);

    //mAuthCard.setVisibility(GONE);
    //mShowBtn.setVisibility(VISIBLE);
  }

  private void showViewFromState() {
    if (isIdle()) {
      showIdleState();
    } else {
      showLoginState();
    }
  }
  //endregion

  //region===================== Events ==========================

  @OnClick(R.id.login_btn)
  void clickOnLogin() {
    mPresenter.clickOnLogin();
  }

  @OnClick(R.id.show_btn)
  void clickOnShow() {
    mPresenter.clickOnShowCatalog();
  }

  @OnClick(R.id.vk_btn)
  void clickOnVk() {
    mPresenter.clickOnVk();
  }

  @OnClick(R.id.fb_btn)
  void clickOnFb() {
    mPresenter.clickOnFb();
  }

  @OnClick(R.id.twitter_btn)
  void clickOnTwitter() {
    mPresenter.clickOnTwitter();
  }

  //endregion

  //region===================== Animation ==========================

  private void invalidLoginAnimation(){
    //ObjectAnimator oa = ObjectAnimator.ofFloat(mLoginBtn, "rotationY", 4f);
    //ObjectAnimator ob = ObjectAnimator.ofFloat(mLoginBtn, "rotationY", -4f);
    //ObjectAnimator oc = ObjectAnimator.ofFloat(mLoginBtn, "rotationY", 0f);
    //
    //AnimatorSet set = new AnimatorSet();
    //
    //set.playSequentially(oa, ob, oc);
    //set.setDuration(300);
    //set.start();

    AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.invalid_field_animator);
    set.setTarget(mAuthCard);
    set.start();
  }

  public void showLoginWithAnim(){
    //Transition tr = new Slide(Gravity.LEFT);
    //FrameLayout root = ((FrameLayout) findViewById(R.id.panel_wrapper));
    //Scene authScene = Scene.getSceneForLayout(root, R.layout.auth_panel_scene, getContext());
    //TransitionManager.go(authScene, tr);

    TransitionSet set = new TransitionSet();
    set.addTransition(mBounds) // анимируем положение и границы
        .addTransition(mFade) // анимируем прозрачность
        .setDuration(300)
        .setInterpolator(new FastOutSlowInInterpolator())
        .setOrdering(TransitionSet.ORDERING_SEQUENTIAL);

    TransitionManager.beginDelayedTransition(mPanelWrapper, set);

    showLoginState();
  }

  private void showIdleWithAnim() {
    Fade fade = new Fade();
    fade.addTarget(mAuthCard.getChildAt(0));

    // TODO: 21.02.17 solve problem with card size (in showError mode)

    TransitionSet set = new TransitionSet();
    set.addTransition(fade)
        .addTransition(mBounds)
        .addTransition(mFade)
        .setDuration(300)
        .setInterpolator(new FastOutSlowInInterpolator())
        .setOrdering(TransitionSet.ORDERING_SEQUENTIAL);

    TransitionManager.beginDelayedTransition(mPanelWrapper, set);
    showIdleState();
  }

  private void startLogoAnim() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      AnimatedVectorDrawable avd = ((AnimatedVectorDrawable) mLogoImg.getDrawable());
      mScaleAnim.setTarget(mLogoImg);
      mAnimSub = Observable.interval(6, TimeUnit.SECONDS)
          .subscribeOn(Schedulers.computation())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(aLong -> {
            avd.start();
            mScaleAnim.start();
          });

      avd.start();
    }
  }

  //endregion
}
