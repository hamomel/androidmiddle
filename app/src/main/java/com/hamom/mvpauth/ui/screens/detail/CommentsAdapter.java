package com.hamom.mvpauth.ui.screens.detail;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.dto.ProductCommentDto;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.utils.ConstantManager;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

/**
 * Created by hamom on 24.12.16.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {
  private static String TAG = ConstantManager.TAG_PREFIX + "CommentsAdapter: ";
  @Inject
  Picasso mPicasso;
  private List<ProductCommentDto> mComments;

  public CommentsAdapter(List<ProductCommentDto> comments) {
    mComments = comments;
  }

  public void addItem(ProductCommentDto dto){
    mComments.add(dto);
    notifyDataSetChanged();
  }

  @Override
  public void onAttachedToRecyclerView(RecyclerView recyclerView) {
    DaggerService.<DetailScreen.Component>getDaggerComponent(recyclerView.getContext()).inject(this);
    super.onAttachedToRecyclerView(recyclerView);
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View view = inflater.inflate(R.layout.item_review, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(CommentsAdapter.ViewHolder holder, int position) {
    ProductCommentDto comment = mComments.get(position);
    String avatar;
    if (comment.getAvatar() != null && (comment.getAvatar().trim().isEmpty())){
      avatar = null;
    } else {
      avatar = comment.getAvatar();
    }
    mPicasso.load(avatar).error(R.drawable.no_image).placeholder(R.drawable.no_image)
        .into(holder.avatarReview);
    holder.nameReview.setText(comment.getName());
    holder.ratingBarReview.setRating(comment.getRaiting());
    holder.reviewText.setText(comment.getComment());
    if (comment.getCommentDate() != null) {
      holder.reviewTime.setText(timeCounter(comment.getCommentDate()));
    }
  }

  private CharSequence timeCounter(String commentDate) {
    Log.d(TAG, "timeCounter: " + commentDate);
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);
    Date date = new Date();
    try {
      date = format.parse(commentDate);
    } catch (ParseException e) {
      e.printStackTrace();
    }

    return DateUtils.getRelativeTimeSpanString(date.getTime(), System.currentTimeMillis(),
        DateUtils.MINUTE_IN_MILLIS);
  }

  @Override
  public int getItemCount() {
    return mComments.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.avatar_review)
    CircleImageView avatarReview;
    @BindView(R.id.name_review)
    TextView nameReview;
    @BindView(R.id.ratingBar_review)
    RatingBar ratingBarReview;
    @BindView(R.id.review_time)
    TextView reviewTime;
    @BindView(R.id.review_text)
    TextView reviewText;

    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
