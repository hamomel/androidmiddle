package com.hamom.mvpauth.ui.screens.product;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.dto.ProductDto;
import com.hamom.mvpauth.data.storage.dto.ProductLocalInfo;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.mvp.views.IProductView;
import com.hamom.mvpauth.ui.custom_views.AspectRatioImageView;
import com.hamom.mvpauth.utils.ConstantManager;
import com.hamom.mvpauth.utils.ViewHelper;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.ChangeImageTransform;
import com.transitionseverywhere.Explode;
import com.transitionseverywhere.SidePropagation;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;
import java.util.ArrayList;
import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by hamom on 27.11.16.
 */

public class ProductView extends LinearLayout implements IProductView {
  private static String TAG = ConstantManager.TAG_PREFIX + "ProductView: ";
  @Inject
  ProductScreen.ProductPresenter mProductPresenter;

  @Inject
  Picasso mPicasso;

  @BindView(R.id.product_card)
  CardView mProductCard;
  @BindView(R.id.product_name_txt)
  TextView mProductName;
  @BindView(R.id.product_description)
  TextView mProductDesc;
  @BindView(R.id.product_price)
  TextView mProductPrice;
  @BindView(R.id.product_count)
  TextView mProductCount;
  @BindView(R.id.product_wrapper)
  LinearLayout mProductWrapper;

  @BindView(R.id.product_image)
  AspectRatioImageView mProductImage;
  @BindView(R.id.favorite_btn)
  CheckBox mFavoriteBtn;

  private AnimatorSet mResultSet;
  private ArrayList<View> mChildsList;
  private boolean isZoomed;
  private int mMinImageHeight;
  private float mDen;

  public ProductView(Context context, AttributeSet attrs) {
    super(context, attrs);
    if (!isInEditMode()) {
      DaggerService.<ProductScreen.Component>getDaggerComponent(context).inject(this);
    }
    mDen = ViewHelper.getDensity(context);
  }

  //region===================== Events ==========================
  @OnClick(R.id.favorite_btn)
  void clickOnFavorite() {
    mProductPresenter.clickOnFavorite();
  }

  @OnClick(R.id.show_more_btn)
  void clickOnShowMore() {
    mProductPresenter.clickOnShowMore();
  }

  @OnClick(R.id.plus_btn)
  void onPlusClick() {
    mProductPresenter.clickOnPlus();
  }

  @OnClick(R.id.minus_btn)
  void onMinusClick() {
    mProductPresenter.clickOnMinus();
  }

  @OnClick(R.id.product_image)
  void zoomImage(){
    startZoomTransition();
  }


  //endregion

  //region===================== LIfeCycle ==========================

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    ButterKnife.bind(this);

    if (!isInEditMode()) {

    }
  }

  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    if (!isInEditMode()) {
      mProductPresenter.takeView(this);
    }
  }

  @Override
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    if (!isInEditMode()) {
      mProductPresenter.dropView(this);
    }
  }
  //endregion

  //region===================== IProductView ==========================
  @Override
  public void showProductView(final ProductDto product) {
    mProductName.setText(product.getProductName());
    mProductDesc.setText(product.getDescription());
    mProductCount.setText(String.valueOf(product.getCount()));
    mFavoriteBtn.setChecked(product.isFavorite());
    if (product.getCount() > 0) {
      mProductPrice.setText(String.valueOf(product.getPrice() * product.getCount() + ".-"));
    } else {
      mProductPrice.setText(String.valueOf(product.getPrice()));
    }

    mPicasso.load(product.getImageUrl())
        .networkPolicy(NetworkPolicy.OFFLINE)
        .placeholder(R.drawable.no_image)
        //.fit()
        //.centerCrop()
        .into(mProductImage, new Callback() {
          @Override
          public void onSuccess() {
          }

          @Override
          public void onError() {
            mPicasso.load(product.getImageUrl())
                .placeholder(R.drawable.no_image)
                //.fit()
                //.centerCrop()
                .into(mProductImage);
          }
        });
  }

  @Override
  public void updateProductCountView(ProductDto product) {
    mProductCount.setText(String.valueOf(product.getCount()));
    if (product.getCount() > 0) {
      mProductPrice.setText(String.valueOf(product.getPrice() * product.getCount() + ".-"));
    }
  }

  @Override
  public boolean viewOnBackPressed() {
    return false;
  }
  //endregion

  //region===================== Animation ==========================
  @SuppressWarnings("NewApi")
  public void startAddToCartAnim(){
    final int cx = (mProductWrapper.getLeft() + mProductWrapper.getRight()) / 2;
    final int cy = (mProductWrapper.getTop() + mProductWrapper.getBottom()) / 2;
    final int radius = Math.max(mProductWrapper.getWidth(), mProductWrapper.getHeight());
    Animator hideCircleAnim = null;
    Animator showCircleAnim = null;
    Animator showColorAnim = null;
    Animator hideColorAnim = null;

    // анимация Reveal ВСЕГДА должна создаваться вновь!!
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
      hideCircleAnim = ViewAnimationUtils.createCircularReveal(mProductWrapper, cx, cy, radius, 0);
      hideCircleAnim.addListener(new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
          super.onAnimationEnd(animation);
          mProductWrapper.setVisibility(INVISIBLE);
        }
      });

      showCircleAnim = ViewAnimationUtils.createCircularReveal(mProductWrapper, cx, cy, 0, radius);
      showCircleAnim.addListener(new AnimatorListenerAdapter() {
        @Override
        public void onAnimationStart(Animator animation) {
          super.onAnimationEnd(animation);
          mProductWrapper.setVisibility(VISIBLE);
        }
      });

      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
        ColorDrawable cdr = ((ColorDrawable) mProductWrapper.getForeground());
        hideColorAnim = ObjectAnimator.ofArgb(cdr, "color", getResources().getColor(R.color.transparent),
            getResources().getColor(R.color.colorAccent));

        showColorAnim = ObjectAnimator.ofArgb(cdr, "color", getResources().getColor(R.color.colorAccent),
            getResources().getColor(R.color.transparent));
      }
    } else {
      // TODO: 21.02.17 add animation for old version
      hideCircleAnim = ObjectAnimator.ofFloat(mProductWrapper, "alpha", 0);
      showCircleAnim = ObjectAnimator.ofFloat(mProductWrapper, "alpha", 1);
    }

    AnimatorSet hideSet = new AnimatorSet();
    AnimatorSet showSet = new AnimatorSet();

    addAnimatorTogetherInSet(hideSet, hideCircleAnim, hideColorAnim);
    addAnimatorTogetherInSet(showSet, showCircleAnim, showColorAnim);
    hideSet.setDuration(600);
    hideSet.setInterpolator(new FastOutSlowInInterpolator());

    showSet.setInterpolator(new FastOutSlowInInterpolator());
    showSet.setStartDelay(500);
    showSet.setDuration(600);

    if (mResultSet != null && !mResultSet.isStarted() || mResultSet == null){
      mResultSet = new AnimatorSet();
      mResultSet.playSequentially(hideSet, showSet);
      mResultSet.start();
    }
  }

  private void addAnimatorTogetherInSet(AnimatorSet set, Animator... anims){
    ArrayList<Animator> animatorList = new ArrayList<>();

    for (Animator anim : anims) {
      if (anim != null) {
        animatorList.add(anim);
      }
    }
    set.playTogether(animatorList);
  }

  public void startZoomTransition(){
    Transition explode = new Explode();
    TransitionSet set = new TransitionSet();
    final Rect rect = new Rect(mProductImage.getLeft(), mProductImage.getTop(), mProductImage.getRight(), mProductImage.getBottom());

    // устанавливаем эпицентр взрыва
    explode.setEpicenterCallback(new Transition.EpicenterCallback() {
      @Override
      public Rect onGetEpicenter(Transition transition) {
        return rect;
      }
    });

    // разная скорость движения в зависимости от удаленности от эпицентра
    SidePropagation prop = new SidePropagation();
    prop.setPropagationSpeed(3f);
    explode.setPropagation(prop);

    ChangeBounds changeBounds = new ChangeBounds();
    ChangeImageTransform changeImageTransform = new ChangeImageTransform();

    if (!isZoomed) {
      changeBounds.setStartDelay(100);
      changeImageTransform.setStartDelay(100);
    }

    set.addTransition(explode)
        .addTransition(changeBounds)
        .addTransition(changeImageTransform)
        .setDuration(600)
    .setInterpolator(new FastOutSlowInInterpolator());

    TransitionManager.beginDelayedTransition(mProductCard, set);

    if (mChildsList == null){
      mChildsList = ViewHelper.getChildExcludeView(mProductWrapper, R.id.product_image);
      Log.d(TAG, "startZoomTransition: " + mChildsList);
    }

    ViewGroup.LayoutParams cardParams = mProductCard.getLayoutParams();
    cardParams.height = !isZoomed ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.WRAP_CONTENT;
    mProductCard.setLayoutParams(cardParams);

    ViewGroup.LayoutParams wrapParams = mProductWrapper.getLayoutParams();
    wrapParams.height = !isZoomed ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.WRAP_CONTENT;
    mProductWrapper.setLayoutParams(wrapParams);
    LinearLayout.LayoutParams imgParam;

    if (!isZoomed) {
      mMinImageHeight = mProductImage.getHeight();
      imgParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
          ViewGroup.LayoutParams.MATCH_PARENT);
      imgParam.setMargins(0, 0, 0, 0);
      mProductImage.setAspectRatio(1f);
      mProductImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
    } else {
      imgParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, mMinImageHeight);
      int defMargin = ((int) (16 * mDen));
      imgParam.setMargins(0, defMargin, 0, defMargin);
      mProductImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
      mProductImage.setAspectRatio(1.78f);
    }

    mProductImage.setLayoutParams(imgParam);

    if (!isZoomed) {
      for (View view : mChildsList) {
        view.setVisibility(GONE);
      }
    } else {
      for (View view : mChildsList) {
        view.setVisibility(VISIBLE);
      }
    }
    isZoomed = !isZoomed;
  }

  public boolean isZoomed() {
    return isZoomed;
  }

  //endregion
}
