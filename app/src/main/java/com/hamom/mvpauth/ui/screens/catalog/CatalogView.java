package com.hamom.mvpauth.ui.screens.catalog;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.dto.ProductDto;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.mvp.views.AbstractView;
import com.hamom.mvpauth.mvp.views.ICatalogView;
import com.hamom.mvpauth.ui.screens.product.ProductView;
import com.hamom.mvpauth.utils.ConstantManager;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by hamom on 27.11.16.
 */

public class CatalogView extends AbstractView<CatalogScreen.CatalogPresenter>
    implements ICatalogView {
  private static String TAG = ConstantManager.TAG_PREFIX + "CatalogView: ";

  @BindView(R.id.add_to_cart_btn)
  Button mButton;

  @BindView(R.id.product_pager)
  ViewPager mViewPager;

  @BindView(R.id.circle_indicator)
  CircleIndicator mIndicator;

  private CatalogAdapter mAdapter;

  public CatalogView(Context context, AttributeSet attrs) {
    super(context, attrs);
    mAdapter = new CatalogAdapter();
  }

  @Override
  protected void initDagger(Context context) {
    if (!isInEditMode()) {
      DaggerService.<CatalogScreen.Component>getDaggerComponent(context).inject(this);
    }
  }

  @OnClick(R.id.add_to_cart_btn)
  void clickAddToCart() {
    Log.d(TAG, "clickAddToCart: ");
    mPresenter.clickOnBuyButton(mViewPager.getCurrentItem());
  }

  //region===================== ICatalogView ==========================

  @Override
  public void showCatalogView() {
    mViewPager.setAdapter(mAdapter);
    mIndicator.setViewPager(mViewPager);
    mAdapter.registerDataSetObserver(mIndicator.getDataSetObserver());
  }

  @Override
  public void updateProductCounter() {
    // TODO: 30.10.16 update count product in cart icon
  }

  @Override
  public boolean viewOnBackPressed() {
    if (getCurrentProductView().isZoomed()){
      getCurrentProductView().startZoomTransition();
      return true;
    } else {
      return false;
    }
  }

  public CatalogAdapter getAdapter() {
    return mAdapter;
  }

  public int getCurrentPagerPosition() {
    return mViewPager.getCurrentItem();
  }

  public ProductView getCurrentProductView() {
    return (ProductView) mViewPager.findViewWithTag("Product" + mViewPager.getCurrentItem());
  }


  //endregion
}
