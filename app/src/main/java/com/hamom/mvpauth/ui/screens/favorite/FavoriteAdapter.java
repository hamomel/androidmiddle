package com.hamom.mvpauth.ui.screens.favorite;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import com.hamom.mvpauth.ui.custom_views.AspectRatioImageView;
import com.hamom.mvpauth.utils.App;
import com.squareup.picasso.Picasso;
import io.realm.RealmResults;
import java.util.ArrayList;

/**
 * Created by hamom on 27.02.17.
 */

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ViewHolder> {
  private RealmResults<ProductRealm> mProducts;
  private FavoriteView.OnClickListener mOnClickListener;
  private Picasso mPicasso;


  public FavoriteAdapter(Picasso picasso, FavoriteView.OnClickListener onClickListener) {
    mOnClickListener = onClickListener;
    mPicasso = picasso;
  }



  public void updateAdapter(RealmResults<ProductRealm> products){
    mProducts = products;
    notifyDataSetChanged();
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View view = inflater.inflate(R.layout.item_favorite, parent, false);

    return new ViewHolder(view, mOnClickListener);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    ProductRealm product = mProducts.get(position);
    mPicasso.load(product.getImageUrl())
        .placeholder(R.drawable.no_image)
        .fit()
        .centerInside()
        .into(holder.imageFavorite);

    holder.titleFavorite.setText(product.getProductName());
    holder.descFavorite.setText(product.getProductDescription());
    int price = product.getCount() > 0 ? product.getPrice() * product.getCount() : product.getPrice();
    holder.priceFavorite.setText(String.valueOf(price) + App.getAppContext().getString(R.string.dot_dash));
    holder.favoriteBtnFavorite.setChecked(true);

  }

  @Override
  public int getItemCount() {
    if (mProducts != null) {
      return mProducts.size();
    } else {
      return 0;
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder{
    private FavoriteView.OnClickListener mOnClickListener;

    @BindView(R.id.image_favorite)
    AspectRatioImageView imageFavorite;
    @BindView(R.id.desc_favorite)
    TextView descFavorite;
    @BindView(R.id.title_favorite)
    TextView titleFavorite;
    @BindView(R.id.price_favorite)
    TextView priceFavorite;
    @BindView(R.id.cart_favorite)
    ImageView cartFavorite;
    @BindView(R.id.favorite_btn_favorite)
    CheckBox favoriteBtnFavorite;

    public ViewHolder(View itemView, FavoriteView.OnClickListener onClickListener) {
      super(itemView);
      mOnClickListener = onClickListener;
      ButterKnife.bind(this, itemView);
    }

    //region===================== Events ==========================
    @OnClick(R.id.image_favorite)
    void onImageClick(View view){
      mOnClickListener.onClick(view, mProducts.get(getAdapterPosition()));
    }

    @OnClick(R.id.cart_favorite)
    void onCartClick(View view){
      mOnClickListener.onClick(view, mProducts.get(getAdapterPosition()));
    }

    @OnClick(R.id.favorite_btn_favorite)
    void onBtnClick(View view){
      mOnClickListener.onClick(view, mProducts.get(getAdapterPosition()));
    }
    //endregion

  }
}
