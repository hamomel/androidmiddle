package com.hamom.mvpauth.ui.screens.catalog;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.widget.FrameLayout;
import com.birbit.android.jobqueue.JobManager;
import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.di.scopes.DaggerScope;
import com.hamom.mvpauth.flow.AbstractScreen;
import com.hamom.mvpauth.flow.Screen;
import com.hamom.mvpauth.mvp.models.CatalogModel;
import com.hamom.mvpauth.mvp.presenters.AbstractPresenter;
import com.hamom.mvpauth.mvp.presenters.ICatalogPresenter;
import com.hamom.mvpauth.mvp.presenters.MenuItemHolder;
import com.hamom.mvpauth.mvp.presenters.RootPresenter;
import com.hamom.mvpauth.ui.activities.RootActivity;
import com.hamom.mvpauth.ui.screens.auth.AuthScreen;
import com.hamom.mvpauth.ui.screens.cart.CartScreen;
import com.hamom.mvpauth.ui.screens.product.ProductScreen;
import com.hamom.mvpauth.utils.ConstantManager;
import com.squareup.leakcanary.RefWatcher;
import com.squareup.picasso.Picasso;

import dagger.Provides;
import flow.Flow;
import io.realm.Realm;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;
import mortar.MortarScope;
import rx.Subscriber;

/**
 * Created by hamom on 27.11.16.
 */
@Screen(R.layout.screen_catalog)
public class CatalogScreen extends AbstractScreen<RootActivity.RootActivityComponent> {
  @Override
  public Object createScreenComponent(RootActivity.RootActivityComponent parentComponent) {
    return DaggerService.createComponent(parentComponent, CatalogScreen.Component.class);
  }

  //region===================== DI ==========================
  @dagger.Module
  public static class CatalogPresenterModule {
    @Provides
    @DaggerScope(CatalogScreen.class)
    CatalogModel provideCatalogModel() {
      return new CatalogModel();
    }

    @Provides
    @DaggerScope(CatalogScreen.class)
    CatalogPresenter provideCatalogPresenter() {
      return new CatalogPresenter();
    }
  }

  @dagger.Component(dependencies = RootActivity.RootActivityComponent.class, modules = CatalogPresenterModule.class)
  @DaggerScope(CatalogScreen.class)
  public interface Component {
    void inject(CatalogPresenter catalogPresenter);

    void inject(CatalogView view);

    RefWatcher getRefWatcher();

    RootPresenter getRootPresenter();

    CatalogModel getCatalogModel();

    Picasso getPicasso();

  }
  //endregion

  //region===================== CatalogPresenter ==========================
  public static class CatalogPresenter extends AbstractPresenter<CatalogView, CatalogModel>
      implements ICatalogPresenter {
    private static String TAG = ConstantManager.TAG_PREFIX + "CatalogPresenter: ";
    @Inject
    RefWatcher mRefWatcher;

    private int mLastPagerPosition;

    //region===================== LifeCycle ==========================
    @Override
    protected void onLoad(Bundle savedInstanceState) {
      super.onLoad(savedInstanceState);
      subscribeOnProductObservable();
    }

    @Override
    public void dropView(CatalogView view) {
      mLastPagerPosition = getView().getCurrentPagerPosition();
      super.dropView(view);
    }

    @Override
    protected void onExitScope() {
      super.onExitScope();
      mRefWatcher.watch(this);
    }

    //endregion

    @Override
    protected void initFab() {
      mRootPresenter.newFabBuilder().setVisible(false).build();
    }

    @Override
    protected void initActionBar() {

      mRootPresenter.newActionBarBuilder()
          .setTitle(getView().getContext().getString(R.string.app_name))
          .build();
    }


    @Override
    protected void initDagger(MortarScope scope) {
      ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }
    //region===================== Subscription ==========================

    private void subscribeOnProductObservable() {
      if (getRootView() != null) {
        getRootView().showLoad();
      }

      mSubscriptions.add(mModel.getProductObs().subscribe(new RealmSubscriber()));
      //subscribe(mModel.getProductObs(), new RealmSubscriber());
    }
    //endregion

    //region===================== ICatalogPresenter ==========================
    @Override
    public void clickOnBuyButton(int position) {

      if (getView() != null) {
        if (checkUserAuth()) {
          getView().getCurrentProductView().startAddToCartAnim();
        } else {
          Flow.get(getView()).set(new AuthScreen());
        }
      }

      if (hasView()){
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> getView().getAdapter().getProduct(position).addToCart());
        realm.close();
      }
    }

    @Override
    public boolean checkUserAuth() {
      return mModel.isUserAuth();
    }

    //endregion
    private class RealmSubscriber extends Subscriber<ProductRealm> {
      CatalogAdapter mAdapter = getView().getAdapter();

      @Override
      public void onCompleted() {

      }

      @Override
      public void onError(Throwable e) {

        if (getRootView() != null) {
          getRootView().showError(e);
        }
      }

      @Override
      public void onNext(ProductRealm productRealm) {
        mAdapter.addItem(productRealm);
        if (mAdapter.getCount() - 1 == mLastPagerPosition) {
          getRootView().hideLoad();
          getView().showCatalogView();
        }
      }
    }
  }
  //endregion


  public static class Factory {
    private static String TAG = ConstantManager.TAG_PREFIX + "Factory: ";

    static Context createProductContext(ProductRealm product, Context parentContext) {
      MortarScope parentScope = MortarScope.getScope(parentContext);
      MortarScope childScope = null;
      ProductScreen screen = new ProductScreen(product);
      String scopeName = String.format("%s_%s", screen.getScopeName(), product.getId());

      if (parentScope.findChild(scopeName) == null) {
        childScope = parentScope.buildChild()
            .withService(DaggerService.SERVICE_NAME, screen.createScreenComponent(
                DaggerService.<CatalogScreen.Component>getDaggerComponent(parentContext)))
            .build(scopeName);
      } else {
        childScope = parentScope.findChild(scopeName);
      }
      return childScope.createContext(parentContext);
    }
  }
}
