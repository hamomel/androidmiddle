package com.hamom.mvpauth.ui.custom_views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.hamom.mvpauth.R;

/**
 * Created by hamom on 29.10.16.
 */

public class AspectRatioImageView  extends ImageView {
    private static final float DEFAULT_ASPECT_RATIO = 1.78f;
    private float mAspectRatio;

    public AspectRatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AspectRatioImageView);
        mAspectRatio = a.getFloat(R.styleable.AspectRatioImageView_aspect_ratio, DEFAULT_ASPECT_RATIO);
        a.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int newHeight;
        int newWidth;

        newWidth = getMeasuredWidth();
        newHeight = (int) (newWidth /mAspectRatio);

        setMeasuredDimension(newWidth, newHeight);
    }

    public void setAspectRatio(float ratio){
        mAspectRatio = ratio;
    }

}
