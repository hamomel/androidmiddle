package com.hamom.mvpauth.ui.screens.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import com.hamom.mvpauth.BuildConfig;
import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.dto.ProductCommentDto;
import com.hamom.mvpauth.data.storage.dto.ProductDto;
import com.hamom.mvpauth.data.storage.realm.CommentRealm;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.di.scopes.DetailScope;
import com.hamom.mvpauth.flow.AbstractScreen;
import com.hamom.mvpauth.flow.Screen;
import com.hamom.mvpauth.mvp.models.CatalogModel;
import com.hamom.mvpauth.mvp.presenters.AbstractPresenter;
import com.hamom.mvpauth.mvp.presenters.MenuItemHolder;
import com.hamom.mvpauth.mvp.presenters.RootPresenter;
import com.hamom.mvpauth.mvp.views.IRootView;
import com.hamom.mvpauth.ui.activities.RootActivity;
import com.hamom.mvpauth.ui.screens.catalog.CatalogScreen;
import com.hamom.mvpauth.utils.App;
import com.hamom.mvpauth.utils.ConstantManager;

import com.squareup.leakcanary.RefWatcher;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import java.util.ArrayList;
import java.util.List;

import dagger.Provides;
import flow.TreeKey;
import javax.inject.Inject;
import mortar.MortarScope;
import rx.Observable;

/**
 * Created by hamom on 22.12.16.
 */
@Screen(R.layout.screen_container_detail)
public class DetailScreen extends AbstractScreen<RootActivity.RootActivityComponent> implements TreeKey {
  private static ProductRealm sProductRealm;

  public DetailScreen(ProductRealm productRealm) {
    sProductRealm = productRealm;
  }

  @Override
  public boolean equals(Object o) {
    return ((o instanceof DetailScreen) && ((DetailScreen) o).sProductRealm.equals(sProductRealm))
        || super.equals(o);
  }

  @Override
  public int hashCode() {
    return sProductRealm.hashCode();
  }

  @Override
  public Object createScreenComponent(RootActivity.RootActivityComponent parentComponent) {
    return DaggerService.createComponent(parentComponent, Component.class);
  }

  @Override
  public Object getParentKey() {
    return new CatalogScreen();
  }

  //region===================== DI ==========================
  @dagger.Module
  public static class Module {
    @Provides
    @DetailScope
    DetailPresenter provideDetailPresenter() {
      return new DetailPresenter(sProductRealm);
    }

    @Provides
    CatalogModel provideCatalogModel() {
      return new CatalogModel();
    }
  }

  @dagger.Component(dependencies = RootActivity.RootActivityComponent.class, modules = Module.class)
  @DetailScope
  public interface Component {
    void inject(DetailPresenter detailPresenter);

    void inject(DetailView detailView);

    void inject(CommentsAdapter commentsAdapter);
  }
  //endregion

  //region===================== Presenter ==========================
  public static class DetailPresenter extends AbstractPresenter<DetailView, CatalogModel> {
    @Inject
    RefWatcher mRefWatcher;

    private static String TAG = ConstantManager.TAG_PREFIX + "DetailPresenter: ";

    private ProductRealm mProductRealm;
    private RealmChangeListener mListener;

    public DetailPresenter(ProductRealm productRealm) {
      mProductRealm = productRealm;
    }

    //region===================== LifeCycle ==========================
    @Override
    protected void onLoad(Bundle savedInstanceState) {
      Log.d(TAG, "onLoad: ");
      super.onLoad(savedInstanceState);
      initView(mProductRealm);
      getView().getDetailViewPager().addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
          initFab();
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
      });

      mListener = element -> {
        if (hasView()) {
          initView(mProductRealm);
          initFab();
        }
      };
      mProductRealm.addChangeListener(mListener);
    }

    @Override
    protected void onExitScope() {
      super.onExitScope();
      mRefWatcher.watch(this);
    }
    //endregion

    //region===================== AbstractPresenter ==========================
    @Override
    public void dropView(DetailView view) {
      mProductRealm.removeChangeListener(mListener);
      view.getDetailViewPager().addOnPageChangeListener(null);
      super.dropView(view);
    }

    @Override
    protected void initFab() {
      RootPresenter.FabBuilder builder = mRootPresenter.newFabBuilder().setVisible(true);
      if (getView().getDetailViewPager().getCurrentItem() == 0) {
        builder.setIcon(mProductRealm.isFavorite() ? R.drawable.ic_favorite_border_white_24dp
            : R.drawable.ic_favorite_white_24dp).setAction(v -> onLikeClick());
      } else {
        builder.setIcon(R.drawable.ic_add_white_24dp).setAction(v -> onAddCommentClick());
      }
      builder.build();
    }

    @Override
    protected void initActionBar() {
      Log.d(TAG, "initActionBar: ");
      mRootPresenter.newActionBarBuilder()
          .setBackArrow(true)
          .setTab(getView().getDetailViewPager())
          .setTitle(mProductRealm.getProductName())
          .build();
    }

    @Override
    protected void initDagger(MortarScope scope) {
      ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    @Nullable
    @Override
    protected IRootView getRootView() {
      return mRootPresenter.getRootView();
    }
    //endregion

    private void initView(ProductRealm product) {
      ProductDto productDto = new ProductDto(product);

      List<CommentRealm> commentRealms = mProductRealm.getComments();
      Observable<ProductCommentDto> commentsObs = Observable.just(commentRealms)
          .flatMap(commentRealms1 -> {
            Realm realm = Realm.getDefaultInstance();

            realm.executeTransaction(realm1 -> {
              for (int i = 0; i < commentRealms1.size() -1 ; i++){
                CommentRealm temp;
                for (int j = i + 1; j < commentRealms1.size(); j++){
                  if (commentRealms1.get(i).getCommentDate().getTime() <
                      commentRealms1.get(j).getCommentDate().getTime()){
                    temp = commentRealms1.get(i);
                    commentRealms1.set(i, commentRealms1.get(j));
                    commentRealms1.set(j, temp);
                  }
                }
              }
            });
            realm.close();
            return Observable.from(commentRealms1);
          })
          .map(ProductCommentDto::new)
          .doOnNext(productDto::addComment);

      mSubscriptions.add(commentsObs.subscribe(new ViewSubscriber<ProductCommentDto>() {

        @Override
        public void onCompleted() {
          if (getView() != null) {
            getView().initView(productDto);
          }
          super.onCompleted();
        }

        @Override
        public void onNext(ProductCommentDto dto) {

        }
      }));
    }

    public void onPlusClick() {
      Realm realm = Realm.getDefaultInstance();
      Log.d(TAG, "RealmConfig: " + realm.getConfiguration());
      realm.executeTransaction(realm1 -> mProductRealm.add());
      realm.close();
    }

    public void onMinusClick() {
      Log.d(TAG, "onMinusClick: ");
      Realm realm = Realm.getDefaultInstance();
      realm.executeTransaction(realm1 -> mProductRealm.remove());
      realm.close();
    }

    public void onLikeClick() {
      if (mProductRealm.isFavorite()){
        mModel.deleteFavoriteJob(mProductRealm.getId());
      } else {
        mModel.addFavoriteJob(mProductRealm.getId());
      }
      Realm realm = Realm.getDefaultInstance();
      realm.executeTransaction(realm1 -> mProductRealm.changeFavorite());
      realm.close();
    }

    public void onAddCommentClick() {
      if (!TextUtils.isEmpty(mModel.getUserName())){
        Log.d(TAG, "onAddCommentClick: " + mModel.getUserName());
        if (getView() != null) {
          getView().showAddCommentDialog();
        }
      } else {
        if (getRootView() != null) getRootView()
            .showMessage(App.getAppContext().getString(R.string.fill_user_data));
      }

    }

    public void addNewComment(float rating, String text) {
      CommentRealm comment = new CommentRealm(mModel.getUserName(), rating, text);

      switch (BuildConfig.FLAVOR){
        case "base":
          mModel.sendComment(mProductRealm.getId(), comment);
          break;
        case "realmMp":
          Realm realm = Realm.getDefaultInstance();
          realm.executeTransaction(realm1 -> mProductRealm.addComment(comment));
          realm.close();
          break;
      }



      //endregion
    }
  }
}
