package com.hamom.mvpauth.ui.screens.catalog;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.dto.ProductDto;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import com.hamom.mvpauth.utils.ConstantManager;

import java.util.ArrayList;
import java.util.List;

import mortar.MortarScope;

/**
 * Created by hamom on 30.10.16.
 */

public class CatalogAdapter extends PagerAdapter {
  private static String TAG = ConstantManager.TAG_PREFIX + "CatalogAdapter: ";
  private List<ProductRealm> mProductList = new ArrayList<>();

  public CatalogAdapter() {
  }

  public void addItem(ProductRealm item) {
    mProductList.add(item);
    notifyDataSetChanged();
  }

  public ProductRealm getProduct(int position){
    return mProductList.get(position);
  }

  @Override
  public int getCount() {
    return mProductList.size();
  }

  @Override
  public boolean isViewFromObject(View view, Object object) {
    return view.equals(object);
  }

  @Override
  public Object instantiateItem(ViewGroup container, int position) {
    ProductRealm product = mProductList.get(position);
    Context productContext =
        CatalogScreen.Factory.createProductContext(product, container.getContext());
    Log.d(TAG, "instantiateItem: " + product.getId());
    View newView =
        LayoutInflater.from(productContext).inflate(R.layout.screen_product, container, false);
    newView.setTag("Product" + position);
    container.addView(newView);
    return newView;
  }

  @Override
  public void destroyItem(ViewGroup container, int position, Object object) {
    MortarScope screenScope = MortarScope.getScope(((View) object).getContext());
    container.removeView((View) object);
    screenScope.destroy();
  }
}
