package com.hamom.mvpauth.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.managers.DataManager;
import com.hamom.mvpauth.data.storage.dto.UserInfoDto;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.di.components.AppComponent;
import com.hamom.mvpauth.di.modules.LocalModule;
import com.hamom.mvpauth.di.modules.ModelModule;
import com.hamom.mvpauth.di.modules.NetworkModule;
import com.hamom.mvpauth.di.modules.PicassoCacheModule;
import com.hamom.mvpauth.di.modules.RootModule;
import com.hamom.mvpauth.di.scopes.RootScope;
import com.hamom.mvpauth.flow.TreeKeyDispatcher;
import com.hamom.mvpauth.mvp.models.AbstractModel;
import com.hamom.mvpauth.mvp.models.AccountModel;
import com.hamom.mvpauth.mvp.presenters.MenuItemHolder;
import com.hamom.mvpauth.mvp.presenters.RootPresenter;
import com.hamom.mvpauth.mvp.views.IActionBarView;
import com.hamom.mvpauth.mvp.views.IFabView;
import com.hamom.mvpauth.mvp.views.IRootView;
import com.hamom.mvpauth.mvp.views.IView;
import com.hamom.mvpauth.ui.screens.account.AccountScreen;
import com.hamom.mvpauth.ui.screens.auth.AuthScreen;
import com.hamom.mvpauth.ui.screens.cart.CartScreen;
import com.hamom.mvpauth.ui.screens.catalog.CatalogScreen;
import com.hamom.mvpauth.ui.screens.favorite.FavoriteScreen;
import com.hamom.mvpauth.utils.AppConfig;
import com.hamom.mvpauth.utils.CircularTransformation;
import com.hamom.mvpauth.utils.ConstantManager;
import com.squareup.leakcanary.RefWatcher;
import com.squareup.picasso.Picasso;

import io.realm.RealmResults;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

public class RootActivity extends AppCompatActivity implements IRootView, IActionBarView, IFabView,
    NavigationView.OnNavigationItemSelectedListener {

  private static String TAG = ConstantManager.TAG_PREFIX + "RootActivity: ";

  @BindView(R.id.nav_view)
  NavigationView mNavigationView;

  @BindView(R.id.toolbar)
  Toolbar mToolbar;

  @BindView(R.id.root_coordinator_container)
  CoordinatorLayout mCoordinatorLayout;

  @BindView(R.id.root_frame)
  FrameLayout mRootFrame;

  @BindView(R.id.root_drawer_layout)
  DrawerLayout mDrawerLayout;

  @BindView(R.id.appbar_layout)
  AppBarLayout mAppBarLayout;

  @BindView(R.id.fab)
  FloatingActionButton fab;

  private View mCartIconRoot;
  private ViewGroup mCartIndicatorWrapper;
  private TextView mCartIndicatorTv;
  private boolean isCartIcon = true;

  @Inject
  RefWatcher mRefWatcher;
  @Inject
  RootPresenter mPresenter;
  @Inject
  Picasso mPicasso;

  private static long mBackPressed;
  private ActionBarDrawerToggle mToggle;
  private ActionBar mActionBar;
  private List<MenuItemHolder> mActionBarMenuItems;

  @Override
  protected void attachBaseContext(Context newBase) {
    newBase = Flow.configure(newBase, this)
        .defaultKey(new AuthScreen())
        .dispatcher(new TreeKeyDispatcher(this))
        .install();
    super.attachBaseContext(newBase);
  }

  @Override
  public Object getSystemService(@NonNull String name) {
    MortarScope rootActivityScope =
        MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
    return rootActivityScope.hasService(name) ? rootActivityScope.getService(name)
        : super.getSystemService(name);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_root);
    BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
    ButterKnife.bind(this);
    DaggerService.<RootActivityComponent>getDaggerComponent(this).inject(this);

    createCartIcon();

    initToolbar();
    mPresenter.takeView(this);
  }


  @Override
  protected void onDestroy() {
    mPresenter.dropView(this);
    super.onDestroy();
    mRefWatcher.watch(this);
  }

  @Override
  public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
    super.onSaveInstanceState(outState, outPersistentState);
    BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
  }

  @Override
  public void onBackPressed() {
    if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
      mDrawerLayout.closeDrawer(GravityCompat.START);
    } else {
      if (getCurrentScreen() != null && !getCurrentScreen().viewOnBackPressed() && !Flow.get(this)
          .goBack()) {
        if (mBackPressed + 2000 > System.currentTimeMillis()) {
          super.onBackPressed();
        } else {
          showToast(getString(R.string.press_once_again_to_exit));
        }
        mBackPressed = System.currentTimeMillis();
      }
    }
  }

  /*
    должен скрывать клавиатуру по клику. (НЕ РАБОТАЕТ)
  */
  @Override
  public boolean onTouchEvent(MotionEvent event) {
    InputMethodManager imm = (InputMethodManager) getSystemService(Context.
        INPUT_METHOD_SERVICE);
    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    return true;
  }

  private void showToast(String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    mPresenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    mPresenter.onActivityResult(requestCode, resultCode, data);
  }

  //region===================== ToolBar ==========================
  private void initToolbar() {
    Log.d(TAG, "initToolbar: ");
    setSupportActionBar(mToolbar);
    mActionBar = getSupportActionBar();
    mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.open_drawer,
        R.string.close_drawer);

    mDrawerLayout.addDrawerListener(mToggle);
    mToggle.syncState();
    mNavigationView.setNavigationItemSelectedListener(this);
  }

  @Override
  public void setToolbarTitle(CharSequence title) {
    if (mActionBar != null) {
      mActionBar.setTitle(title);
    }
  }

  @Override
  public void setToolbarVisible(boolean visible) {
    if (visible) {
      mToolbar.setVisibility(View.VISIBLE);
    } else {
      mToolbar.setVisibility(View.GONE);
    }
  }

  @Override
  public void setBackArrow(boolean enabled) {
    if (mToggle != null && mActionBar != null) {
      Log.d(TAG, "setBackArrow: " + enabled);
      if (enabled) {
        mToggle.setDrawerIndicatorEnabled(false);
        mActionBar.setDisplayHomeAsUpEnabled(true);
        if (mToggle.getToolbarNavigationClickListener() == null) {
          mToggle.setToolbarNavigationClickListener(v -> onBackPressed());
        }
      } else {
        mActionBar.setDisplayHomeAsUpEnabled(false);
        mToggle.setDrawerIndicatorEnabled(true);
        mToggle.setToolbarNavigationClickListener(null);
      }

      mDrawerLayout.setDrawerLockMode(
          enabled ? DrawerLayout.LOCK_MODE_LOCKED_CLOSED : DrawerLayout.LOCK_MODE_UNLOCKED);
      mToggle.syncState();
    }
  }

  @Override
  public void setMenuItems(List<MenuItemHolder> items) {
    Log.d(TAG, "setMenuItems: " + items.size());
    if (isCartIcon){
      MenuItemHolder item = new MenuItemHolder("", mCartIconRoot);
      items.add(0, item);
    }

    if (items.size() == 0) {
      items.add(new MenuItemHolder("", 0, null));
    }

    mActionBarMenuItems = items;
    supportInvalidateOptionsMenu();
  }

  private void createCartIcon() {
    mCartIconRoot = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE))
        .inflate(R.layout.cart_layout, null);
    mCartIndicatorWrapper = ButterKnife.findById(mCartIconRoot, R.id.cart_indicator_wrapper);
    mCartIndicatorTv = ButterKnife.findById(mCartIconRoot, R.id.cart_indicator_tv);
  }

  @Override
  public void initCartIcon(RealmResults<ProductRealm> productRealms) {
    if (productRealms.size() > 0){
      mCartIndicatorWrapper.setVisibility(View.VISIBLE);
    } else {
      mCartIndicatorWrapper.setVisibility(View.GONE);
    }
    mCartIndicatorTv.setText(String.valueOf(productRealms.size()));
    mCartIconRoot.setOnClickListener(v -> Flow.get(getBaseContext()).set(new CartScreen()));
  }

  @Override
  public void showCart(boolean showCart) {
    this.isCartIcon = showCart;
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    // FIXME: 15.01.17 menu is not cleared after menu.clear(). it is cause of leakage
    if (mActionBarMenuItems != null && !mActionBarMenuItems.isEmpty()) {
      for (MenuItemHolder actionBarMenuItem : mActionBarMenuItems) {
        MenuItem item = menu.add(actionBarMenuItem.getItemTitle());
        item.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);
        if (actionBarMenuItem.getItemResId() != 0) {
          item.setIcon(actionBarMenuItem.getItemResId());
        }
        if (actionBarMenuItem.getListener() != null){
          item.setOnMenuItemClickListener(actionBarMenuItem.getListener());
        }
        if (actionBarMenuItem.getActionView() != null){
          item.setActionView(actionBarMenuItem.getActionView());
        }
      }

    } else {
      menu.clear();
    }
    return super.onPrepareOptionsMenu(menu);
  }

  @Override
  public void setTabLayout(ViewPager pager) {
    TabLayout tabView = new TabLayout(this);
    tabView.setupWithViewPager(pager);
    mAppBarLayout.addView(tabView);
    pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabView));
  }

  @Override
  public void removeTabLayout() {
    View tabView = mAppBarLayout.getChildAt(1);
    if (tabView != null && tabView instanceof TabLayout) {
      mAppBarLayout.removeView(tabView);
    }
  }

  //    @Override
  //    public boolean onCreateOptionsMenu(Menu menu) {
  //        getMenuInflater().inflate(R.menu.activity_root_appbar, menu);
  //        return true;
  //    }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      mDrawerLayout.openDrawer(GravityCompat.START);
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void hideToolbar() {
    if (getSupportActionBar() != null) getSupportActionBar().hide();
    mRootFrame.setPadding(0, 0, 0, 0);
  }

  @Override
  public void showToolbar() {
    if (getSupportActionBar() != null) getSupportActionBar().show();
    mRootFrame.setPadding(0, getSupportActionBar().getHeight(), 0, 0);
  }

  //endregion

  //region===================== Drawer ==========================

  @Override
  public void initDrawerInfo(UserInfoDto userInfoDto) {
    ImageView avatar =
        (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.nav_header_avatar);
    mPicasso.load(userInfoDto.getAvatar())
        .error(R.drawable.avatar)
        .fit()
        .transform(new CircularTransformation())
        .into(avatar);

    TextView nameTxt =
        (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.nav_header_title);
    nameTxt.setText(userInfoDto.getName());
  }

  @Override
  public boolean onNavigationItemSelected(@NonNull MenuItem item) {
    Object key = null;
    switch (item.getItemId()) {
      case R.id.nav_account:
        key = new AccountScreen();
        break;
      case R.id.nav_catalog:
        key = new CatalogScreen();
        break;
      case R.id.nav_favorities:
        key = new FavoriteScreen();
        break;
      case R.id.nav_orders:
        break;
      case R.id.nav_notifications:
        break;
    }
    if (key != null) {
      Flow.get(this).set(key);
    }
    mDrawerLayout.closeDrawer(GravityCompat.START);
    return false;
  }

  @Override
  public void setMenuItemChecked(View view) {
    int id = 0;
    switch (view.getClass().getSimpleName()) {
      case "AccountView":
        id = R.id.nav_account;
        break;
      case "CatalogView":
        id = R.id.nav_catalog;
        break;
      case "FavoriteView":
        id = R.id.nav_favorities;
        break;
    }
    if (id != 0) {
      mNavigationView.getMenu().findItem(id).setChecked(true);
    }
  }

  //endregion

  //region===================== FAB ==========================
  @Override
  public void setFabVisible(boolean visible) {
    if (visible) {
      fab.setVisibility(View.VISIBLE);
    } else {
      fab.setVisibility(View.GONE);
    }
  }

  @Override
  public void setFabIcon(int resId) {
    fab.setImageResource(resId);
  }

  @Override
  public void setAction(FloatingActionButton.OnClickListener action) {
    fab.setOnClickListener(action);
  }
  //endregion

  //region================IRootView========================
  @Override
  public void showMessage(String message) {
    Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
  }

  @Override
  public void showError(Throwable e) {
    if (AppConfig.DEBUG) {
      showMessage(e.getMessage());

      e.printStackTrace();
    } else {
      showMessage(getString(R.string.something_went_wrong));
      // TODO: 21.10.16 send stacktrace to crashlytics
    }
  }

  @Override
  public void showLoad() {

    // TODO: 21.10.16 show progressbar
  }

  @Override
  public void hideLoad() {

    // TODO: 21.10.16 hide progressbar
  }

  @Override
  public IView getCurrentScreen() {
    return (IView) mRootFrame.getChildAt(0);
  }

  @Override
  public boolean viewOnBackPressed() {
    return false;
  }



  //endregion

  //region===================== DI ==========================
  @dagger.Component(dependencies = AppComponent.class, modules = {
      RootModule.class, PicassoCacheModule.class, //ModelModule.class
  })
  @RootScope
  public interface RootActivityComponent {
    void inject(RootActivity rootActivity);


    void inject(RootPresenter rootPresenter);

    RootPresenter getRootPresenter();

    AccountModel getAccountModel();

    RefWatcher getRefWatcher();

    Picasso getPicasso();
  }

  //endregion
}
