package com.hamom.mvpauth.ui.screens.favorite;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import butterknife.BindView;
import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.mvp.views.AbstractView;
import com.hamom.mvpauth.mvp.views.IView;
import com.squareup.picasso.Picasso;
import flow.Flow;
import io.realm.RealmResults;
import javax.inject.Inject;

/**
 * Created by hamom on 27.02.17.
 */

public class FavoriteView extends AbstractView<FavoriteScreen.FavoritePresenter> implements IView {
  @BindView(R.id.favorite_recycler)
  RecyclerView favoriteRecycler;

  @Inject
  Picasso mPicasso;

  private FavoriteAdapter mFavoriteAdapter;

  public FavoriteView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  protected void initDagger(Context context) {
    if (!isInEditMode()) {
      DaggerService.<FavoriteScreen.Component>getDaggerComponent(context).inject(this);
    }
  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    setAdapter();
  }

  @Override
  public boolean viewOnBackPressed() {
    Flow.get(this).goBack();
    return true;
  }

  private void setAdapter(){
    GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
    favoriteRecycler.setLayoutManager(layoutManager);
    mFavoriteAdapter = new FavoriteAdapter(mPicasso, getOnClickListener());
    favoriteRecycler.setAdapter(mFavoriteAdapter);

  }

  public void initView(RealmResults<ProductRealm> products){
    mFavoriteAdapter.updateAdapter(products);
  }

  private OnClickListener getOnClickListener(){

      return (view, product) -> {
        switch (view.getId()){
          case R.id.image_favorite:
            mPresenter.onImageClick(product);
            break;
          case R.id.cart_favorite:
            mPresenter.onCartClick(product);
            break;
          case R.id.favorite_btn_favorite:
            mPresenter.onFavoriteClick(product);
            break;
        }
      };

  }

  public interface OnClickListener{
    void onClick(View view, ProductRealm product);
  }
}
