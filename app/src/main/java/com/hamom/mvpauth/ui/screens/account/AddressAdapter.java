package com.hamom.mvpauth.ui.screens.account;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.dto.UserAddressDto;

import com.hamom.mvpauth.utils.ConstantManager;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hamom on 29.11.16.
 */

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {
  private static String TAG = ConstantManager.TAG_PREFIX + "AddressAdapter: ";
  private List<UserAddressDto> mAddressList = new ArrayList<>();
  private Context mContext;

  @Override
  public void onAttachedToRecyclerView(RecyclerView recyclerView) {
    super.onAttachedToRecyclerView(recyclerView);
    mContext = recyclerView.getContext();
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View view = inflater.inflate(R.layout.item_list_adress, parent, false);
    return new ViewHolder(view);
  }

  public int getItemIntId(int position) {
    return mAddressList.get(position).getId();
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    UserAddressDto addressDto = mAddressList.get(position);

    String address =
        addressDto.getStreet() + mContext.getString(R.string.house_abbr) + addressDto.getHome();
    if (!TextUtils.isEmpty(addressDto.getApartment())) {
      address = address.concat(mContext.getString(R.string.apartment_abbr) + addressDto.getApartment());
    }

    holder.mAddressEt.setText(address);
    holder.mCommentEt.setText(addressDto.getComment());
  }

  @Override
  public int getItemCount() {
    return mAddressList.size();
  }

  public void removeItem(int position) {
    notifyItemRemoved(position);
    notifyItemRangeChanged(position, mAddressList.size());
  }

  public void addItem(UserAddressDto address) {
    mAddressList.add(address);
    notifyDataSetChanged();
  }

  public void reloadAdapter() {
    mAddressList.clear();
    Log.d(TAG, "reloadAdapter: " + mAddressList);
    notifyDataSetChanged();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.place_address_et)
    EditText mAddressEt;
    @BindView(R.id.comment_et)
    EditText mCommentEt;

    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
