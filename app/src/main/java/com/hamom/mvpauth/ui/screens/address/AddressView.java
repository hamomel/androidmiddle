package com.hamom.mvpauth.ui.screens.address;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.hamom.mvpauth.R;
import com.hamom.mvpauth.data.storage.dto.UserAddressDto;
import com.hamom.mvpauth.di.DaggerService;
import com.hamom.mvpauth.mvp.views.AbstractView;
import com.hamom.mvpauth.mvp.views.IAddressView;
import com.hamom.mvpauth.ui.screens.account.AccountScreen;

import com.hamom.mvpauth.utils.ConstantManager;
import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import flow.Flow;

/**
 * Created by hamom on 30.11.16.
 */

public class AddressView extends AbstractView<AddressScreen.AddressPresenter>
    implements IAddressView {
  private static String TAG = ConstantManager.TAG_PREFIX + "AddressView: ";
  @Inject
  AddressScreen.AddressPresenter mPresenter;

  @BindView(R.id.place_et)
  EditText mPlaceEt;

  @BindView(R.id.street_et)
  EditText mStreetEt;

  @BindView(R.id.street_ti_layout)
  TextInputLayout streetTiLayout;

  @BindView(R.id.home_et)
  EditText mHomeEt;

  @BindView(R.id.home_ti_layout)
  TextInputLayout homeTiLayout;


  @BindView(R.id.apartment_et)
  EditText mApartmentEt;

  @BindView(R.id.stage_et)
  EditText mStageEt;

  @BindView(R.id.note_et)
  EditText mCommentEt;

  @BindView(R.id.save_address_btn)
  Button mSaveAddressBtn;

  private int mAddressId = 0;

  public AddressView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  protected void initDagger(Context context) {
    if (!isInEditMode()) {
      DaggerService.<AddressScreen.Component>getDaggerComponent(context).inject(this);
    }
  }

  //region===================== IAddressView ==========================

  public void initView(@Nullable UserAddressDto address) {
    if (address != null) {
      mAddressId = address.getId();
      mPlaceEt.setText(address.getPlaceName());
      mStreetEt.setText(address.getStreet());
      mHomeEt.setText(address.getHome());
      mApartmentEt.setText(address.getApartment());
      mStageEt.setText(String.valueOf(address.getStage()));
      mCommentEt.setText(address.getComment());
      mSaveAddressBtn.setText(R.string.save);
    }
  }

  @Override
  public void showInputError() {
    homeTiLayout.setError(getContext().getString(R.string.field_cant_be_empty));
    streetTiLayout.setError(getContext().getString(R.string.field_cant_be_empty));
  }

  @Override
  public UserAddressDto getUserAddress() {
    int stage = 0;
    if (!TextUtils.isEmpty(mStageEt.getText().toString())) {
      stage = Integer.parseInt(mStageEt.getText().toString());
    }
    return new UserAddressDto(mAddressId, mCommentEt.getText().toString(), stage,
        mApartmentEt.getText().toString(), mHomeEt.getText().toString(),
        mStreetEt.getText().toString(), mPlaceEt.getText().toString());
  }

  @Override
  public boolean viewOnBackPressed() {
    return false;
  }
  //endregion

  //region===================== Events ==========================

  @OnClick(R.id.save_address_btn)
  void onSaveAddressClick() {
    Log.d(TAG, "onSaveAddressClick: ");
    mPresenter.clickOnAddAddress();
  }
  //endregion
}
