package com.hamom.mvpauth.jobs;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.hamom.mvpauth.utils.App;
import com.hamom.mvpauth.utils.AppConfig;
import com.hamom.mvpauth.utils.ConstantManager;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by hamom on 11.03.17.
 */

public class DeleteFavoriteJob extends Job {
  private static String TAG = ConstantManager.TAG_PREFIX + "DeleteFavJob: ";
  private static Set<String> mProdIds = new HashSet<>();

  public DeleteFavoriteJob(String prodId) {
    super(new Params(JobPriority.MID).requireNetwork().persist().singleInstanceBy("deleteFavorite"));
    mProdIds.add(prodId);
  }

  @Override
  public void onAdded() {
    Log.d(TAG, "onAdded: " );

  }

  @Override
  public void onRun() throws Throwable {
    if (mProdIds.size() < 1){
      return;
    }
    Log.d(TAG, "onRun: " );
    App.getAppComponent().getDataManger().deleteFavorite(mProdIds)
        .subscribe(voidResponse -> {
          if (voidResponse.code() == 201){
            Log.d(TAG, "onRun: success ");
            mProdIds.clear();
          }
        });

  }

  @Override
  protected void onCancel(int cancelReason, @Nullable Throwable throwable) {

  }

  @Override
  protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount,
      int maxRunCount) {
    return RetryConstraint.createExponentialBackoff(runCount, AppConfig.INITIAL_BACK_OFF_IN_MS);
  }
}
