package com.hamom.mvpauth.jobs;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.hamom.mvpauth.data.managers.DataManager;
import com.hamom.mvpauth.data.network.res.CommentRes;
import com.hamom.mvpauth.data.storage.realm.CommentRealm;
import com.hamom.mvpauth.data.storage.realm.ProductRealm;
import com.hamom.mvpauth.utils.App;
import com.hamom.mvpauth.utils.AppConfig;
import com.hamom.mvpauth.utils.ConstantManager;
import io.realm.Realm;

/**
 * Created by hamomel on 27.01.17.
 */

public class SendMessageJob extends Job {
  private static String TAG = ConstantManager.TAG_PREFIX + "SendMessageJob: ";
  private final CommentRealm mComment;
  private final String mProductId;

  public SendMessageJob(String productId, CommentRealm comment) {
    super(new Params(JobPriority.MID)
    .requireNetwork()
    .persist()
    .groupBy("Comments"));
    mComment = comment;
    mProductId = productId;
  }

  @Override
  public void onAdded() {
    Log.d(TAG, "MESSAGE onAdded: ");
    Realm realm = Realm.getDefaultInstance();
    ProductRealm product = realm.where(ProductRealm.class).equalTo("id", mProductId).findFirst();
    realm.executeTransaction(realm1 -> product.getComments().add(mComment));
    realm.close();
  }

  @Override
  public void onRun() throws Throwable {
    Log.d(TAG, "MESSAGE onRun: ");
    CommentRes comment = new CommentRes(mComment);
    App.getAppComponent().getDataManger().sendComment(mProductId, comment)
        .subscribe(commentRes -> {
          Realm realm = Realm.getDefaultInstance();
          CommentRealm localComment =
              realm.where(CommentRealm.class).equalTo("id", mComment.getId()).findFirst();

          ProductRealm product =
              realm.where(ProductRealm.class).equalTo("id", mProductId).findFirst();

          CommentRealm serverComment = new CommentRealm(commentRes);

          realm.executeTransaction(realm1 -> {
            localComment.deleteFromRealm();
            product.getComments().add(serverComment);
          });
          realm.close();
        });

  }

  @Override
  protected void onCancel(int cancelReason, @Nullable Throwable throwable) {

  }

  @Override
  protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount,
      int maxRunCount) {
    Log.d(TAG, "MESSAGE shouldReRunOnThrowable: " + runCount);
    return RetryConstraint.createExponentialBackoff(runCount, AppConfig.INITIAL_BACK_OFF_IN_MS);
  }
}
