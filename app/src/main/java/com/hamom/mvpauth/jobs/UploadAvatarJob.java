package com.hamom.mvpauth.jobs;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.hamom.mvpauth.utils.App;
import com.hamom.mvpauth.utils.AppConfig;
import com.hamom.mvpauth.utils.ConstantManager;
import java.io.File;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by hamomel on 27.01.17.
 */

public class UploadAvatarJob extends Job {
  private static String TAG = ConstantManager.TAG_PREFIX + "UploadAvatarJob: ";
  private String mImageUrl;
  public UploadAvatarJob(String imageUrl) {
    super (new Params(JobPriority.HIGH)
               .requireNetwork()
               .persist());
    mImageUrl = imageUrl;
  }

  @Override
  public void onAdded() {
    Log.d(TAG, "UPLOAD onAdded: ");
  }

  @Override
  public void onRun() throws Throwable {
    File file = new File(Uri.parse(mImageUrl).getPath());
    Log.d(TAG, "UPLOAD onRun: " + file.getAbsolutePath());
    RequestBody sendFile = RequestBody.create(MediaType.parse("multipart/from-data"), file);

    MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", file.getName(), sendFile);

    App.getAppComponent().getDataManger().uploadAvatar(body).subscribe(avatarRes -> {
      Log.d(TAG, "avatar uploaded ");
      App.getAppComponent().getDataManger().getAppPreferencesManager()
          .saveUserAvatar(avatarRes.getAvatarUrl());
    });
  }

  @Override
  protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
    Log.d(TAG, "UPLOAD onCancel: ");
  }

  @Override
  protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount,
      int maxRunCount) {
    Log.d(TAG, "UPLOAD shouldReRunOnThrowable: ");
    return RetryConstraint.createExponentialBackoff(runCount, AppConfig.INITIAL_BACK_OFF_IN_MS);
  }
}
