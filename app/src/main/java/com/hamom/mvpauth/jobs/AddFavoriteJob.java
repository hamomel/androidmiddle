package com.hamom.mvpauth.jobs;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.hamom.mvpauth.data.managers.DataManager;
import com.hamom.mvpauth.utils.App;
import com.hamom.mvpauth.utils.AppConfig;
import com.hamom.mvpauth.utils.ConstantManager;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by hamom on 10.03.17.
 */

public class AddFavoriteJob extends Job {
  private static String TAG = ConstantManager.TAG_PREFIX + "AddFavoriteJob: ";

  private static Set<String> mProdIds = new HashSet<>();

  private int mId;

  public AddFavoriteJob(String prodId) {
    super(new Params(JobPriority.MID).requireNetwork().persist().singleInstanceBy("addFavorite"));
    mProdIds.add(prodId);
  }

  @Override
  public void onAdded() {
    Log.d(TAG, "onAdded: " + hashCode() + " " + mId + " " + mProdIds);
  }

  @Override
  public void onRun() throws Throwable {
    if (mProdIds.size() < 1){
      return;
    }
    Log.d(TAG, "onRun: " + hashCode() + " " + mId + " " + mProdIds);
    App.getAppComponent().getDataManger().sendFavorite(mProdIds).subscribe(voidResponse -> {
    if (voidResponse.code() == 201){
      Log.d(TAG, "onRun: success " + hashCode() + " " + mProdIds );
      mProdIds.clear();
    } else {
      Log.d(TAG, "onRun: fail " + hashCode() + " " + mProdIds);
    }
  });
  }

  @Override
  protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
    Log.d(TAG, "onCancel: " + hashCode());
  }

  @Override
  protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount,
      int maxRunCount) {
    Log.d(TAG, "shouldReRunOnThrowable: " + mId + " " + hashCode() + " " + throwable.getMessage());
    return RetryConstraint.createExponentialBackoff(runCount, AppConfig.INITIAL_BACK_OFF_IN_MS);
  }
}
