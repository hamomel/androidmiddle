package com.hamom.mvpauth.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by hamom on 05.11.16.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface RootScope {
}
