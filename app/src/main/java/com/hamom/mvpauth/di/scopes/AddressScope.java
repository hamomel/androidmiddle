package com.hamom.mvpauth.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by hamom on 30.11.16.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface AddressScope {
}
