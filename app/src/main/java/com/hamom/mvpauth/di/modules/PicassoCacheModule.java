package com.hamom.mvpauth.di.modules;

import android.content.Context;
import android.util.Log;

import com.hamom.mvpauth.di.scopes.RootScope;
import com.hamom.mvpauth.utils.ConstantManager;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hamom on 04.11.16.
 */
@Module
public class PicassoCacheModule {
    private static String TAG = ConstantManager.TAG_PREFIX + "PCacheModule: ";
    @Provides
    @RootScope
    Picasso providePicasso(Context context) {
        OkHttp3Downloader okHttpDownloader = new OkHttp3Downloader(context);
        Picasso picasso = new Picasso.Builder(context)
                                    .downloader(okHttpDownloader)
                                    .debugging(true)
                                    .build();
        Log.d(TAG, "providePicasso() returned: " + picasso);
        Picasso.setSingletonInstance(picasso);
        return picasso;
    }

}
