package com.hamom.mvpauth.di.components;

import com.hamom.mvpauth.data.managers.DataManager;
import com.hamom.mvpauth.di.modules.LocalModule;
import com.hamom.mvpauth.di.modules.NetworkModule;
import com.hamom.mvpauth.di.scopes.RootScope;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by hamom on 04.11.16.
 */
@Component(dependencies = AppComponent.class, modules = {LocalModule.class, NetworkModule.class})
@RootScope
public interface DataManagerComponent {
    void inject(DataManager dataManager);
}
