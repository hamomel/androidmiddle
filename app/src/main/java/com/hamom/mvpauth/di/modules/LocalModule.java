package com.hamom.mvpauth.di.modules;

import android.content.Context;

import com.hamom.mvpauth.data.managers.AppPreferencesManager;
import com.hamom.mvpauth.data.managers.RealmManager;
import com.hamom.mvpauth.di.scopes.RootScope;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hamom on 04.11.16.
 */
@Module
public class LocalModule extends FlavorLocalModule{

    @Provides
    @Singleton
    AppPreferencesManager providePreferencesManager(Context context){
        return new AppPreferencesManager(context);
    }

}
