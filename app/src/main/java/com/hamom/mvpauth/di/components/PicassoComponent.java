package com.hamom.mvpauth.di.components;

import com.hamom.mvpauth.di.modules.PicassoCacheModule;
import com.hamom.mvpauth.di.scopes.RootScope;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by hamom on 04.11.16.
 */
@Component(dependencies = AppComponent.class, modules = PicassoCacheModule.class)
@RootScope
public interface PicassoComponent {
    Picasso getPicasso();
}
