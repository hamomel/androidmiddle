package com.hamom.mvpauth.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import javax.inject.Scope;

/**
 * Created by hamom on 27.02.17.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface FavoriteScope {
}
