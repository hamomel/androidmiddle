package com.hamom.mvpauth.di.modules;

import com.hamom.mvpauth.di.scopes.RootScope;
import com.hamom.mvpauth.mvp.models.AccountModel;
import com.hamom.mvpauth.mvp.presenters.RootPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hamom on 06.11.16.
 */
@Module
public class RootModule {
  @Provides
  @RootScope
  RootPresenter provideRootPresenter() {
    return new RootPresenter();
  }

  @Provides
  AccountModel provideAccountModel() {
    return new AccountModel();
  }
}
