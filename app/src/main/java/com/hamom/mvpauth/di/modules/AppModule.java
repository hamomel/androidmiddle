package com.hamom.mvpauth.di.modules;

import android.content.Context;

import com.squareup.leakcanary.RefWatcher;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

/**
 * Created by hamom on 04.11.16.
 */
@Module
public class AppModule {
  private Context mContext;
  private RefWatcher mRefWatcher;

  public AppModule(Context context, RefWatcher refWatcher) {
    mContext = context;
    mRefWatcher = refWatcher;
  }

  @Provides
  @Singleton
  Context provideContext() {
    return mContext;
  }

  @Provides
  @Singleton
  RefWatcher provideRefWatcher() {
    return mRefWatcher;
  }
}
