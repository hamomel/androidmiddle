package com.hamom.mvpauth.di.modules;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.hamom.mvpauth.data.network.RestService;
import com.hamom.mvpauth.utils.AppConfig;
import com.squareup.moshi.Moshi;
import dagger.Module;
import dagger.Provides;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

/**
 * Created by hamom on 04.11.16.
 */
@Module
public class NetworkModule {
  @Provides
  @Singleton
  OkHttpClient provideOkHttpClient() {
    return createClient();
  }

  @Provides
  @Singleton
  Retrofit provideRetrofit(OkHttpClient client) {
    return createRetrofit(client);
  }

  @Provides
  @Singleton
  RestService provideRestService(Retrofit retrofit) {
    return retrofit.create(RestService.class);
  }

  private Retrofit createRetrofit(OkHttpClient client) {
    return new Retrofit.Builder().baseUrl(AppConfig.BASE_URL)
        .addConverterFactory(createConverter())
        .addCallAdapterFactory(RxJavaCallAdapterFactory.create()) //add rx adapter
        .client(client)
        .build();
  }

  private Converter.Factory createConverter() {
    return MoshiConverterFactory.create(new Moshi.Builder().build());
  }

  private OkHttpClient createClient() {
    return new OkHttpClient.Builder().addInterceptor(
        new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .addNetworkInterceptor(new StethoInterceptor())
        .connectTimeout(AppConfig.MAX_CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
        .readTimeout(AppConfig.MAX_READ_TIMEOUT, TimeUnit.MILLISECONDS)
        .writeTimeout(AppConfig.MAX_WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
        .build();
  }
}
