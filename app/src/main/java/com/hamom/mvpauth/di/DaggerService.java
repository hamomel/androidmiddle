package com.hamom.mvpauth.di;

import android.content.Context;
import android.util.Log;

import com.hamom.mvpauth.ui.screens.account.AccountScreen;
import com.hamom.mvpauth.utils.ConstantManager;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import dagger.Component;

/**
 * Created by hamom on 03.11.16.
 */

public class DaggerService {
  public static final String SERVICE_NAME = "MY_DAGGER_SERVICE";
  private static String TAG = ConstantManager.TAG_PREFIX + "DaggerSvc: ";

  @SuppressWarnings("unchecked")
  public static <T> T getDaggerComponent(Context context) {
    //noinspection ResourceType
    return (T) context.getSystemService(SERVICE_NAME);
  }

  //region===================== CreateComponent ==========================

  public static <T> T createComponent(Object parentComponent, Class<T> componentClass,
      Object... moduleParameters) {
    T component = null;

    try {
      Component ann = componentClass.getAnnotation(Component.class);
      List<Object> dependencies = getAnnotatedModules(ann, moduleParameters);
      if (parentComponent != null) {
        dependencies.add(0, parentComponent);
      }
      String daggerComponentName = nameParser(componentClass);

      Class<?> daggerComponent = Class.forName(daggerComponentName);
      Object builder = daggerComponent.getMethod("builder").invoke(null);

      for (Method method : builder.getClass().getDeclaredMethods()) {
        Class<?>[] params = method.getParameterTypes();
        if (params.length == 1) {
          Class<?> dependencyClass = params[0];
          for (Object dependency : dependencies) {
            if (dependencyClass.isAssignableFrom(dependency.getClass())) {
              method.invoke(builder, dependency);
              break;
            }
          }
        }
      }
      Log.d(TAG, "createComponent: " + builder.getClass().getName());

      component = (T) builder.getClass().getMethod("build").invoke(builder);
    } catch (InvocationTargetException e) {
      e.getCause();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }

    return component;
  }

  private static String nameParser(Class componentClass) {
    String fqn = componentClass.getName();
    String packageName = componentClass.getPackage().getName();
    String simpleName = fqn.substring(packageName.length() + 1);

    return (packageName + ".Dagger" + simpleName).replace('$', '_');
  }

  private static List<Object> getAnnotatedModules(Component ann, Object... moduleParameters)
      throws Exception {
    List<Object> modules = new ArrayList<>();

    for (Class<?> dep : ann.modules()) {
      if (moduleParameters.length > 0) {
        for (Constructor constructor : dep.getConstructors()) {
          if (constructor.getParameterTypes().length == moduleParameters.length) {
            boolean flag = true;
            for (int i = 0; i < moduleParameters.length; i++) {
              if (!constructor.getParameterTypes()[i].isAssignableFrom(
                  moduleParameters[i].getClass())) {
                flag = false;
                break;
              }
            }

            if (flag) {
              Object module = constructor.newInstance(moduleParameters);
              modules.add(module);
              break;
            }
          }
        }
      } else {
        Object module = dep.newInstance();
        modules.add(module);
      }
    }
    return modules;
  }
  //endregion
}
