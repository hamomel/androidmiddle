package com.hamom.mvpauth.di.components;

import android.content.Context;

import com.hamom.mvpauth.data.managers.DataManager;
import com.hamom.mvpauth.di.modules.AppModule;

import com.hamom.mvpauth.di.modules.LocalModule;
import com.hamom.mvpauth.di.modules.NetworkModule;
import com.squareup.leakcanary.RefWatcher;
import dagger.Component;
import javax.inject.Singleton;

/**
 * Created by hamom on 04.11.16.
 */
@Singleton
@Component(modules = {AppModule.class, LocalModule.class, NetworkModule.class})
public interface AppComponent {
  Context getContext();
  RefWatcher getRefWatcher();
  DataManager getDataManger();
}
